@extends('ms.master')

@section('content')

    <div class="col-md-11">
        @if ($myprofile==true)
            <div class="box-tools center-block text-center">
                <a href="{{url('user/changepass/'.optional($user)->id.'/edit')}}" class="btn btn-primary">تغير كلمه السر</a>
                <a href="{{url('user/profile/'.optional($user)->id.'/edit')}}" class="btn btn-warning text-white">تحديث البيانات</a>
                @if (count(optional($user)->stories)>0)
                    @php ($money=optional($user)->mymoney()->where('case',3)->orWhere('case',4)->sum('money'))
                    @if ($money>0 && $myprofile==true)
                        <form action="{{url('user/money/'.optional($user)->id)}}" method="POST" class="d-inline-block">
                            @csrf
                            @method('PUT')
                            <button class="btn btn-info text-white">اطلب مستحقاتك</button>
                        </form>
                        @elseif ($money==0)
                            <button class="btn btn-info text-white" disabled>لا توجد مستحقات</button>
                        @endif
                @endif
            </div>

            <br>
        @endif
        <div class="col-md-5 float-right">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">البيانات الشخصية</h3>
                    <!-- /.box-tools -->
                </div>
                <div class="box-body">
                    الاسم : {{optional($user)->name}}<br>
                    الايمال : {{optional($user)->email}}<br>
                    @if ($pay)
                        طريقة الدفع : {{optional($pay)->name}}<br>
                    @endif
                    @if ($contact)
                        طريقة التواصل : {{optional($contact)->name}}<br>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-md-4 float-right">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">الفصول</h3>
                    <div class="box-tools pull-right"></div>
                </div>
                <div class="box-body">

                    @if (count(optional($user)->stories)>0)
                        <ul class="list-unstyled scrolH">
                            @foreach (optional($user)->stories as $stroy)
                                <li><a href="{{url('user/stories/'.optional($stroy)->id)}}">{{optional($stroy)->title}}</a></li>
                            @endforeach
                        </ul>
                    @else
                        {{"لا يوجد فصول"}}
                    @endif
                </div>
            </div>
        </div>

        <div class="col-md-3  float-right">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">الفريق</h3>
                    <div class="box-tools pull-right"></div>
                </div>
                <div class="box-body">
                    @if (count(optional($user)->teams)>0)
                        <ul class="list-unstyled">
                            @foreach (optional($user)->teams as $team)
                                <li><a href="{{url('admin/teams/'.optional($team)->id)}}">{{optional($team)->name}}</a></li>
                            @endforeach
                        </ul>
                    @else
                        {{"لا يوجد فريق"}}
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    @if ($myprofile==true)

        <div class="info-box bg-info text-white col-md-5 col-sm-12 d-inline-block">
            <span class="info-box-icon"><i class="fa fa-usd"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">مستحقات الشهر</span>
                <span class="info-box-number">{{$nowMoney}}$</span>

            </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->


        <div class="info-box bg-light-blue text-white col-md-5 d-inline-block">
            <span class="info-box-icon"><i class="fa fa-usd"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">مستحقات متراكمة</span>
                <span class="info-box-number">{{$moneyMonth}}$</span>
            </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->

        <div class="info-box bg-red col-md-5 col-sm-12 d-inline-block">
            <span class="info-box-icon"><i class="fa fa-usd"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">مستحقات تحت الطلب</span>
                <span class="info-box-number">{{$userMoney}}$</span>

            </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->

        <div class="info-box bg-success text-white col-md-5 col-sm-12 d-inline-block">
            <span class="info-box-icon"><i class="fa fa-usd"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">مستحقات تم استلامها</span>
                <span class="info-box-number">{{$lastMoney}}$</span>

            </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->

        <div class="clearfix"></div>


        <div class="col-md-6 float-right">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">علميات طلب المستحقات</h3>
                    <div class="box-tools pull-right"></div>
                </div>
                <div class="box-body scrolH">
                    <table class="table table-hover">
                        <tr>
                            <td>المستحقات تحت الطلب</td>
                            <td>تاريخ الطلب</td>
                        </tr>
                        @foreach ($detalisMoney as $des)
                            @if (optional($des)->case===0)
                                <tr>
                                    <td>{{optional($des)->money}}</td>
                                    <td>{{date_format(optional($des)->updated_at,'Y-m-d')}}</td>
                                </tr>
                            @endif
                        @endforeach
                    </table>
                </div>
            </div>
        </div>


        <div class="col-md-6 float-right">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">علميات استلام المستحقات</h3>
                    <div class="box-tools pull-right"></div>
                </div>
                <div class="box-body scrolH">
                    <table class="table table-hover">
                        <tr>
                            <td>المستحقات المستلمه</td>
                            <td>تاريخ التسليم</td>
                        </tr>
                        @foreach ($detalisMoney as $des)
                            @if (optional($des)->case===1)
                            <tr>
                                <td>{{optional($des)->money}}</td>
                                <td>{{date_format(optional($des)->updated_at,'Y-m-d')}}</td>
                            </tr>
                            @endif
                        @endforeach
                    </table>
                </div>
            </div>
        </div>


        <div class="clearfix"></div>

    @endif
@stop
