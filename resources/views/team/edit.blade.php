@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>تعديل الفريق</h1>
@stop

@section('content')
    <form action="{{url('admin/teams/'.$team->id)}}" method="POST" role="form">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="name">اسم الفريق</label>
            <input type="text" name="name" id="name" class="form-control" value="{{$team->name}}" placeholder="اسم الفريق" aria-describedby="name">
        </div>
        <div class="form-group">
            <label for="chatCode">كود المحادثات</label>
            <textarea name="chatCode"  cols="30" rows="10" class="form-control">{{$team->chatCode}}</textarea>
        </div>
        <button type="submit" class="btn btn-warning">تعديل الفريق</button>
    </form>

@stop
