@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>اضافه مستخدم</h1>
@stop

@section('content')
    <form method="POST" action="{{ route('users.store') }}">
        @csrf
            <div class="form-group row">
                <label for="name" class="col-md-3 col-form-label text-md-right">{{ __('name') }}</label>

                <div class="col-md-6">
                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="email" class="col-md-3 col-form-label text-md-right">{{ __('Email') }}</label>

                <div class="col-md-6">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="name" class="col-md-3 col-form-label text-md-right">{{ __('How To Contact') }}</label>

                <div class="col-md-6">
                    <select class="form-control{{ $errors->has('contact_id') ? ' is-invalid' : '' }}" name="contact_id"  value="{{ old('contact_id') }}" id="contact_id">
                        @foreach (\App\Contact::all() as $contact)
                            <option value="{{$contact->id}}">{{$contact->name}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('contact'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('contact') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        <div class="form-group row">
            <label for="name" class="col-md-3 col-form-label text-md-right">{{ __('Contact') }}</label>
            <div class="col-md-6">
                <input id="contact" type="text" class="form-control{{ $errors->has('contact') ? ' is-invalid' : '' }}" name="contact" value="{{ old('contact') }}" required>
            </div>
        </div>

            <div class="form-group row">
                <label for="name" class="col-md-3 col-form-label text-md-right">{{ __('How To Pay') }}</label>

                <div class="col-md-6">
                    <select class="form-control{{ $errors->has('pay_id') ? ' is-invalid' : '' }}" name="pay_id"  value="{{ old('pay_id') }}" id="pay_id">
                        @foreach (\App\Pay::all() as $contact)
                            <option value="{{$contact->id}}">{{$contact->name}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('contact') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

        <div class="form-group row">
            <label for="name" class="col-md-3 col-form-label text-md-right">{{ __('Pay') }}</label>

            <div class="col-md-6">
                <input id="contact" type="text" class="form-control{{ $errors->has('paymoney') ? ' is-invalid' : '' }}" name="paymoney" value="{{ old('paymoney') }}" required >
            </div>
        </div>

        <div class="form-group row">
                <label for="password" class="col-md-3 col-form-label text-md-right">{{ __('Password') }}</label>

                <div class="col-md-6">
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="password-confirm" class="col-md-3 col-form-label text-md-right">{{ __('Password confirm') }}</label>

                <div class="col-md-6">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                </div>
            </div>

            <div class="form-group row mb-0">
                <label for="" class="col-md-5"></label>
                <div class="col-md-6">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Register') }}
                    </button>
                </div>
            </div>
    </form>
@stop
