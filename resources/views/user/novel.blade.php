@extends('ms.master')
@section('content')
    <h3 class="text-center">فصول الرواية</h3>
    <nav class="nav">
        <h2 class="text-dark">{{$novel->name}}</h2>
    </nav>
    <ul class="text-right list-unstyled">
        @if (count($stories)>0)
            @foreach ($stories as $story)
                <li class="mt-3"><a href="{{url('user/stories/'.$story->id)}}" style="border:1px solid currentColor;"  class="btn btn-outline-danger">{{$story->storynum." - ".$story->title}}</a></li>
            @endforeach
            @else
            <li class="alert alert-info text-center"><h3>انتظر الفصول قريبا</h3></li>
        @endif
    </ul>
    <hr>
    <a class="btn btn-secondary" href="{{url('user/novel')}}">العودة</a>
    <div class="clearfix"></div>
@stop
