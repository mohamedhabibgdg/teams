@extends('ms.master')
@section('content')
    <h3 class="text-center">الروايات</h3>
    <nav class="nav">
        @foreach ($novels as $novel)
            <a class="nav-link btn btn-outline-primary ml-1" href="{{url('user/novel/'.$novel->id)}}">{{$novel->name}}</a>
        @endforeach
    </nav>
    {{$novels->links()}}
@stop



