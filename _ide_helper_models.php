<?php
/** @noinspection PhpUndefinedClassInspection */
/** @noinspection PhpFullyQualifiedNameUsageInspection */
/** @noinspection PhpUnusedAliasInspection */

namespace App {

    use Carbon\Carbon;
    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\Relations\BelongsTo;
    use Illuminate\Database\Eloquent\Relations\BelongsToMany;
    use Illuminate\Database\Eloquent\Relations\HasMany;
    use Illuminate\Database\Eloquent\Relations\MorphToMany;
    use Illuminate\Database\Eloquent\Scope;
    use Illuminate\Notifications\DatabaseNotification;
    use LaravelIdea\Helper\App\_ContactCollection;
    use LaravelIdea\Helper\App\_ContactQueryBuilder;
    use LaravelIdea\Helper\App\_MoneyCollection;
    use LaravelIdea\Helper\App\_MoneyQueryBuilder;
    use LaravelIdea\Helper\App\_NavelCollection;
    use LaravelIdea\Helper\App\_NavelQueryBuilder;
    use LaravelIdea\Helper\App\_PayCollection;
    use LaravelIdea\Helper\App\_PayQueryBuilder;
    use LaravelIdea\Helper\App\_RoleCollection;
    use LaravelIdea\Helper\App\_RoleQueryBuilder;
    use LaravelIdea\Helper\App\_StoryCollection;
    use LaravelIdea\Helper\App\_StoryQueryBuilder;
    use LaravelIdea\Helper\App\_TeamCollection;
    use LaravelIdea\Helper\App\_TeamQueryBuilder;
    use LaravelIdea\Helper\App\_TeamUserCollection;
    use LaravelIdea\Helper\App\_TeamUserQueryBuilder;
    use LaravelIdea\Helper\App\_UserCollection;
    use LaravelIdea\Helper\App\_UserQueryBuilder;
    use LaravelIdea\Helper\Illuminate\Notifications\_DatabaseNotificationCollection;
    use LaravelIdea\Helper\Illuminate\Notifications\_DatabaseNotificationQueryBuilder;

    /**
     * @property int $id
     * @property string $name
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property _UserCollection|User[] $users
     * @method HasMany|_UserQueryBuilder users()
     * @method _ContactQueryBuilder newModelQuery()
     * @method _ContactQueryBuilder newQuery()
     * @method static _ContactQueryBuilder query()
     * @method static _ContactCollection|Contact[] all()
     * @method static _ContactQueryBuilder whereId($value)
     * @method static _ContactQueryBuilder whereName($value)
     * @method static _ContactQueryBuilder whereCreatedAt($value)
     * @method static _ContactQueryBuilder whereUpdatedAt($value)
     * @method static average(string $column)
     * @method static avg(string $column)
     * @method static bool chunk(int $count, callable $callback)
     * @method static bool chunkById(int $count, callable $callback, null|string $column = null, null|string $alias = null)
     * @method static int count(string $columns = '*')
     * @method static Contact create(array $attributes = [])
     * @method static _ContactQueryBuilder crossJoin(string $table, \Closure|null|string $first = null, null|string $operator = null, null|string $second = null)
     * @method static _ContactCollection|Contact[] cursor()
     * @method static int decrement(string $column, float|int $amount = 1, array $extra = [])
     * @method static bool doesntExist()
     * @method static _ContactQueryBuilder each(callable $callback, int $count = 1000)
     * @method static bool exists()
     * @method static Contact|null find($id, array $columns = ['*'])
     * @method static _ContactCollection|Contact[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method static Contact findOrFail($id, array $columns = ['*'])
     * @method static Contact findOrNew($id, array $columns = ['*'])
     * @method static Contact first(array $columns = ['*'])
     * @method static Contact firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method static Contact firstOrCreate(array $attributes, array $values = [])
     * @method static Contact firstOrFail(array $columns = ['*'])
     * @method static Contact firstOrNew(array $attributes, array $values = [])
     * @method static Contact forceCreate(array $attributes)
     * @method static _ContactCollection|Contact[] fromQuery(string $query, array $bindings = [])
     * @method static _ContactCollection|Contact[] get(array $columns = ['*'])
     * @method static int getCountForPagination(array $columns = ['*'])
     * @method static Contact getModel()
     * @method static Contact[] getModels(array $columns = ['*'])
     * @method static _ContactQueryBuilder getQuery()
     * @method static _ContactQueryBuilder groupBy(array $groups)
     * @method static bool hasMacro(string $name)
     * @method static _ContactCollection|Contact[] hydrate(array $items)
     * @method static int increment(string $column, float|int $amount = 1, array $extra = [])
     * @method static bool insert(array $values)
     * @method static int insertGetId(array $values, null|string $sequence = null)
     * @method static int insertOrIgnore(array $values)
     * @method static bool insertUsing(array $columns, \Closure|\Illuminate\Database\Query\Builder|string $query)
     * @method static _ContactQueryBuilder join(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null, string $type = 'inner', bool $where = false)
     * @method static _ContactQueryBuilder latest(string $column = null)
     * @method static _ContactQueryBuilder leftJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @method static _ContactQueryBuilder limit(int $value)
     * @method static Contact make(array $attributes = [])
     * @method static max(string $column)
     * @method static min(string $column)
     * @method static Contact newModelInstance(array $attributes = [])
     * @method static int numericAggregate(string $function, array $columns = ['*'])
     * @method static _ContactQueryBuilder offset(int $value)
     * @method static _ContactQueryBuilder oldest(string $column = null)
     * @method static _ContactQueryBuilder orderBy(string $column, string $direction = 'asc')
     * @method static _ContactQueryBuilder orderByDesc(string $column)
     * @method static _ContactQueryBuilder orderByRaw(string $sql, array $bindings = [])
     * @method static _ContactQueryBuilder paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method static _ContactQueryBuilder rightJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @method static _ContactQueryBuilder select(array $columns = ['*'])
     * @method static _ContactQueryBuilder setQuery(\Illuminate\Database\Query\Builder $query)
     * @method static _ContactQueryBuilder simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method static _ContactQueryBuilder skip(int $value)
     * @method static sum(string $column)
     * @method static _ContactQueryBuilder take(int $value)
     * @method static _ContactQueryBuilder tap(callable $callback)
     * @method static _ContactQueryBuilder truncate()
     * @method static _ContactQueryBuilder unless($value, callable $callback, callable|null $default = null)
     * @method static int update(array $values)
     * @method static Contact updateOrCreate(array $attributes, array $values = [])
     * @method static bool updateOrInsert(array $attributes, array $values = [])
     * @method static _ContactQueryBuilder when($value, callable $callback, callable|null $default = null)
     * @method static _ContactQueryBuilder where(array|\Closure|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method static _ContactQueryBuilder whereBetween(string $column, array $values, string $boolean = 'and', bool $not = false)
     * @method static _ContactQueryBuilder whereColumn(array|string $first, null|string $operator = null, null|string $second = null, null|string $boolean = 'and')
     * @method static _ContactQueryBuilder whereDate(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _ContactQueryBuilder whereDay(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _ContactQueryBuilder whereDoesntHave(string $relation, \Closure $callback = null)
     * @method static _ContactQueryBuilder whereDoesntHaveMorph(string $relation, array|string $types, \Closure $callback = null)
     * @method static _ContactQueryBuilder whereExists(\Closure $callback, string $boolean = 'and', bool $not = false)
     * @method static _ContactQueryBuilder whereHas(string $relation, \Closure $callback = null, string $operator = '>=', int $count = 1)
     * @method static _ContactQueryBuilder whereHasMorph(string $relation, array|string $types, \Closure $callback = null, string $operator = '>=', int $count = 1)
     * @method static _ContactQueryBuilder whereIn(string $column, $values, string $boolean = 'and', bool $not = false)
     * @method static _ContactQueryBuilder whereIntegerInRaw(string $column, array|Arrayable $values, string $boolean = 'and', bool $not = false)
     * @method static _ContactQueryBuilder whereIntegerNotInRaw(string $column, array|Arrayable $values, string $boolean = 'and')
     * @method static _ContactQueryBuilder whereJsonContains(string $column, $value, string $boolean = 'and', bool $not = false)
     * @method static _ContactQueryBuilder whereJsonDoesntContain(string $column, $value, string $boolean = 'and')
     * @method static _ContactQueryBuilder whereJsonLength(string $column, $operator, $value = null, string $boolean = 'and')
     * @method static _ContactQueryBuilder whereKey($id)
     * @method static _ContactQueryBuilder whereKeyNot($id)
     * @method static _ContactQueryBuilder whereMonth(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _ContactQueryBuilder whereNested(\Closure $callback, string $boolean = 'and')
     * @method static _ContactQueryBuilder whereNotBetween(string $column, array $values, string $boolean = 'and')
     * @method static _ContactQueryBuilder whereNotExists(\Closure $callback, string $boolean = 'and')
     * @method static _ContactQueryBuilder whereNotIn(string $column, $values, string $boolean = 'and')
     * @method static _ContactQueryBuilder whereNotNull(string $column, string $boolean = 'and')
     * @method static _ContactQueryBuilder whereNull(array|string $columns, string $boolean = 'and', bool $not = false)
     * @method static _ContactQueryBuilder whereRaw(string $sql, $bindings = [], string $boolean = 'and')
     * @method static _ContactQueryBuilder whereRowValues(array $columns, string $operator, array $values, string $boolean = 'and')
     * @method static _ContactQueryBuilder whereTime(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _ContactQueryBuilder whereYear(string $column, string $operator, \DateTimeInterface|int|null|string $value = null, string $boolean = 'and')
     * @method static _ContactQueryBuilder with($relations)
     * @method static _ContactQueryBuilder withCount($relations)
     * @method static _ContactQueryBuilder withGlobalScope(string $identifier, \Closure|Scope $scope)
     * @method static _ContactQueryBuilder without($relations)
     * @method static _ContactQueryBuilder withoutGlobalScope(Scope|string $scope)
     * @method static _ContactQueryBuilder withoutGlobalScopes(array $scopes = null)
     */
    class Contact extends Model
    {
    }

    /**
     * @property int $id
     * @property int $user_id
     * @property string $money
     * @property int $case
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property User $user
     * @method BelongsTo|_UserQueryBuilder user()
     * @method _MoneyQueryBuilder newModelQuery()
     * @method _MoneyQueryBuilder newQuery()
     * @method static _MoneyQueryBuilder query()
     * @method static _MoneyCollection|Money[] all()
     * @method static _MoneyQueryBuilder whereId($value)
     * @method static _MoneyQueryBuilder whereUserId($value)
     * @method static _MoneyQueryBuilder whereMoney($value)
     * @method static _MoneyQueryBuilder whereCase($value)
     * @method static _MoneyQueryBuilder whereCreatedAt($value)
     * @method static _MoneyQueryBuilder whereUpdatedAt($value)
     * @method static average(string $column)
     * @method static avg(string $column)
     * @method static bool chunk(int $count, callable $callback)
     * @method static bool chunkById(int $count, callable $callback, null|string $column = null, null|string $alias = null)
     * @method static int count(string $columns = '*')
     * @method static Money create(array $attributes = [])
     * @method static _MoneyQueryBuilder crossJoin(string $table, \Closure|null|string $first = null, null|string $operator = null, null|string $second = null)
     * @method static _MoneyCollection|Money[] cursor()
     * @method static int decrement(string $column, float|int $amount = 1, array $extra = [])
     * @method static bool doesntExist()
     * @method static _MoneyQueryBuilder each(callable $callback, int $count = 1000)
     * @method static bool exists()
     * @method static Money|null find($id, array $columns = ['*'])
     * @method static _MoneyCollection|Money[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method static Money findOrFail($id, array $columns = ['*'])
     * @method static Money findOrNew($id, array $columns = ['*'])
     * @method static Money first(array $columns = ['*'])
     * @method static Money firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method static Money firstOrCreate(array $attributes, array $values = [])
     * @method static Money firstOrFail(array $columns = ['*'])
     * @method static Money firstOrNew(array $attributes, array $values = [])
     * @method static Money forceCreate(array $attributes)
     * @method static _MoneyCollection|Money[] fromQuery(string $query, array $bindings = [])
     * @method static _MoneyCollection|Money[] get(array $columns = ['*'])
     * @method static int getCountForPagination(array $columns = ['*'])
     * @method static Money getModel()
     * @method static Money[] getModels(array $columns = ['*'])
     * @method static _MoneyQueryBuilder getQuery()
     * @method static _MoneyQueryBuilder groupBy(array $groups)
     * @method static bool hasMacro(string $name)
     * @method static _MoneyCollection|Money[] hydrate(array $items)
     * @method static int increment(string $column, float|int $amount = 1, array $extra = [])
     * @method static bool insert(array $values)
     * @method static int insertGetId(array $values, null|string $sequence = null)
     * @method static int insertOrIgnore(array $values)
     * @method static bool insertUsing(array $columns, \Closure|\Illuminate\Database\Query\Builder|string $query)
     * @method static _MoneyQueryBuilder join(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null, string $type = 'inner', bool $where = false)
     * @method static _MoneyQueryBuilder latest(string $column = null)
     * @method static _MoneyQueryBuilder leftJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @method static _MoneyQueryBuilder limit(int $value)
     * @method static Money make(array $attributes = [])
     * @method static max(string $column)
     * @method static min(string $column)
     * @method static Money newModelInstance(array $attributes = [])
     * @method static int numericAggregate(string $function, array $columns = ['*'])
     * @method static _MoneyQueryBuilder offset(int $value)
     * @method static _MoneyQueryBuilder oldest(string $column = null)
     * @method static _MoneyQueryBuilder orderBy(string $column, string $direction = 'asc')
     * @method static _MoneyQueryBuilder orderByDesc(string $column)
     * @method static _MoneyQueryBuilder orderByRaw(string $sql, array $bindings = [])
     * @method static _MoneyQueryBuilder paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method static _MoneyQueryBuilder rightJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @method static _MoneyQueryBuilder select(array $columns = ['*'])
     * @method static _MoneyQueryBuilder setQuery(\Illuminate\Database\Query\Builder $query)
     * @method static _MoneyQueryBuilder simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method static _MoneyQueryBuilder skip(int $value)
     * @method static sum(string $column)
     * @method static _MoneyQueryBuilder take(int $value)
     * @method static _MoneyQueryBuilder tap(callable $callback)
     * @method static _MoneyQueryBuilder truncate()
     * @method static _MoneyQueryBuilder unless($value, callable $callback, callable|null $default = null)
     * @method static int update(array $values)
     * @method static Money updateOrCreate(array $attributes, array $values = [])
     * @method static bool updateOrInsert(array $attributes, array $values = [])
     * @method static _MoneyQueryBuilder when($value, callable $callback, callable|null $default = null)
     * @method static _MoneyQueryBuilder where(array|\Closure|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method static _MoneyQueryBuilder whereBetween(string $column, array $values, string $boolean = 'and', bool $not = false)
     * @method static _MoneyQueryBuilder whereColumn(array|string $first, null|string $operator = null, null|string $second = null, null|string $boolean = 'and')
     * @method static _MoneyQueryBuilder whereDate(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _MoneyQueryBuilder whereDay(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _MoneyQueryBuilder whereDoesntHave(string $relation, \Closure $callback = null)
     * @method static _MoneyQueryBuilder whereDoesntHaveMorph(string $relation, array|string $types, \Closure $callback = null)
     * @method static _MoneyQueryBuilder whereExists(\Closure $callback, string $boolean = 'and', bool $not = false)
     * @method static _MoneyQueryBuilder whereHas(string $relation, \Closure $callback = null, string $operator = '>=', int $count = 1)
     * @method static _MoneyQueryBuilder whereHasMorph(string $relation, array|string $types, \Closure $callback = null, string $operator = '>=', int $count = 1)
     * @method static _MoneyQueryBuilder whereIn(string $column, $values, string $boolean = 'and', bool $not = false)
     * @method static _MoneyQueryBuilder whereIntegerInRaw(string $column, array|Arrayable $values, string $boolean = 'and', bool $not = false)
     * @method static _MoneyQueryBuilder whereIntegerNotInRaw(string $column, array|Arrayable $values, string $boolean = 'and')
     * @method static _MoneyQueryBuilder whereJsonContains(string $column, $value, string $boolean = 'and', bool $not = false)
     * @method static _MoneyQueryBuilder whereJsonDoesntContain(string $column, $value, string $boolean = 'and')
     * @method static _MoneyQueryBuilder whereJsonLength(string $column, $operator, $value = null, string $boolean = 'and')
     * @method static _MoneyQueryBuilder whereKey($id)
     * @method static _MoneyQueryBuilder whereKeyNot($id)
     * @method static _MoneyQueryBuilder whereMonth(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _MoneyQueryBuilder whereNested(\Closure $callback, string $boolean = 'and')
     * @method static _MoneyQueryBuilder whereNotBetween(string $column, array $values, string $boolean = 'and')
     * @method static _MoneyQueryBuilder whereNotExists(\Closure $callback, string $boolean = 'and')
     * @method static _MoneyQueryBuilder whereNotIn(string $column, $values, string $boolean = 'and')
     * @method static _MoneyQueryBuilder whereNotNull(string $column, string $boolean = 'and')
     * @method static _MoneyQueryBuilder whereNull(array|string $columns, string $boolean = 'and', bool $not = false)
     * @method static _MoneyQueryBuilder whereRaw(string $sql, $bindings = [], string $boolean = 'and')
     * @method static _MoneyQueryBuilder whereRowValues(array $columns, string $operator, array $values, string $boolean = 'and')
     * @method static _MoneyQueryBuilder whereTime(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _MoneyQueryBuilder whereYear(string $column, string $operator, \DateTimeInterface|int|null|string $value = null, string $boolean = 'and')
     * @method static _MoneyQueryBuilder with($relations)
     * @method static _MoneyQueryBuilder withCount($relations)
     * @method static _MoneyQueryBuilder withGlobalScope(string $identifier, \Closure|Scope $scope)
     * @method static _MoneyQueryBuilder without($relations)
     * @method static _MoneyQueryBuilder withoutGlobalScope(Scope|string $scope)
     * @method static _MoneyQueryBuilder withoutGlobalScopes(array $scopes = null)
     * @method static _MoneyQueryBuilder own($user_id = null)
     */
    class Money extends Model
    {
    }

    /**
     * @property int $id
     * @property string $name
     * @property string|null $image
     * @property int $team_id
     * @property float $price
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property _StoryCollection|Story[] $stories
     * @method HasMany|_StoryQueryBuilder stories()
     * @property Team $team
     * @method BelongsTo|_TeamQueryBuilder team()
     * @method _NavelQueryBuilder newModelQuery()
     * @method _NavelQueryBuilder newQuery()
     * @method static _NavelQueryBuilder query()
     * @method static _NavelCollection|Navel[] all()
     * @method static _NavelQueryBuilder whereId($value)
     * @method static _NavelQueryBuilder whereName($value)
     * @method static _NavelQueryBuilder whereImage($value)
     * @method static _NavelQueryBuilder whereTeamId($value)
     * @method static _NavelQueryBuilder wherePrice($value)
     * @method static _NavelQueryBuilder whereCreatedAt($value)
     * @method static _NavelQueryBuilder whereUpdatedAt($value)
     * @method static average(string $column)
     * @method static avg(string $column)
     * @method static bool chunk(int $count, callable $callback)
     * @method static bool chunkById(int $count, callable $callback, null|string $column = null, null|string $alias = null)
     * @method static int count(string $columns = '*')
     * @method static Navel create(array $attributes = [])
     * @method static _NavelQueryBuilder crossJoin(string $table, \Closure|null|string $first = null, null|string $operator = null, null|string $second = null)
     * @method static _NavelCollection|Navel[] cursor()
     * @method static int decrement(string $column, float|int $amount = 1, array $extra = [])
     * @method static bool doesntExist()
     * @method static _NavelQueryBuilder each(callable $callback, int $count = 1000)
     * @method static bool exists()
     * @method static Navel|null find($id, array $columns = ['*'])
     * @method static _NavelCollection|Navel[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method static Navel findOrFail($id, array $columns = ['*'])
     * @method static Navel findOrNew($id, array $columns = ['*'])
     * @method static Navel first(array $columns = ['*'])
     * @method static Navel firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method static Navel firstOrCreate(array $attributes, array $values = [])
     * @method static Navel firstOrFail(array $columns = ['*'])
     * @method static Navel firstOrNew(array $attributes, array $values = [])
     * @method static Navel forceCreate(array $attributes)
     * @method static _NavelCollection|Navel[] fromQuery(string $query, array $bindings = [])
     * @method static _NavelCollection|Navel[] get(array $columns = ['*'])
     * @method static int getCountForPagination(array $columns = ['*'])
     * @method static Navel getModel()
     * @method static Navel[] getModels(array $columns = ['*'])
     * @method static _NavelQueryBuilder getQuery()
     * @method static _NavelQueryBuilder groupBy(array $groups)
     * @method static bool hasMacro(string $name)
     * @method static _NavelCollection|Navel[] hydrate(array $items)
     * @method static int increment(string $column, float|int $amount = 1, array $extra = [])
     * @method static bool insert(array $values)
     * @method static int insertGetId(array $values, null|string $sequence = null)
     * @method static int insertOrIgnore(array $values)
     * @method static bool insertUsing(array $columns, \Closure|\Illuminate\Database\Query\Builder|string $query)
     * @method static _NavelQueryBuilder join(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null, string $type = 'inner', bool $where = false)
     * @method static _NavelQueryBuilder latest(string $column = null)
     * @method static _NavelQueryBuilder leftJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @method static _NavelQueryBuilder limit(int $value)
     * @method static Navel make(array $attributes = [])
     * @method static max(string $column)
     * @method static min(string $column)
     * @method static Navel newModelInstance(array $attributes = [])
     * @method static int numericAggregate(string $function, array $columns = ['*'])
     * @method static _NavelQueryBuilder offset(int $value)
     * @method static _NavelQueryBuilder oldest(string $column = null)
     * @method static _NavelQueryBuilder orderBy(string $column, string $direction = 'asc')
     * @method static _NavelQueryBuilder orderByDesc(string $column)
     * @method static _NavelQueryBuilder orderByRaw(string $sql, array $bindings = [])
     * @method static _NavelQueryBuilder paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method static _NavelQueryBuilder rightJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @method static _NavelQueryBuilder select(array $columns = ['*'])
     * @method static _NavelQueryBuilder setQuery(\Illuminate\Database\Query\Builder $query)
     * @method static _NavelQueryBuilder simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method static _NavelQueryBuilder skip(int $value)
     * @method static sum(string $column)
     * @method static _NavelQueryBuilder take(int $value)
     * @method static _NavelQueryBuilder tap(callable $callback)
     * @method static _NavelQueryBuilder truncate()
     * @method static _NavelQueryBuilder unless($value, callable $callback, callable|null $default = null)
     * @method static int update(array $values)
     * @method static Navel updateOrCreate(array $attributes, array $values = [])
     * @method static bool updateOrInsert(array $attributes, array $values = [])
     * @method static _NavelQueryBuilder when($value, callable $callback, callable|null $default = null)
     * @method static _NavelQueryBuilder where(array|\Closure|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method static _NavelQueryBuilder whereBetween(string $column, array $values, string $boolean = 'and', bool $not = false)
     * @method static _NavelQueryBuilder whereColumn(array|string $first, null|string $operator = null, null|string $second = null, null|string $boolean = 'and')
     * @method static _NavelQueryBuilder whereDate(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _NavelQueryBuilder whereDay(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _NavelQueryBuilder whereDoesntHave(string $relation, \Closure $callback = null)
     * @method static _NavelQueryBuilder whereDoesntHaveMorph(string $relation, array|string $types, \Closure $callback = null)
     * @method static _NavelQueryBuilder whereExists(\Closure $callback, string $boolean = 'and', bool $not = false)
     * @method static _NavelQueryBuilder whereHas(string $relation, \Closure $callback = null, string $operator = '>=', int $count = 1)
     * @method static _NavelQueryBuilder whereHasMorph(string $relation, array|string $types, \Closure $callback = null, string $operator = '>=', int $count = 1)
     * @method static _NavelQueryBuilder whereIn(string $column, $values, string $boolean = 'and', bool $not = false)
     * @method static _NavelQueryBuilder whereIntegerInRaw(string $column, array|Arrayable $values, string $boolean = 'and', bool $not = false)
     * @method static _NavelQueryBuilder whereIntegerNotInRaw(string $column, array|Arrayable $values, string $boolean = 'and')
     * @method static _NavelQueryBuilder whereJsonContains(string $column, $value, string $boolean = 'and', bool $not = false)
     * @method static _NavelQueryBuilder whereJsonDoesntContain(string $column, $value, string $boolean = 'and')
     * @method static _NavelQueryBuilder whereJsonLength(string $column, $operator, $value = null, string $boolean = 'and')
     * @method static _NavelQueryBuilder whereKey($id)
     * @method static _NavelQueryBuilder whereKeyNot($id)
     * @method static _NavelQueryBuilder whereMonth(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _NavelQueryBuilder whereNested(\Closure $callback, string $boolean = 'and')
     * @method static _NavelQueryBuilder whereNotBetween(string $column, array $values, string $boolean = 'and')
     * @method static _NavelQueryBuilder whereNotExists(\Closure $callback, string $boolean = 'and')
     * @method static _NavelQueryBuilder whereNotIn(string $column, $values, string $boolean = 'and')
     * @method static _NavelQueryBuilder whereNotNull(string $column, string $boolean = 'and')
     * @method static _NavelQueryBuilder whereNull(array|string $columns, string $boolean = 'and', bool $not = false)
     * @method static _NavelQueryBuilder whereRaw(string $sql, $bindings = [], string $boolean = 'and')
     * @method static _NavelQueryBuilder whereRowValues(array $columns, string $operator, array $values, string $boolean = 'and')
     * @method static _NavelQueryBuilder whereTime(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _NavelQueryBuilder whereYear(string $column, string $operator, \DateTimeInterface|int|null|string $value = null, string $boolean = 'and')
     * @method static _NavelQueryBuilder with($relations)
     * @method static _NavelQueryBuilder withCount($relations)
     * @method static _NavelQueryBuilder withGlobalScope(string $identifier, \Closure|Scope $scope)
     * @method static _NavelQueryBuilder without($relations)
     * @method static _NavelQueryBuilder withoutGlobalScope(Scope|string $scope)
     * @method static _NavelQueryBuilder withoutGlobalScopes(array $scopes = null)
     */
    class Navel extends Model
    {
    }

    /**
     * @property int $id
     * @property string $name
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property _UserCollection|User[] $users
     * @method HasMany|_UserQueryBuilder users()
     * @method _PayQueryBuilder newModelQuery()
     * @method _PayQueryBuilder newQuery()
     * @method static _PayQueryBuilder query()
     * @method static _PayCollection|Pay[] all()
     * @method static _PayQueryBuilder whereId($value)
     * @method static _PayQueryBuilder whereName($value)
     * @method static _PayQueryBuilder whereCreatedAt($value)
     * @method static _PayQueryBuilder whereUpdatedAt($value)
     * @method static average(string $column)
     * @method static avg(string $column)
     * @method static bool chunk(int $count, callable $callback)
     * @method static bool chunkById(int $count, callable $callback, null|string $column = null, null|string $alias = null)
     * @method static int count(string $columns = '*')
     * @method static Pay create(array $attributes = [])
     * @method static _PayQueryBuilder crossJoin(string $table, \Closure|null|string $first = null, null|string $operator = null, null|string $second = null)
     * @method static _PayCollection|Pay[] cursor()
     * @method static int decrement(string $column, float|int $amount = 1, array $extra = [])
     * @method static bool doesntExist()
     * @method static _PayQueryBuilder each(callable $callback, int $count = 1000)
     * @method static bool exists()
     * @method static Pay|null find($id, array $columns = ['*'])
     * @method static _PayCollection|Pay[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method static Pay findOrFail($id, array $columns = ['*'])
     * @method static Pay findOrNew($id, array $columns = ['*'])
     * @method static Pay first(array $columns = ['*'])
     * @method static Pay firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method static Pay firstOrCreate(array $attributes, array $values = [])
     * @method static Pay firstOrFail(array $columns = ['*'])
     * @method static Pay firstOrNew(array $attributes, array $values = [])
     * @method static Pay forceCreate(array $attributes)
     * @method static _PayCollection|Pay[] fromQuery(string $query, array $bindings = [])
     * @method static _PayCollection|Pay[] get(array $columns = ['*'])
     * @method static int getCountForPagination(array $columns = ['*'])
     * @method static Pay getModel()
     * @method static Pay[] getModels(array $columns = ['*'])
     * @method static _PayQueryBuilder getQuery()
     * @method static _PayQueryBuilder groupBy(array $groups)
     * @method static bool hasMacro(string $name)
     * @method static _PayCollection|Pay[] hydrate(array $items)
     * @method static int increment(string $column, float|int $amount = 1, array $extra = [])
     * @method static bool insert(array $values)
     * @method static int insertGetId(array $values, null|string $sequence = null)
     * @method static int insertOrIgnore(array $values)
     * @method static bool insertUsing(array $columns, \Closure|\Illuminate\Database\Query\Builder|string $query)
     * @method static _PayQueryBuilder join(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null, string $type = 'inner', bool $where = false)
     * @method static _PayQueryBuilder latest(string $column = null)
     * @method static _PayQueryBuilder leftJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @method static _PayQueryBuilder limit(int $value)
     * @method static Pay make(array $attributes = [])
     * @method static max(string $column)
     * @method static min(string $column)
     * @method static Pay newModelInstance(array $attributes = [])
     * @method static int numericAggregate(string $function, array $columns = ['*'])
     * @method static _PayQueryBuilder offset(int $value)
     * @method static _PayQueryBuilder oldest(string $column = null)
     * @method static _PayQueryBuilder orderBy(string $column, string $direction = 'asc')
     * @method static _PayQueryBuilder orderByDesc(string $column)
     * @method static _PayQueryBuilder orderByRaw(string $sql, array $bindings = [])
     * @method static _PayQueryBuilder paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method static _PayQueryBuilder rightJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @method static _PayQueryBuilder select(array $columns = ['*'])
     * @method static _PayQueryBuilder setQuery(\Illuminate\Database\Query\Builder $query)
     * @method static _PayQueryBuilder simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method static _PayQueryBuilder skip(int $value)
     * @method static sum(string $column)
     * @method static _PayQueryBuilder take(int $value)
     * @method static _PayQueryBuilder tap(callable $callback)
     * @method static _PayQueryBuilder truncate()
     * @method static _PayQueryBuilder unless($value, callable $callback, callable|null $default = null)
     * @method static int update(array $values)
     * @method static Pay updateOrCreate(array $attributes, array $values = [])
     * @method static bool updateOrInsert(array $attributes, array $values = [])
     * @method static _PayQueryBuilder when($value, callable $callback, callable|null $default = null)
     * @method static _PayQueryBuilder where(array|\Closure|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method static _PayQueryBuilder whereBetween(string $column, array $values, string $boolean = 'and', bool $not = false)
     * @method static _PayQueryBuilder whereColumn(array|string $first, null|string $operator = null, null|string $second = null, null|string $boolean = 'and')
     * @method static _PayQueryBuilder whereDate(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _PayQueryBuilder whereDay(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _PayQueryBuilder whereDoesntHave(string $relation, \Closure $callback = null)
     * @method static _PayQueryBuilder whereDoesntHaveMorph(string $relation, array|string $types, \Closure $callback = null)
     * @method static _PayQueryBuilder whereExists(\Closure $callback, string $boolean = 'and', bool $not = false)
     * @method static _PayQueryBuilder whereHas(string $relation, \Closure $callback = null, string $operator = '>=', int $count = 1)
     * @method static _PayQueryBuilder whereHasMorph(string $relation, array|string $types, \Closure $callback = null, string $operator = '>=', int $count = 1)
     * @method static _PayQueryBuilder whereIn(string $column, $values, string $boolean = 'and', bool $not = false)
     * @method static _PayQueryBuilder whereIntegerInRaw(string $column, array|Arrayable $values, string $boolean = 'and', bool $not = false)
     * @method static _PayQueryBuilder whereIntegerNotInRaw(string $column, array|Arrayable $values, string $boolean = 'and')
     * @method static _PayQueryBuilder whereJsonContains(string $column, $value, string $boolean = 'and', bool $not = false)
     * @method static _PayQueryBuilder whereJsonDoesntContain(string $column, $value, string $boolean = 'and')
     * @method static _PayQueryBuilder whereJsonLength(string $column, $operator, $value = null, string $boolean = 'and')
     * @method static _PayQueryBuilder whereKey($id)
     * @method static _PayQueryBuilder whereKeyNot($id)
     * @method static _PayQueryBuilder whereMonth(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _PayQueryBuilder whereNested(\Closure $callback, string $boolean = 'and')
     * @method static _PayQueryBuilder whereNotBetween(string $column, array $values, string $boolean = 'and')
     * @method static _PayQueryBuilder whereNotExists(\Closure $callback, string $boolean = 'and')
     * @method static _PayQueryBuilder whereNotIn(string $column, $values, string $boolean = 'and')
     * @method static _PayQueryBuilder whereNotNull(string $column, string $boolean = 'and')
     * @method static _PayQueryBuilder whereNull(array|string $columns, string $boolean = 'and', bool $not = false)
     * @method static _PayQueryBuilder whereRaw(string $sql, $bindings = [], string $boolean = 'and')
     * @method static _PayQueryBuilder whereRowValues(array $columns, string $operator, array $values, string $boolean = 'and')
     * @method static _PayQueryBuilder whereTime(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _PayQueryBuilder whereYear(string $column, string $operator, \DateTimeInterface|int|null|string $value = null, string $boolean = 'and')
     * @method static _PayQueryBuilder with($relations)
     * @method static _PayQueryBuilder withCount($relations)
     * @method static _PayQueryBuilder withGlobalScope(string $identifier, \Closure|Scope $scope)
     * @method static _PayQueryBuilder without($relations)
     * @method static _PayQueryBuilder withoutGlobalScope(Scope|string $scope)
     * @method static _PayQueryBuilder withoutGlobalScopes(array $scopes = null)
     */
    class Pay extends Model
    {
    }

    /**
     * @property int $id
     * @property string $name
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property _UserCollection|User[] $users
     * @method BelongsToMany|_UserQueryBuilder users()
     * @method _RoleQueryBuilder newModelQuery()
     * @method _RoleQueryBuilder newQuery()
     * @method static _RoleQueryBuilder query()
     * @method static _RoleCollection|Role[] all()
     * @method static _RoleQueryBuilder whereId($value)
     * @method static _RoleQueryBuilder whereName($value)
     * @method static _RoleQueryBuilder whereCreatedAt($value)
     * @method static _RoleQueryBuilder whereUpdatedAt($value)
     * @method static average(string $column)
     * @method static avg(string $column)
     * @method static bool chunk(int $count, callable $callback)
     * @method static bool chunkById(int $count, callable $callback, null|string $column = null, null|string $alias = null)
     * @method static int count(string $columns = '*')
     * @method static Role create(array $attributes = [])
     * @method static _RoleQueryBuilder crossJoin(string $table, \Closure|null|string $first = null, null|string $operator = null, null|string $second = null)
     * @method static _RoleCollection|Role[] cursor()
     * @method static int decrement(string $column, float|int $amount = 1, array $extra = [])
     * @method static bool doesntExist()
     * @method static _RoleQueryBuilder each(callable $callback, int $count = 1000)
     * @method static bool exists()
     * @method static Role|null find($id, array $columns = ['*'])
     * @method static _RoleCollection|Role[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method static Role findOrFail($id, array $columns = ['*'])
     * @method static Role findOrNew($id, array $columns = ['*'])
     * @method static Role first(array $columns = ['*'])
     * @method static Role firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method static Role firstOrCreate(array $attributes, array $values = [])
     * @method static Role firstOrFail(array $columns = ['*'])
     * @method static Role firstOrNew(array $attributes, array $values = [])
     * @method static Role forceCreate(array $attributes)
     * @method static _RoleCollection|Role[] fromQuery(string $query, array $bindings = [])
     * @method static _RoleCollection|Role[] get(array $columns = ['*'])
     * @method static int getCountForPagination(array $columns = ['*'])
     * @method static Role getModel()
     * @method static Role[] getModels(array $columns = ['*'])
     * @method static _RoleQueryBuilder getQuery()
     * @method static _RoleQueryBuilder groupBy(array $groups)
     * @method static bool hasMacro(string $name)
     * @method static _RoleCollection|Role[] hydrate(array $items)
     * @method static int increment(string $column, float|int $amount = 1, array $extra = [])
     * @method static bool insert(array $values)
     * @method static int insertGetId(array $values, null|string $sequence = null)
     * @method static int insertOrIgnore(array $values)
     * @method static bool insertUsing(array $columns, \Closure|\Illuminate\Database\Query\Builder|string $query)
     * @method static _RoleQueryBuilder join(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null, string $type = 'inner', bool $where = false)
     * @method static _RoleQueryBuilder latest(string $column = null)
     * @method static _RoleQueryBuilder leftJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @method static _RoleQueryBuilder limit(int $value)
     * @method static Role make(array $attributes = [])
     * @method static max(string $column)
     * @method static min(string $column)
     * @method static Role newModelInstance(array $attributes = [])
     * @method static int numericAggregate(string $function, array $columns = ['*'])
     * @method static _RoleQueryBuilder offset(int $value)
     * @method static _RoleQueryBuilder oldest(string $column = null)
     * @method static _RoleQueryBuilder orderBy(string $column, string $direction = 'asc')
     * @method static _RoleQueryBuilder orderByDesc(string $column)
     * @method static _RoleQueryBuilder orderByRaw(string $sql, array $bindings = [])
     * @method static _RoleQueryBuilder paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method static _RoleQueryBuilder rightJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @method static _RoleQueryBuilder select(array $columns = ['*'])
     * @method static _RoleQueryBuilder setQuery(\Illuminate\Database\Query\Builder $query)
     * @method static _RoleQueryBuilder simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method static _RoleQueryBuilder skip(int $value)
     * @method static sum(string $column)
     * @method static _RoleQueryBuilder take(int $value)
     * @method static _RoleQueryBuilder tap(callable $callback)
     * @method static _RoleQueryBuilder truncate()
     * @method static _RoleQueryBuilder unless($value, callable $callback, callable|null $default = null)
     * @method static int update(array $values)
     * @method static Role updateOrCreate(array $attributes, array $values = [])
     * @method static bool updateOrInsert(array $attributes, array $values = [])
     * @method static _RoleQueryBuilder when($value, callable $callback, callable|null $default = null)
     * @method static _RoleQueryBuilder where(array|\Closure|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method static _RoleQueryBuilder whereBetween(string $column, array $values, string $boolean = 'and', bool $not = false)
     * @method static _RoleQueryBuilder whereColumn(array|string $first, null|string $operator = null, null|string $second = null, null|string $boolean = 'and')
     * @method static _RoleQueryBuilder whereDate(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _RoleQueryBuilder whereDay(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _RoleQueryBuilder whereDoesntHave(string $relation, \Closure $callback = null)
     * @method static _RoleQueryBuilder whereDoesntHaveMorph(string $relation, array|string $types, \Closure $callback = null)
     * @method static _RoleQueryBuilder whereExists(\Closure $callback, string $boolean = 'and', bool $not = false)
     * @method static _RoleQueryBuilder whereHas(string $relation, \Closure $callback = null, string $operator = '>=', int $count = 1)
     * @method static _RoleQueryBuilder whereHasMorph(string $relation, array|string $types, \Closure $callback = null, string $operator = '>=', int $count = 1)
     * @method static _RoleQueryBuilder whereIn(string $column, $values, string $boolean = 'and', bool $not = false)
     * @method static _RoleQueryBuilder whereIntegerInRaw(string $column, array|Arrayable $values, string $boolean = 'and', bool $not = false)
     * @method static _RoleQueryBuilder whereIntegerNotInRaw(string $column, array|Arrayable $values, string $boolean = 'and')
     * @method static _RoleQueryBuilder whereJsonContains(string $column, $value, string $boolean = 'and', bool $not = false)
     * @method static _RoleQueryBuilder whereJsonDoesntContain(string $column, $value, string $boolean = 'and')
     * @method static _RoleQueryBuilder whereJsonLength(string $column, $operator, $value = null, string $boolean = 'and')
     * @method static _RoleQueryBuilder whereKey($id)
     * @method static _RoleQueryBuilder whereKeyNot($id)
     * @method static _RoleQueryBuilder whereMonth(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _RoleQueryBuilder whereNested(\Closure $callback, string $boolean = 'and')
     * @method static _RoleQueryBuilder whereNotBetween(string $column, array $values, string $boolean = 'and')
     * @method static _RoleQueryBuilder whereNotExists(\Closure $callback, string $boolean = 'and')
     * @method static _RoleQueryBuilder whereNotIn(string $column, $values, string $boolean = 'and')
     * @method static _RoleQueryBuilder whereNotNull(string $column, string $boolean = 'and')
     * @method static _RoleQueryBuilder whereNull(array|string $columns, string $boolean = 'and', bool $not = false)
     * @method static _RoleQueryBuilder whereRaw(string $sql, $bindings = [], string $boolean = 'and')
     * @method static _RoleQueryBuilder whereRowValues(array $columns, string $operator, array $values, string $boolean = 'and')
     * @method static _RoleQueryBuilder whereTime(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _RoleQueryBuilder whereYear(string $column, string $operator, \DateTimeInterface|int|null|string $value = null, string $boolean = 'and')
     * @method static _RoleQueryBuilder with($relations)
     * @method static _RoleQueryBuilder withCount($relations)
     * @method static _RoleQueryBuilder withGlobalScope(string $identifier, \Closure|Scope $scope)
     * @method static _RoleQueryBuilder without($relations)
     * @method static _RoleQueryBuilder withoutGlobalScope(Scope|string $scope)
     * @method static _RoleQueryBuilder withoutGlobalScopes(array $scopes = null)
     */
    class Role extends Model
    {
    }

    /**
     * @property int $id
     * @property int $user_id
     * @property int $storynum
     * @property string $title
     * @property string $body
     * @property int $navel_id
     * @property int $case
     * @property int $done
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property Navel $navel
     * @method BelongsTo|_NavelQueryBuilder navel()
     * @property User $user
     * @method BelongsTo|_UserQueryBuilder user()
     * @method _StoryQueryBuilder newModelQuery()
     * @method _StoryQueryBuilder newQuery()
     * @method static _StoryQueryBuilder query()
     * @method static _StoryCollection|Story[] all()
     * @method static _StoryQueryBuilder whereId($value)
     * @method static _StoryQueryBuilder whereUserId($value)
     * @method static _StoryQueryBuilder whereStorynum($value)
     * @method static _StoryQueryBuilder whereTitle($value)
     * @method static _StoryQueryBuilder whereBody($value)
     * @method static _StoryQueryBuilder whereNavelId($value)
     * @method static _StoryQueryBuilder whereCase($value)
     * @method static _StoryQueryBuilder whereDone($value)
     * @method static _StoryQueryBuilder whereCreatedAt($value)
     * @method static _StoryQueryBuilder whereUpdatedAt($value)
     * @method static average(string $column)
     * @method static avg(string $column)
     * @method static bool chunk(int $count, callable $callback)
     * @method static bool chunkById(int $count, callable $callback, null|string $column = null, null|string $alias = null)
     * @method static int count(string $columns = '*')
     * @method static Story create(array $attributes = [])
     * @method static _StoryQueryBuilder crossJoin(string $table, \Closure|null|string $first = null, null|string $operator = null, null|string $second = null)
     * @method static _StoryCollection|Story[] cursor()
     * @method static int decrement(string $column, float|int $amount = 1, array $extra = [])
     * @method static bool doesntExist()
     * @method static _StoryQueryBuilder each(callable $callback, int $count = 1000)
     * @method static bool exists()
     * @method static Story|null find($id, array $columns = ['*'])
     * @method static _StoryCollection|Story[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method static Story findOrFail($id, array $columns = ['*'])
     * @method static Story findOrNew($id, array $columns = ['*'])
     * @method static Story first(array $columns = ['*'])
     * @method static Story firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method static Story firstOrCreate(array $attributes, array $values = [])
     * @method static Story firstOrFail(array $columns = ['*'])
     * @method static Story firstOrNew(array $attributes, array $values = [])
     * @method static Story forceCreate(array $attributes)
     * @method static _StoryCollection|Story[] fromQuery(string $query, array $bindings = [])
     * @method static _StoryCollection|Story[] get(array $columns = ['*'])
     * @method static int getCountForPagination(array $columns = ['*'])
     * @method static Story getModel()
     * @method static Story[] getModels(array $columns = ['*'])
     * @method static _StoryQueryBuilder getQuery()
     * @method static _StoryQueryBuilder groupBy(array $groups)
     * @method static bool hasMacro(string $name)
     * @method static _StoryCollection|Story[] hydrate(array $items)
     * @method static int increment(string $column, float|int $amount = 1, array $extra = [])
     * @method static bool insert(array $values)
     * @method static int insertGetId(array $values, null|string $sequence = null)
     * @method static int insertOrIgnore(array $values)
     * @method static bool insertUsing(array $columns, \Closure|\Illuminate\Database\Query\Builder|string $query)
     * @method static _StoryQueryBuilder join(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null, string $type = 'inner', bool $where = false)
     * @method static _StoryQueryBuilder latest(string $column = null)
     * @method static _StoryQueryBuilder leftJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @method static _StoryQueryBuilder limit(int $value)
     * @method static Story make(array $attributes = [])
     * @method static max(string $column)
     * @method static min(string $column)
     * @method static Story newModelInstance(array $attributes = [])
     * @method static int numericAggregate(string $function, array $columns = ['*'])
     * @method static _StoryQueryBuilder offset(int $value)
     * @method static _StoryQueryBuilder oldest(string $column = null)
     * @method static _StoryQueryBuilder orderBy(string $column, string $direction = 'asc')
     * @method static _StoryQueryBuilder orderByDesc(string $column)
     * @method static _StoryQueryBuilder orderByRaw(string $sql, array $bindings = [])
     * @method static _StoryQueryBuilder paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method static _StoryQueryBuilder rightJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @method static _StoryQueryBuilder select(array $columns = ['*'])
     * @method static _StoryQueryBuilder setQuery(\Illuminate\Database\Query\Builder $query)
     * @method static _StoryQueryBuilder simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method static _StoryQueryBuilder skip(int $value)
     * @method static sum(string $column)
     * @method static _StoryQueryBuilder take(int $value)
     * @method static _StoryQueryBuilder tap(callable $callback)
     * @method static _StoryQueryBuilder truncate()
     * @method static _StoryQueryBuilder unless($value, callable $callback, callable|null $default = null)
     * @method static int update(array $values)
     * @method static Story updateOrCreate(array $attributes, array $values = [])
     * @method static bool updateOrInsert(array $attributes, array $values = [])
     * @method static _StoryQueryBuilder when($value, callable $callback, callable|null $default = null)
     * @method static _StoryQueryBuilder where(array|\Closure|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method static _StoryQueryBuilder whereBetween(string $column, array $values, string $boolean = 'and', bool $not = false)
     * @method static _StoryQueryBuilder whereColumn(array|string $first, null|string $operator = null, null|string $second = null, null|string $boolean = 'and')
     * @method static _StoryQueryBuilder whereDate(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _StoryQueryBuilder whereDay(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _StoryQueryBuilder whereDoesntHave(string $relation, \Closure $callback = null)
     * @method static _StoryQueryBuilder whereDoesntHaveMorph(string $relation, array|string $types, \Closure $callback = null)
     * @method static _StoryQueryBuilder whereExists(\Closure $callback, string $boolean = 'and', bool $not = false)
     * @method static _StoryQueryBuilder whereHas(string $relation, \Closure $callback = null, string $operator = '>=', int $count = 1)
     * @method static _StoryQueryBuilder whereHasMorph(string $relation, array|string $types, \Closure $callback = null, string $operator = '>=', int $count = 1)
     * @method static _StoryQueryBuilder whereIn(string $column, $values, string $boolean = 'and', bool $not = false)
     * @method static _StoryQueryBuilder whereIntegerInRaw(string $column, array|Arrayable $values, string $boolean = 'and', bool $not = false)
     * @method static _StoryQueryBuilder whereIntegerNotInRaw(string $column, array|Arrayable $values, string $boolean = 'and')
     * @method static _StoryQueryBuilder whereJsonContains(string $column, $value, string $boolean = 'and', bool $not = false)
     * @method static _StoryQueryBuilder whereJsonDoesntContain(string $column, $value, string $boolean = 'and')
     * @method static _StoryQueryBuilder whereJsonLength(string $column, $operator, $value = null, string $boolean = 'and')
     * @method static _StoryQueryBuilder whereKey($id)
     * @method static _StoryQueryBuilder whereKeyNot($id)
     * @method static _StoryQueryBuilder whereMonth(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _StoryQueryBuilder whereNested(\Closure $callback, string $boolean = 'and')
     * @method static _StoryQueryBuilder whereNotBetween(string $column, array $values, string $boolean = 'and')
     * @method static _StoryQueryBuilder whereNotExists(\Closure $callback, string $boolean = 'and')
     * @method static _StoryQueryBuilder whereNotIn(string $column, $values, string $boolean = 'and')
     * @method static _StoryQueryBuilder whereNotNull(string $column, string $boolean = 'and')
     * @method static _StoryQueryBuilder whereNull(array|string $columns, string $boolean = 'and', bool $not = false)
     * @method static _StoryQueryBuilder whereRaw(string $sql, $bindings = [], string $boolean = 'and')
     * @method static _StoryQueryBuilder whereRowValues(array $columns, string $operator, array $values, string $boolean = 'and')
     * @method static _StoryQueryBuilder whereTime(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _StoryQueryBuilder whereYear(string $column, string $operator, \DateTimeInterface|int|null|string $value = null, string $boolean = 'and')
     * @method static _StoryQueryBuilder with($relations)
     * @method static _StoryQueryBuilder withCount($relations)
     * @method static _StoryQueryBuilder withGlobalScope(string $identifier, \Closure|Scope $scope)
     * @method static _StoryQueryBuilder without($relations)
     * @method static _StoryQueryBuilder withoutGlobalScope(Scope|string $scope)
     * @method static _StoryQueryBuilder withoutGlobalScopes(array $scopes = null)
     */
    class Story extends Model
    {
    }

    /**
     * @property int $id
     * @property string $name
     * @property string|null $chatCode
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property _NavelCollection|Navel[] $novels
     * @method HasMany|_NavelQueryBuilder novels()
     * @property _UserCollection|User[] $users
     * @method BelongsToMany|_UserQueryBuilder users()
     * @method _TeamQueryBuilder newModelQuery()
     * @method _TeamQueryBuilder newQuery()
     * @method static _TeamQueryBuilder query()
     * @method static _TeamCollection|Team[] all()
     * @method static _TeamQueryBuilder whereId($value)
     * @method static _TeamQueryBuilder whereName($value)
     * @method static _TeamQueryBuilder whereChatcode($value)
     * @method static _TeamQueryBuilder whereCreatedAt($value)
     * @method static _TeamQueryBuilder whereUpdatedAt($value)
     * @method static average(string $column)
     * @method static avg(string $column)
     * @method static bool chunk(int $count, callable $callback)
     * @method static bool chunkById(int $count, callable $callback, null|string $column = null, null|string $alias = null)
     * @method static int count(string $columns = '*')
     * @method static Team create(array $attributes = [])
     * @method static _TeamQueryBuilder crossJoin(string $table, \Closure|null|string $first = null, null|string $operator = null, null|string $second = null)
     * @method static _TeamCollection|Team[] cursor()
     * @method static int decrement(string $column, float|int $amount = 1, array $extra = [])
     * @method static bool doesntExist()
     * @method static _TeamQueryBuilder each(callable $callback, int $count = 1000)
     * @method static bool exists()
     * @method static Team|null find($id, array $columns = ['*'])
     * @method static _TeamCollection|Team[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method static Team findOrFail($id, array $columns = ['*'])
     * @method static Team findOrNew($id, array $columns = ['*'])
     * @method static Team first(array $columns = ['*'])
     * @method static Team firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method static Team firstOrCreate(array $attributes, array $values = [])
     * @method static Team firstOrFail(array $columns = ['*'])
     * @method static Team firstOrNew(array $attributes, array $values = [])
     * @method static Team forceCreate(array $attributes)
     * @method static _TeamCollection|Team[] fromQuery(string $query, array $bindings = [])
     * @method static _TeamCollection|Team[] get(array $columns = ['*'])
     * @method static int getCountForPagination(array $columns = ['*'])
     * @method static Team getModel()
     * @method static Team[] getModels(array $columns = ['*'])
     * @method static _TeamQueryBuilder getQuery()
     * @method static _TeamQueryBuilder groupBy(array $groups)
     * @method static bool hasMacro(string $name)
     * @method static _TeamCollection|Team[] hydrate(array $items)
     * @method static int increment(string $column, float|int $amount = 1, array $extra = [])
     * @method static bool insert(array $values)
     * @method static int insertGetId(array $values, null|string $sequence = null)
     * @method static int insertOrIgnore(array $values)
     * @method static bool insertUsing(array $columns, \Closure|\Illuminate\Database\Query\Builder|string $query)
     * @method static _TeamQueryBuilder join(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null, string $type = 'inner', bool $where = false)
     * @method static _TeamQueryBuilder latest(string $column = null)
     * @method static _TeamQueryBuilder leftJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @method static _TeamQueryBuilder limit(int $value)
     * @method static Team make(array $attributes = [])
     * @method static max(string $column)
     * @method static min(string $column)
     * @method static Team newModelInstance(array $attributes = [])
     * @method static int numericAggregate(string $function, array $columns = ['*'])
     * @method static _TeamQueryBuilder offset(int $value)
     * @method static _TeamQueryBuilder oldest(string $column = null)
     * @method static _TeamQueryBuilder orderBy(string $column, string $direction = 'asc')
     * @method static _TeamQueryBuilder orderByDesc(string $column)
     * @method static _TeamQueryBuilder orderByRaw(string $sql, array $bindings = [])
     * @method static _TeamQueryBuilder paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method static _TeamQueryBuilder rightJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @method static _TeamQueryBuilder select(array $columns = ['*'])
     * @method static _TeamQueryBuilder setQuery(\Illuminate\Database\Query\Builder $query)
     * @method static _TeamQueryBuilder simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method static _TeamQueryBuilder skip(int $value)
     * @method static sum(string $column)
     * @method static _TeamQueryBuilder take(int $value)
     * @method static _TeamQueryBuilder tap(callable $callback)
     * @method static _TeamQueryBuilder truncate()
     * @method static _TeamQueryBuilder unless($value, callable $callback, callable|null $default = null)
     * @method static int update(array $values)
     * @method static Team updateOrCreate(array $attributes, array $values = [])
     * @method static bool updateOrInsert(array $attributes, array $values = [])
     * @method static _TeamQueryBuilder when($value, callable $callback, callable|null $default = null)
     * @method static _TeamQueryBuilder where(array|\Closure|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method static _TeamQueryBuilder whereBetween(string $column, array $values, string $boolean = 'and', bool $not = false)
     * @method static _TeamQueryBuilder whereColumn(array|string $first, null|string $operator = null, null|string $second = null, null|string $boolean = 'and')
     * @method static _TeamQueryBuilder whereDate(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _TeamQueryBuilder whereDay(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _TeamQueryBuilder whereDoesntHave(string $relation, \Closure $callback = null)
     * @method static _TeamQueryBuilder whereDoesntHaveMorph(string $relation, array|string $types, \Closure $callback = null)
     * @method static _TeamQueryBuilder whereExists(\Closure $callback, string $boolean = 'and', bool $not = false)
     * @method static _TeamQueryBuilder whereHas(string $relation, \Closure $callback = null, string $operator = '>=', int $count = 1)
     * @method static _TeamQueryBuilder whereHasMorph(string $relation, array|string $types, \Closure $callback = null, string $operator = '>=', int $count = 1)
     * @method static _TeamQueryBuilder whereIn(string $column, $values, string $boolean = 'and', bool $not = false)
     * @method static _TeamQueryBuilder whereIntegerInRaw(string $column, array|Arrayable $values, string $boolean = 'and', bool $not = false)
     * @method static _TeamQueryBuilder whereIntegerNotInRaw(string $column, array|Arrayable $values, string $boolean = 'and')
     * @method static _TeamQueryBuilder whereJsonContains(string $column, $value, string $boolean = 'and', bool $not = false)
     * @method static _TeamQueryBuilder whereJsonDoesntContain(string $column, $value, string $boolean = 'and')
     * @method static _TeamQueryBuilder whereJsonLength(string $column, $operator, $value = null, string $boolean = 'and')
     * @method static _TeamQueryBuilder whereKey($id)
     * @method static _TeamQueryBuilder whereKeyNot($id)
     * @method static _TeamQueryBuilder whereMonth(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _TeamQueryBuilder whereNested(\Closure $callback, string $boolean = 'and')
     * @method static _TeamQueryBuilder whereNotBetween(string $column, array $values, string $boolean = 'and')
     * @method static _TeamQueryBuilder whereNotExists(\Closure $callback, string $boolean = 'and')
     * @method static _TeamQueryBuilder whereNotIn(string $column, $values, string $boolean = 'and')
     * @method static _TeamQueryBuilder whereNotNull(string $column, string $boolean = 'and')
     * @method static _TeamQueryBuilder whereNull(array|string $columns, string $boolean = 'and', bool $not = false)
     * @method static _TeamQueryBuilder whereRaw(string $sql, $bindings = [], string $boolean = 'and')
     * @method static _TeamQueryBuilder whereRowValues(array $columns, string $operator, array $values, string $boolean = 'and')
     * @method static _TeamQueryBuilder whereTime(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _TeamQueryBuilder whereYear(string $column, string $operator, \DateTimeInterface|int|null|string $value = null, string $boolean = 'and')
     * @method static _TeamQueryBuilder with($relations)
     * @method static _TeamQueryBuilder withCount($relations)
     * @method static _TeamQueryBuilder withGlobalScope(string $identifier, \Closure|Scope $scope)
     * @method static _TeamQueryBuilder without($relations)
     * @method static _TeamQueryBuilder withoutGlobalScope(Scope|string $scope)
     * @method static _TeamQueryBuilder withoutGlobalScopes(array $scopes = null)
     */
    class Team extends Model
    {
    }

    /**
     * @property int $id
     * @property int $user_id
     * @property int $team_id
     * @property int $rank
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @method _TeamUserQueryBuilder newModelQuery()
     * @method _TeamUserQueryBuilder newQuery()
     * @method static _TeamUserQueryBuilder query()
     * @method static _TeamUserCollection|TeamUser[] all()
     * @method static _TeamUserQueryBuilder whereId($value)
     * @method static _TeamUserQueryBuilder whereUserId($value)
     * @method static _TeamUserQueryBuilder whereTeamId($value)
     * @method static _TeamUserQueryBuilder whereRank($value)
     * @method static _TeamUserQueryBuilder whereCreatedAt($value)
     * @method static _TeamUserQueryBuilder whereUpdatedAt($value)
     * @method static average(string $column)
     * @method static avg(string $column)
     * @method static bool chunk(int $count, callable $callback)
     * @method static bool chunkById(int $count, callable $callback, null|string $column = null, null|string $alias = null)
     * @method static int count(string $columns = '*')
     * @method static TeamUser create(array $attributes = [])
     * @method static _TeamUserQueryBuilder crossJoin(string $table, \Closure|null|string $first = null, null|string $operator = null, null|string $second = null)
     * @method static _TeamUserCollection|TeamUser[] cursor()
     * @method static int decrement(string $column, float|int $amount = 1, array $extra = [])
     * @method static bool doesntExist()
     * @method static _TeamUserQueryBuilder each(callable $callback, int $count = 1000)
     * @method static bool exists()
     * @method static TeamUser|null find($id, array $columns = ['*'])
     * @method static _TeamUserCollection|TeamUser[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method static TeamUser findOrFail($id, array $columns = ['*'])
     * @method static TeamUser findOrNew($id, array $columns = ['*'])
     * @method static TeamUser first(array $columns = ['*'])
     * @method static TeamUser firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method static TeamUser firstOrCreate(array $attributes, array $values = [])
     * @method static TeamUser firstOrFail(array $columns = ['*'])
     * @method static TeamUser firstOrNew(array $attributes, array $values = [])
     * @method static TeamUser forceCreate(array $attributes)
     * @method static _TeamUserCollection|TeamUser[] fromQuery(string $query, array $bindings = [])
     * @method static _TeamUserCollection|TeamUser[] get(array $columns = ['*'])
     * @method static int getCountForPagination(array $columns = ['*'])
     * @method static TeamUser getModel()
     * @method static TeamUser[] getModels(array $columns = ['*'])
     * @method static _TeamUserQueryBuilder getQuery()
     * @method static _TeamUserQueryBuilder groupBy(array $groups)
     * @method static bool hasMacro(string $name)
     * @method static _TeamUserCollection|TeamUser[] hydrate(array $items)
     * @method static int increment(string $column, float|int $amount = 1, array $extra = [])
     * @method static bool insert(array $values)
     * @method static int insertGetId(array $values, null|string $sequence = null)
     * @method static int insertOrIgnore(array $values)
     * @method static bool insertUsing(array $columns, \Closure|\Illuminate\Database\Query\Builder|string $query)
     * @method static _TeamUserQueryBuilder join(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null, string $type = 'inner', bool $where = false)
     * @method static _TeamUserQueryBuilder latest(string $column = null)
     * @method static _TeamUserQueryBuilder leftJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @method static _TeamUserQueryBuilder limit(int $value)
     * @method static TeamUser make(array $attributes = [])
     * @method static max(string $column)
     * @method static min(string $column)
     * @method static TeamUser newModelInstance(array $attributes = [])
     * @method static int numericAggregate(string $function, array $columns = ['*'])
     * @method static _TeamUserQueryBuilder offset(int $value)
     * @method static _TeamUserQueryBuilder oldest(string $column = null)
     * @method static _TeamUserQueryBuilder orderBy(string $column, string $direction = 'asc')
     * @method static _TeamUserQueryBuilder orderByDesc(string $column)
     * @method static _TeamUserQueryBuilder orderByRaw(string $sql, array $bindings = [])
     * @method static _TeamUserQueryBuilder paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method static _TeamUserQueryBuilder rightJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @method static _TeamUserQueryBuilder select(array $columns = ['*'])
     * @method static _TeamUserQueryBuilder setQuery(\Illuminate\Database\Query\Builder $query)
     * @method static _TeamUserQueryBuilder simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method static _TeamUserQueryBuilder skip(int $value)
     * @method static sum(string $column)
     * @method static _TeamUserQueryBuilder take(int $value)
     * @method static _TeamUserQueryBuilder tap(callable $callback)
     * @method static _TeamUserQueryBuilder truncate()
     * @method static _TeamUserQueryBuilder unless($value, callable $callback, callable|null $default = null)
     * @method static int update(array $values)
     * @method static TeamUser updateOrCreate(array $attributes, array $values = [])
     * @method static bool updateOrInsert(array $attributes, array $values = [])
     * @method static _TeamUserQueryBuilder when($value, callable $callback, callable|null $default = null)
     * @method static _TeamUserQueryBuilder where(array|\Closure|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method static _TeamUserQueryBuilder whereBetween(string $column, array $values, string $boolean = 'and', bool $not = false)
     * @method static _TeamUserQueryBuilder whereColumn(array|string $first, null|string $operator = null, null|string $second = null, null|string $boolean = 'and')
     * @method static _TeamUserQueryBuilder whereDate(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _TeamUserQueryBuilder whereDay(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _TeamUserQueryBuilder whereDoesntHave(string $relation, \Closure $callback = null)
     * @method static _TeamUserQueryBuilder whereDoesntHaveMorph(string $relation, array|string $types, \Closure $callback = null)
     * @method static _TeamUserQueryBuilder whereExists(\Closure $callback, string $boolean = 'and', bool $not = false)
     * @method static _TeamUserQueryBuilder whereHas(string $relation, \Closure $callback = null, string $operator = '>=', int $count = 1)
     * @method static _TeamUserQueryBuilder whereHasMorph(string $relation, array|string $types, \Closure $callback = null, string $operator = '>=', int $count = 1)
     * @method static _TeamUserQueryBuilder whereIn(string $column, $values, string $boolean = 'and', bool $not = false)
     * @method static _TeamUserQueryBuilder whereIntegerInRaw(string $column, array|Arrayable $values, string $boolean = 'and', bool $not = false)
     * @method static _TeamUserQueryBuilder whereIntegerNotInRaw(string $column, array|Arrayable $values, string $boolean = 'and')
     * @method static _TeamUserQueryBuilder whereJsonContains(string $column, $value, string $boolean = 'and', bool $not = false)
     * @method static _TeamUserQueryBuilder whereJsonDoesntContain(string $column, $value, string $boolean = 'and')
     * @method static _TeamUserQueryBuilder whereJsonLength(string $column, $operator, $value = null, string $boolean = 'and')
     * @method static _TeamUserQueryBuilder whereKey($id)
     * @method static _TeamUserQueryBuilder whereKeyNot($id)
     * @method static _TeamUserQueryBuilder whereMonth(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _TeamUserQueryBuilder whereNested(\Closure $callback, string $boolean = 'and')
     * @method static _TeamUserQueryBuilder whereNotBetween(string $column, array $values, string $boolean = 'and')
     * @method static _TeamUserQueryBuilder whereNotExists(\Closure $callback, string $boolean = 'and')
     * @method static _TeamUserQueryBuilder whereNotIn(string $column, $values, string $boolean = 'and')
     * @method static _TeamUserQueryBuilder whereNotNull(string $column, string $boolean = 'and')
     * @method static _TeamUserQueryBuilder whereNull(array|string $columns, string $boolean = 'and', bool $not = false)
     * @method static _TeamUserQueryBuilder whereRaw(string $sql, $bindings = [], string $boolean = 'and')
     * @method static _TeamUserQueryBuilder whereRowValues(array $columns, string $operator, array $values, string $boolean = 'and')
     * @method static _TeamUserQueryBuilder whereTime(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _TeamUserQueryBuilder whereYear(string $column, string $operator, \DateTimeInterface|int|null|string $value = null, string $boolean = 'and')
     * @method static _TeamUserQueryBuilder with($relations)
     * @method static _TeamUserQueryBuilder withCount($relations)
     * @method static _TeamUserQueryBuilder withGlobalScope(string $identifier, \Closure|Scope $scope)
     * @method static _TeamUserQueryBuilder without($relations)
     * @method static _TeamUserQueryBuilder withoutGlobalScope(Scope|string $scope)
     * @method static _TeamUserQueryBuilder withoutGlobalScopes(array $scopes = null)
     */
    class TeamUser extends Model
    {
    }

    /**
     * @property int $id
     * @property string $name
     * @property string $email
     * @property int $pay_id
     * @property string|null $contact
     * @property string|null $image
     * @property int $contact_id
     * @property string|null $paymoney
     * @property Carbon|null $email_verified_at
     * @property string $password
     * @property string|null $remember_token
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property Contact $cont
     * @method BelongsTo|_ContactQueryBuilder cont()
     * @property Contact $contact
     * @method BelongsTo|_ContactQueryBuilder contact()
     * @property _MoneyCollection|Money[] $mymoney
     * @method HasMany|_MoneyQueryBuilder mymoney()
     * @property _DatabaseNotificationCollection|DatabaseNotification[] $notifications
     * @method MorphToMany|_DatabaseNotificationQueryBuilder notifications()
     * @property Pay $pay
     * @method BelongsTo|_PayQueryBuilder pay()
     * @property _RoleCollection|Role[] $roles
     * @method BelongsToMany|_RoleQueryBuilder roles()
     * @property _StoryCollection|Story[] $stories
     * @method HasMany|_StoryQueryBuilder stories()
     * @property _TeamCollection|Team[] $teams
     * @method BelongsToMany|_TeamQueryBuilder teams()
     * @method _UserQueryBuilder newModelQuery()
     * @method _UserQueryBuilder newQuery()
     * @method static _UserQueryBuilder query()
     * @method static _UserCollection|User[] all()
     * @method static _UserQueryBuilder whereId($value)
     * @method static _UserQueryBuilder whereName($value)
     * @method static _UserQueryBuilder whereEmail($value)
     * @method static _UserQueryBuilder wherePayId($value)
     * @method static _UserQueryBuilder whereContact($value)
     * @method static _UserQueryBuilder whereImage($value)
     * @method static _UserQueryBuilder whereContactId($value)
     * @method static _UserQueryBuilder wherePaymoney($value)
     * @method static _UserQueryBuilder whereEmailVerifiedAt($value)
     * @method static _UserQueryBuilder wherePassword($value)
     * @method static _UserQueryBuilder whereRememberToken($value)
     * @method static _UserQueryBuilder whereCreatedAt($value)
     * @method static _UserQueryBuilder whereUpdatedAt($value)
     * @method static average(string $column)
     * @method static avg(string $column)
     * @method static bool chunk(int $count, callable $callback)
     * @method static bool chunkById(int $count, callable $callback, null|string $column = null, null|string $alias = null)
     * @method static int count(string $columns = '*')
     * @method static User create(array $attributes = [])
     * @method static _UserQueryBuilder crossJoin(string $table, \Closure|null|string $first = null, null|string $operator = null, null|string $second = null)
     * @method static _UserCollection|User[] cursor()
     * @method static int decrement(string $column, float|int $amount = 1, array $extra = [])
     * @method static bool doesntExist()
     * @method static _UserQueryBuilder each(callable $callback, int $count = 1000)
     * @method static bool exists()
     * @method static User|null find($id, array $columns = ['*'])
     * @method static _UserCollection|User[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method static User findOrFail($id, array $columns = ['*'])
     * @method static User findOrNew($id, array $columns = ['*'])
     * @method static User first(array $columns = ['*'])
     * @method static User firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method static User firstOrCreate(array $attributes, array $values = [])
     * @method static User firstOrFail(array $columns = ['*'])
     * @method static User firstOrNew(array $attributes, array $values = [])
     * @method static User forceCreate(array $attributes)
     * @method static _UserCollection|User[] fromQuery(string $query, array $bindings = [])
     * @method static _UserCollection|User[] get(array $columns = ['*'])
     * @method static int getCountForPagination(array $columns = ['*'])
     * @method static User getModel()
     * @method static User[] getModels(array $columns = ['*'])
     * @method static _UserQueryBuilder getQuery()
     * @method static _UserQueryBuilder groupBy(array $groups)
     * @method static bool hasMacro(string $name)
     * @method static _UserCollection|User[] hydrate(array $items)
     * @method static int increment(string $column, float|int $amount = 1, array $extra = [])
     * @method static bool insert(array $values)
     * @method static int insertGetId(array $values, null|string $sequence = null)
     * @method static int insertOrIgnore(array $values)
     * @method static bool insertUsing(array $columns, \Closure|\Illuminate\Database\Query\Builder|string $query)
     * @method static _UserQueryBuilder join(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null, string $type = 'inner', bool $where = false)
     * @method static _UserQueryBuilder latest(string $column = null)
     * @method static _UserQueryBuilder leftJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @method static _UserQueryBuilder limit(int $value)
     * @method static User make(array $attributes = [])
     * @method static max(string $column)
     * @method static min(string $column)
     * @method static User newModelInstance(array $attributes = [])
     * @method static int numericAggregate(string $function, array $columns = ['*'])
     * @method static _UserQueryBuilder offset(int $value)
     * @method static _UserQueryBuilder oldest(string $column = null)
     * @method static _UserQueryBuilder orderBy(string $column, string $direction = 'asc')
     * @method static _UserQueryBuilder orderByDesc(string $column)
     * @method static _UserQueryBuilder orderByRaw(string $sql, array $bindings = [])
     * @method static _UserQueryBuilder paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method static _UserQueryBuilder rightJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @method static _UserQueryBuilder select(array $columns = ['*'])
     * @method static _UserQueryBuilder setQuery(\Illuminate\Database\Query\Builder $query)
     * @method static _UserQueryBuilder simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method static _UserQueryBuilder skip(int $value)
     * @method static sum(string $column)
     * @method static _UserQueryBuilder take(int $value)
     * @method static _UserQueryBuilder tap(callable $callback)
     * @method static _UserQueryBuilder truncate()
     * @method static _UserQueryBuilder unless($value, callable $callback, callable|null $default = null)
     * @method static int update(array $values)
     * @method static User updateOrCreate(array $attributes, array $values = [])
     * @method static bool updateOrInsert(array $attributes, array $values = [])
     * @method static _UserQueryBuilder when($value, callable $callback, callable|null $default = null)
     * @method static _UserQueryBuilder where(array|\Closure|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method static _UserQueryBuilder whereBetween(string $column, array $values, string $boolean = 'and', bool $not = false)
     * @method static _UserQueryBuilder whereColumn(array|string $first, null|string $operator = null, null|string $second = null, null|string $boolean = 'and')
     * @method static _UserQueryBuilder whereDate(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _UserQueryBuilder whereDay(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _UserQueryBuilder whereDoesntHave(string $relation, \Closure $callback = null)
     * @method static _UserQueryBuilder whereDoesntHaveMorph(string $relation, array|string $types, \Closure $callback = null)
     * @method static _UserQueryBuilder whereExists(\Closure $callback, string $boolean = 'and', bool $not = false)
     * @method static _UserQueryBuilder whereHas(string $relation, \Closure $callback = null, string $operator = '>=', int $count = 1)
     * @method static _UserQueryBuilder whereHasMorph(string $relation, array|string $types, \Closure $callback = null, string $operator = '>=', int $count = 1)
     * @method static _UserQueryBuilder whereIn(string $column, $values, string $boolean = 'and', bool $not = false)
     * @method static _UserQueryBuilder whereIntegerInRaw(string $column, array|Arrayable $values, string $boolean = 'and', bool $not = false)
     * @method static _UserQueryBuilder whereIntegerNotInRaw(string $column, array|Arrayable $values, string $boolean = 'and')
     * @method static _UserQueryBuilder whereJsonContains(string $column, $value, string $boolean = 'and', bool $not = false)
     * @method static _UserQueryBuilder whereJsonDoesntContain(string $column, $value, string $boolean = 'and')
     * @method static _UserQueryBuilder whereJsonLength(string $column, $operator, $value = null, string $boolean = 'and')
     * @method static _UserQueryBuilder whereKey($id)
     * @method static _UserQueryBuilder whereKeyNot($id)
     * @method static _UserQueryBuilder whereMonth(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _UserQueryBuilder whereNested(\Closure $callback, string $boolean = 'and')
     * @method static _UserQueryBuilder whereNotBetween(string $column, array $values, string $boolean = 'and')
     * @method static _UserQueryBuilder whereNotExists(\Closure $callback, string $boolean = 'and')
     * @method static _UserQueryBuilder whereNotIn(string $column, $values, string $boolean = 'and')
     * @method static _UserQueryBuilder whereNotNull(string $column, string $boolean = 'and')
     * @method static _UserQueryBuilder whereNull(array|string $columns, string $boolean = 'and', bool $not = false)
     * @method static _UserQueryBuilder whereRaw(string $sql, $bindings = [], string $boolean = 'and')
     * @method static _UserQueryBuilder whereRowValues(array $columns, string $operator, array $values, string $boolean = 'and')
     * @method static _UserQueryBuilder whereTime(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _UserQueryBuilder whereYear(string $column, string $operator, \DateTimeInterface|int|null|string $value = null, string $boolean = 'and')
     * @method static _UserQueryBuilder with($relations)
     * @method static _UserQueryBuilder withCount($relations)
     * @method static _UserQueryBuilder withGlobalScope(string $identifier, \Closure|Scope $scope)
     * @method static _UserQueryBuilder without($relations)
     * @method static _UserQueryBuilder withoutGlobalScope(Scope|string $scope)
     * @method static _UserQueryBuilder withoutGlobalScopes(array $scopes = null)
     */
    class User extends Model
    {
    }
}

namespace Illuminate\Notifications {

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\Relations\MorphTo;
    use Illuminate\Database\Eloquent\Scope;
    use LaravelIdea\Helper\Illuminate\Notifications\_DatabaseNotificationCollection;
    use LaravelIdea\Helper\Illuminate\Notifications\_DatabaseNotificationQueryBuilder;

    /**
     * @property Model $notifiable
     * @method MorphTo notifiable()
     * @method _DatabaseNotificationQueryBuilder newModelQuery()
     * @method _DatabaseNotificationQueryBuilder newQuery()
     * @method static _DatabaseNotificationQueryBuilder query()
     * @method static _DatabaseNotificationCollection|DatabaseNotification[] all()
     * @method static average(string $column)
     * @method static avg(string $column)
     * @method static bool chunk(int $count, callable $callback)
     * @method static bool chunkById(int $count, callable $callback, null|string $column = null, null|string $alias = null)
     * @method static int count(string $columns = '*')
     * @method static DatabaseNotification create(array $attributes = [])
     * @method static _DatabaseNotificationQueryBuilder crossJoin(string $table, \Closure|null|string $first = null, null|string $operator = null, null|string $second = null)
     * @method static _DatabaseNotificationCollection|DatabaseNotification[] cursor()
     * @method static int decrement(string $column, float|int $amount = 1, array $extra = [])
     * @method static bool doesntExist()
     * @method static _DatabaseNotificationQueryBuilder each(callable $callback, int $count = 1000)
     * @method static bool exists()
     * @method static DatabaseNotification|null find($id, array $columns = ['*'])
     * @method static _DatabaseNotificationCollection|DatabaseNotification[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method static DatabaseNotification findOrFail($id, array $columns = ['*'])
     * @method static DatabaseNotification findOrNew($id, array $columns = ['*'])
     * @method static DatabaseNotification first(array $columns = ['*'])
     * @method static DatabaseNotification firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method static DatabaseNotification firstOrCreate(array $attributes, array $values = [])
     * @method static DatabaseNotification firstOrFail(array $columns = ['*'])
     * @method static DatabaseNotification firstOrNew(array $attributes, array $values = [])
     * @method static DatabaseNotification forceCreate(array $attributes)
     * @method static _DatabaseNotificationCollection|DatabaseNotification[] fromQuery(string $query, array $bindings = [])
     * @method static _DatabaseNotificationCollection|DatabaseNotification[] get(array $columns = ['*'])
     * @method static int getCountForPagination(array $columns = ['*'])
     * @method static DatabaseNotification getModel()
     * @method static DatabaseNotification[] getModels(array $columns = ['*'])
     * @method static _DatabaseNotificationQueryBuilder getQuery()
     * @method static _DatabaseNotificationQueryBuilder groupBy(array $groups)
     * @method static bool hasMacro(string $name)
     * @method static _DatabaseNotificationCollection|DatabaseNotification[] hydrate(array $items)
     * @method static int increment(string $column, float|int $amount = 1, array $extra = [])
     * @method static bool insert(array $values)
     * @method static int insertGetId(array $values, null|string $sequence = null)
     * @method static int insertOrIgnore(array $values)
     * @method static bool insertUsing(array $columns, \Closure|\Illuminate\Database\Query\Builder|string $query)
     * @method static _DatabaseNotificationQueryBuilder join(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null, string $type = 'inner', bool $where = false)
     * @method static _DatabaseNotificationQueryBuilder latest(string $column = null)
     * @method static _DatabaseNotificationQueryBuilder leftJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @method static _DatabaseNotificationQueryBuilder limit(int $value)
     * @method static DatabaseNotification make(array $attributes = [])
     * @method static max(string $column)
     * @method static min(string $column)
     * @method static DatabaseNotification newModelInstance(array $attributes = [])
     * @method static int numericAggregate(string $function, array $columns = ['*'])
     * @method static _DatabaseNotificationQueryBuilder offset(int $value)
     * @method static _DatabaseNotificationQueryBuilder oldest(string $column = null)
     * @method static _DatabaseNotificationQueryBuilder orderBy(string $column, string $direction = 'asc')
     * @method static _DatabaseNotificationQueryBuilder orderByDesc(string $column)
     * @method static _DatabaseNotificationQueryBuilder orderByRaw(string $sql, array $bindings = [])
     * @method static _DatabaseNotificationQueryBuilder paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method static _DatabaseNotificationQueryBuilder rightJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @method static _DatabaseNotificationQueryBuilder select(array $columns = ['*'])
     * @method static _DatabaseNotificationQueryBuilder setQuery(\Illuminate\Database\Query\Builder $query)
     * @method static _DatabaseNotificationQueryBuilder simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method static _DatabaseNotificationQueryBuilder skip(int $value)
     * @method static sum(string $column)
     * @method static _DatabaseNotificationQueryBuilder take(int $value)
     * @method static _DatabaseNotificationQueryBuilder tap(callable $callback)
     * @method static _DatabaseNotificationQueryBuilder truncate()
     * @method static _DatabaseNotificationQueryBuilder unless($value, callable $callback, callable|null $default = null)
     * @method static int update(array $values)
     * @method static DatabaseNotification updateOrCreate(array $attributes, array $values = [])
     * @method static bool updateOrInsert(array $attributes, array $values = [])
     * @method static _DatabaseNotificationQueryBuilder when($value, callable $callback, callable|null $default = null)
     * @method static _DatabaseNotificationQueryBuilder where(array|\Closure|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method static _DatabaseNotificationQueryBuilder whereBetween(string $column, array $values, string $boolean = 'and', bool $not = false)
     * @method static _DatabaseNotificationQueryBuilder whereColumn(array|string $first, null|string $operator = null, null|string $second = null, null|string $boolean = 'and')
     * @method static _DatabaseNotificationQueryBuilder whereDate(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _DatabaseNotificationQueryBuilder whereDay(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _DatabaseNotificationQueryBuilder whereDoesntHave(string $relation, \Closure $callback = null)
     * @method static _DatabaseNotificationQueryBuilder whereDoesntHaveMorph(string $relation, array|string $types, \Closure $callback = null)
     * @method static _DatabaseNotificationQueryBuilder whereExists(\Closure $callback, string $boolean = 'and', bool $not = false)
     * @method static _DatabaseNotificationQueryBuilder whereHas(string $relation, \Closure $callback = null, string $operator = '>=', int $count = 1)
     * @method static _DatabaseNotificationQueryBuilder whereHasMorph(string $relation, array|string $types, \Closure $callback = null, string $operator = '>=', int $count = 1)
     * @method static _DatabaseNotificationQueryBuilder whereIn(string $column, $values, string $boolean = 'and', bool $not = false)
     * @method static _DatabaseNotificationQueryBuilder whereIntegerInRaw(string $column, array|Arrayable $values, string $boolean = 'and', bool $not = false)
     * @method static _DatabaseNotificationQueryBuilder whereIntegerNotInRaw(string $column, array|Arrayable $values, string $boolean = 'and')
     * @method static _DatabaseNotificationQueryBuilder whereJsonContains(string $column, $value, string $boolean = 'and', bool $not = false)
     * @method static _DatabaseNotificationQueryBuilder whereJsonDoesntContain(string $column, $value, string $boolean = 'and')
     * @method static _DatabaseNotificationQueryBuilder whereJsonLength(string $column, $operator, $value = null, string $boolean = 'and')
     * @method static _DatabaseNotificationQueryBuilder whereKey($id)
     * @method static _DatabaseNotificationQueryBuilder whereKeyNot($id)
     * @method static _DatabaseNotificationQueryBuilder whereMonth(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _DatabaseNotificationQueryBuilder whereNested(\Closure $callback, string $boolean = 'and')
     * @method static _DatabaseNotificationQueryBuilder whereNotBetween(string $column, array $values, string $boolean = 'and')
     * @method static _DatabaseNotificationQueryBuilder whereNotExists(\Closure $callback, string $boolean = 'and')
     * @method static _DatabaseNotificationQueryBuilder whereNotIn(string $column, $values, string $boolean = 'and')
     * @method static _DatabaseNotificationQueryBuilder whereNotNull(string $column, string $boolean = 'and')
     * @method static _DatabaseNotificationQueryBuilder whereNull(array|string $columns, string $boolean = 'and', bool $not = false)
     * @method static _DatabaseNotificationQueryBuilder whereRaw(string $sql, $bindings = [], string $boolean = 'and')
     * @method static _DatabaseNotificationQueryBuilder whereRowValues(array $columns, string $operator, array $values, string $boolean = 'and')
     * @method static _DatabaseNotificationQueryBuilder whereTime(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _DatabaseNotificationQueryBuilder whereYear(string $column, string $operator, \DateTimeInterface|int|null|string $value = null, string $boolean = 'and')
     * @method static _DatabaseNotificationQueryBuilder with($relations)
     * @method static _DatabaseNotificationQueryBuilder withCount($relations)
     * @method static _DatabaseNotificationQueryBuilder withGlobalScope(string $identifier, \Closure|Scope $scope)
     * @method static _DatabaseNotificationQueryBuilder without($relations)
     * @method static _DatabaseNotificationQueryBuilder withoutGlobalScope(Scope|string $scope)
     * @method static _DatabaseNotificationQueryBuilder withoutGlobalScopes(array $scopes = null)
     */
    class DatabaseNotification extends Model
    {
    }
}

namespace LaravelIdea\Helper {

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Database\Eloquent\Builder;
    use Illuminate\Support\Collection;

    /**
     * @see \Illuminate\Database\Query\Builder::select
     * @method $this select(array $columns = ['*'])
     * @see \Illuminate\Database\Query\Builder::paginate
     * @method $this paginate(int $perPage = 15, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @see \Illuminate\Database\Query\Builder::addSelect
     * @method $this addSelect(array $column)
     * @see \Illuminate\Database\Concerns\BuildsQueries::when
     * @method $this when($value, callable $callback, callable|null $default = null)
     * @see \Illuminate\Database\Query\Builder::whereIn
     * @method $this whereIn(string $column, $values, string $boolean = 'and', bool $not = false)
     * @see \Illuminate\Database\Query\Builder::orWhereExists
     * @method $this orWhereExists(\Closure $callback, bool $not = false)
     * @see \Illuminate\Database\Query\Builder::whereJsonLength
     * @method $this whereJsonLength(string $column, $operator, $value = null, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::orWhereNotIn
     * @method $this orWhereNotIn(string $column, $values)
     * @see \Illuminate\Database\Query\Builder::selectRaw
     * @method $this selectRaw(string $expression, array $bindings = [])
     * @see \Illuminate\Database\Query\Builder::truncate
     * @method $this truncate()
     * @see \Illuminate\Database\Query\Builder::lock
     * @method $this lock(bool|string $value = true)
     * @see \Illuminate\Database\Query\Builder::insertOrIgnore
     * @method $this insertOrIgnore(array $values)
     * @see \Illuminate\Database\Query\Builder::join
     * @method $this join(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null, string $type = 'inner', bool $where = false)
     * @see \Illuminate\Database\Query\Builder::unionAll
     * @method $this unionAll(\Closure|\Illuminate\Database\Query\Builder $query)
     * @see \Illuminate\Database\Query\Builder::whereMonth
     * @method $this whereMonth(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::having
     * @method $this having(string $column, null|string $operator = null, null|string $value = null, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::orWhereNull
     * @method $this orWhereNull(string $column)
     * @see \Illuminate\Database\Query\Builder::whereNested
     * @method $this whereNested(\Closure $callback, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::joinWhere
     * @method $this joinWhere(string $table, \Closure|string $first, string $operator, string $second, string $type = 'inner')
     * @see \Illuminate\Database\Query\Builder::orWhereJsonContains
     * @method $this orWhereJsonContains(string $column, $value)
     * @see \Illuminate\Database\Query\Builder::raw
     * @method $this raw($value)
     * @see \Illuminate\Database\Query\Builder::orderBy
     * @method $this orderBy(string $column, string $direction = 'asc')
     * @see \Illuminate\Database\Query\Builder::orWhereRowValues
     * @method $this orWhereRowValues(array $columns, string $operator, array $values)
     * @see \Illuminate\Database\Concerns\BuildsQueries::each
     * @method $this each(callable $callback, int $count = 1000)
     * @see \Illuminate\Database\Query\Builder::setBindings
     * @method $this setBindings(array $bindings, string $type = 'where')
     * @see \Illuminate\Database\Query\Builder::orWhereJsonLength
     * @method $this orWhereJsonLength(string $column, $operator, $value = null)
     * @see \Illuminate\Database\Query\Builder::whereRowValues
     * @method $this whereRowValues(array $columns, string $operator, array $values, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::useWritePdo
     * @method $this useWritePdo()
     * @see \Illuminate\Database\Query\Builder::orWhereNotExists
     * @method $this orWhereNotExists(\Closure $callback)
     * @see \Illuminate\Database\Query\Builder::orWhereIn
     * @method $this orWhereIn(string $column, $values)
     * @see \Illuminate\Database\Query\Builder::newQuery
     * @method $this newQuery()
     * @see \Illuminate\Database\Query\Builder::rightJoinSub
     * @method $this rightJoinSub(\Closure|\Illuminate\Database\Query\Builder|string $query, string $as, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @see \Illuminate\Database\Query\Builder::crossJoin
     * @method $this crossJoin(string $table, \Closure|null|string $first = null, null|string $operator = null, null|string $second = null)
     * @see \Illuminate\Database\Query\Builder::orderByDesc
     * @method $this orderByDesc(string $column)
     * @see \Illuminate\Database\Query\Builder::orWhereNotNull
     * @method $this orWhereNotNull(string $column)
     * @see \Illuminate\Database\Query\Builder::average
     * @method $this average(string $column)
     * @see \Illuminate\Database\Query\Builder::getProcessor
     * @method $this getProcessor()
     * @see \Illuminate\Database\Query\Builder::increment
     * @method $this increment(string $column, float|int $amount = 1, array $extra = [])
     * @see \Illuminate\Database\Query\Builder::havingRaw
     * @method $this havingRaw(string $sql, array $bindings = [], string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::skip
     * @method $this skip(int $value)
     * @see \Illuminate\Database\Query\Builder::sum
     * @method $this sum(string $column)
     * @see \Illuminate\Database\Query\Builder::leftJoinWhere
     * @method $this leftJoinWhere(string $table, \Closure|string $first, string $operator, string $second)
     * @see \Illuminate\Database\Query\Builder::orWhereColumn
     * @method $this orWhereColumn(array|string $first, null|string $operator = null, null|string $second = null)
     * @see \Illuminate\Database\Query\Builder::getRawBindings
     * @method $this getRawBindings()
     * @see \Illuminate\Database\Query\Builder::min
     * @method $this min(string $column)
     * @see \Illuminate\Support\Traits\Macroable::hasMacro
     * @method $this hasMacro(string $name)
     * @see \Illuminate\Database\Query\Builder::whereNotExists
     * @method $this whereNotExists(\Closure $callback, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::whereIntegerInRaw
     * @method $this whereIntegerInRaw(string $column, array|Arrayable $values, string $boolean = 'and', bool $not = false)
     * @see \Illuminate\Database\Concerns\BuildsQueries::unless
     * @method $this unless($value, callable $callback, callable|null $default = null)
     * @see \Illuminate\Database\Query\Builder::whereDay
     * @method $this whereDay(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::get
     * @method $this get(array|string $columns = ['*'])
     * @see \Illuminate\Database\Query\Builder::whereNotIn
     * @method $this whereNotIn(string $column, $values, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::whereTime
     * @method $this whereTime(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::where
     * @method $this where(array|\Closure|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::latest
     * @method $this latest(string $column = 'created_at')
     * @see \Illuminate\Database\Query\Builder::forNestedWhere
     * @method $this forNestedWhere()
     * @see \Illuminate\Database\Query\Builder::insertUsing
     * @method $this insertUsing(array $columns, \Closure|\Illuminate\Database\Query\Builder|string $query)
     * @see \Illuminate\Database\Query\Builder::max
     * @method $this max(string $column)
     * @see \Illuminate\Database\Query\Builder::rightJoinWhere
     * @method $this rightJoinWhere(string $table, \Closure|string $first, string $operator, string $second)
     * @see \Illuminate\Database\Query\Builder::whereExists
     * @method $this whereExists(\Closure $callback, string $boolean = 'and', bool $not = false)
     * @see \Illuminate\Database\Query\Builder::inRandomOrder
     * @method $this inRandomOrder(string $seed = '')
     * @see \Illuminate\Database\Query\Builder::havingBetween
     * @method $this havingBetween(string $column, array $values, string $boolean = 'and', bool $not = false)
     * @see \Illuminate\Database\Query\Builder::union
     * @method $this union(\Closure|\Illuminate\Database\Query\Builder $query, bool $all = false)
     * @see \Illuminate\Database\Query\Builder::groupBy
     * @method $this groupBy(array $groups)
     * @see \Illuminate\Database\Query\Builder::orWhereYear
     * @method $this orWhereYear(string $column, string $operator, \DateTimeInterface|int|null|string $value = null)
     * @see \Illuminate\Database\Query\Builder::orWhereDay
     * @method $this orWhereDay(string $column, string $operator, \DateTimeInterface|null|string $value = null)
     * @see \Illuminate\Database\Query\Builder::chunkById
     * @method $this chunkById(int $count, callable $callback, string $column = 'id', null|string $alias = null)
     * @see \Illuminate\Database\Query\Builder::joinSub
     * @method $this joinSub(\Closure|\Illuminate\Database\Query\Builder|string $query, string $as, \Closure|string $first, null|string $operator = null, null|string $second = null, string $type = 'inner', bool $where = false)
     * @see \Illuminate\Database\Query\Builder::whereDate
     * @method $this whereDate(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::whereJsonDoesntContain
     * @method $this whereJsonDoesntContain(string $column, $value, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::oldest
     * @method $this oldest(string $column = 'created_at')
     * @see \Illuminate\Database\Query\Builder::decrement
     * @method $this decrement(string $column, float|int $amount = 1, array $extra = [])
     * @see \Illuminate\Database\Query\Builder::forPageAfterId
     * @method $this forPageAfterId(int $perPage = 15, int|null $lastId = 0, string $column = 'id')
     * @see \Illuminate\Database\Query\Builder::forPage
     * @method $this forPage(int $page, int $perPage = 15)
     * @see \Illuminate\Database\Query\Builder::exists
     * @method $this exists()
     * @see \Illuminate\Support\Traits\Macroable::macroCall
     * @method $this macroCall(string $method, array $parameters)
     * @see \Illuminate\Database\Query\Builder::selectSub
     * @method $this selectSub(\Closure|\Illuminate\Database\Query\Builder|string $query, string $as)
     * @see \Illuminate\Database\Query\Builder::pluck
     * @method $this pluck(string $column, null|string $key = null)
     * @see \Illuminate\Database\Concerns\BuildsQueries::first
     * @method $this first(array $columns = ['*'])
     * @see \Illuminate\Database\Query\Builder::dd
     * @method $this dd()
     * @see \Illuminate\Database\Query\Builder::whereColumn
     * @method $this whereColumn(array|string $first, null|string $operator = null, null|string $second = null, null|string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::prepareValueAndOperator
     * @method $this prepareValueAndOperator(string $value, string $operator, bool $useDefault = false)
     * @see \Illuminate\Database\Query\Builder::whereNull
     * @method $this whereNull(array|string $columns, string $boolean = 'and', bool $not = false)
     * @see \Illuminate\Database\Query\Builder::numericAggregate
     * @method $this numericAggregate(string $function, array $columns = ['*'])
     * @see \Illuminate\Database\Query\Builder::whereNotBetween
     * @method $this whereNotBetween(string $column, array $values, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::getConnection
     * @method $this getConnection()
     * @see \Illuminate\Database\Query\Builder::mergeBindings
     * @method $this mergeBindings(\Illuminate\Database\Query\Builder $query)
     * @see \Illuminate\Database\Query\Builder::whereIntegerNotInRaw
     * @method $this whereIntegerNotInRaw(string $column, array|Arrayable $values, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::orWhereRaw
     * @method $this orWhereRaw(string $sql, $bindings = [])
     * @see \Illuminate\Database\Query\Builder::orWhereJsonDoesntContain
     * @method $this orWhereJsonDoesntContain(string $column, $value)
     * @see \Illuminate\Database\Query\Builder::leftJoinSub
     * @method $this leftJoinSub(\Closure|\Illuminate\Database\Query\Builder|string $query, string $as, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @see \Illuminate\Database\Query\Builder::find
     * @method $this find(int|string $id, array $columns = ['*'])
     * @see \Illuminate\Database\Query\Builder::whereJsonContains
     * @method $this whereJsonContains(string $column, $value, string $boolean = 'and', bool $not = false)
     * @see \Illuminate\Database\Query\Builder::limit
     * @method $this limit(int $value)
     * @see \Illuminate\Database\Query\Builder::from
     * @method $this from(string $table)
     * @see \Illuminate\Database\Query\Builder::insertGetId
     * @method $this insertGetId(array $values, null|string $sequence = null)
     * @see \Illuminate\Database\Query\Builder::whereBetween
     * @method $this whereBetween(string $column, array $values, string $boolean = 'and', bool $not = false)
     * @see \Illuminate\Database\Query\Builder::mergeWheres
     * @method $this mergeWheres(array $wheres, array $bindings)
     * @see \Illuminate\Database\Query\Builder::sharedLock
     * @method $this sharedLock()
     * @see \Illuminate\Database\Query\Builder::orderByRaw
     * @method $this orderByRaw(string $sql, array $bindings = [])
     * @see \Illuminate\Database\Concerns\BuildsQueries::tap
     * @method $this tap(callable $callback)
     * @see \Illuminate\Database\Query\Builder::doesntExist
     * @method $this doesntExist()
     * @see \Illuminate\Database\Query\Builder::simplePaginate
     * @method $this simplePaginate(int $perPage = 15, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @see \Illuminate\Database\Query\Builder::offset
     * @method $this offset(int $value)
     * @see \Illuminate\Database\Query\Builder::orWhereMonth
     * @method $this orWhereMonth(string $column, string $operator, \DateTimeInterface|null|string $value = null)
     * @see \Illuminate\Database\Query\Builder::whereNotNull
     * @method $this whereNotNull(string $column, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::count
     * @method $this count(string $columns = '*')
     * @see \Illuminate\Database\Query\Builder::orWhereNotBetween
     * @method $this orWhereNotBetween(string $column, array $values)
     * @see \Illuminate\Database\Query\Builder::fromRaw
     * @method $this fromRaw(string $expression, $bindings = [])
     * @see \Illuminate\Support\Traits\Macroable::mixin
     * @method $this mixin(object $mixin, bool $replace = true)
     * @see \Illuminate\Database\Query\Builder::take
     * @method $this take(int $value)
     * @see \Illuminate\Database\Query\Builder::updateOrInsert
     * @method $this updateOrInsert(array $attributes, array $values = [])
     * @see \Illuminate\Database\Query\Builder::addNestedWhereQuery
     * @method $this addNestedWhereQuery(\Illuminate\Database\Query\Builder $query, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::cursor
     * @method $this cursor()
     * @see \Illuminate\Database\Query\Builder::cloneWithout
     * @method $this cloneWithout(array $properties)
     * @see \Illuminate\Database\Query\Builder::fromSub
     * @method $this fromSub(\Closure|\Illuminate\Database\Query\Builder|string $query, string $as)
     * @see \Illuminate\Database\Query\Builder::rightJoin
     * @method $this rightJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @see \Illuminate\Database\Query\Builder::leftJoin
     * @method $this leftJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @see \Illuminate\Database\Query\Builder::update
     * @method $this update(array $values)
     * @see \Illuminate\Database\Query\Builder::insert
     * @method $this insert(array $values)
     * @see \Illuminate\Database\Query\Builder::distinct
     * @method $this distinct()
     * @see \Illuminate\Database\Concerns\BuildsQueries::chunk
     * @method $this chunk(int $count, callable $callback)
     * @see \Illuminate\Database\Query\Builder::whereYear
     * @method $this whereYear(string $column, string $operator, \DateTimeInterface|int|null|string $value = null, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::getCountForPagination
     * @method $this getCountForPagination(array $columns = ['*'])
     * @see \Illuminate\Database\Query\Builder::delete
     * @method $this delete($id = null)
     * @see \Illuminate\Database\Query\Builder::aggregate
     * @method $this aggregate(string $function, array $columns = ['*'])
     * @see \Illuminate\Database\Query\Builder::orWhereDate
     * @method $this orWhereDate(string $column, string $operator, \DateTimeInterface|null|string $value = null)
     * @see \Illuminate\Database\Query\Builder::avg
     * @method $this avg(string $column)
     * @see \Illuminate\Database\Query\Builder::addBinding
     * @method $this addBinding($value, string $type = 'where')
     * @see \Illuminate\Database\Query\Builder::getGrammar
     * @method $this getGrammar()
     * @see \Illuminate\Database\Query\Builder::lockForUpdate
     * @method $this lockForUpdate()
     * @see \Illuminate\Database\Query\Builder::implode
     * @method $this implode(string $column, string $glue = '')
     * @see \Illuminate\Database\Query\Builder::dump
     * @method $this dump()
     * @see \Illuminate\Database\Query\Builder::value
     * @method $this value(string $column)
     * @see \Illuminate\Database\Query\Builder::cloneWithoutBindings
     * @method $this cloneWithoutBindings(array $except)
     * @see \Illuminate\Database\Query\Builder::addWhereExistsQuery
     * @method $this addWhereExistsQuery(\Illuminate\Database\Query\Builder $query, string $boolean = 'and', bool $not = false)
     * @see \Illuminate\Support\Traits\Macroable::macro
     * @method $this macro(string $name, callable|object $macro)
     * @see \Illuminate\Database\Query\Builder::whereRaw
     * @method $this whereRaw(string $sql, $bindings = [], string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::toSql
     * @method $this toSql()
     * @see \Illuminate\Database\Query\Builder::orHaving
     * @method $this orHaving(string $column, null|string $operator = null, null|string $value = null)
     * @see \Illuminate\Database\Query\Builder::orHavingRaw
     * @method $this orHavingRaw(string $sql, array $bindings = [])
     * @see \Illuminate\Database\Query\Builder::getBindings
     * @method $this getBindings()
     * @see \Illuminate\Database\Query\Builder::forPageBeforeId
     * @method $this forPageBeforeId(int $perPage = 15, int|null $lastId = 0, string $column = 'id')
     * @see \Illuminate\Database\Query\Builder::orWhereTime
     * @method $this orWhereTime(string $column, string $operator, \DateTimeInterface|null|string $value = null)
     * @see \Illuminate\Database\Query\Builder::orWhereBetween
     * @method $this orWhereBetween(string $column, array $values)
     * @see \Illuminate\Database\Query\Builder::orWhere
     * @method $this orWhere(array|\Closure|string $column, $operator = null, $value = null)
     * @see \Illuminate\Database\Query\Builder::dynamicWhere
     * @method $this dynamicWhere(string $method, array $parameters)
     */
    class _BaseBuilder extends Builder
    {
    }

    /**
     * @method Collection mapSpread(callable $callback)
     * @method Collection mapWithKeys(callable $callback)
     * @method Collection zip($items)
     * @method Collection partition(callable|string $key, $operator = null, $value = null)
     * @method Collection mapInto(string $class)
     * @method Collection mapToGroups(callable $callback)
     * @method Collection map(callable $callback)
     * @method Collection groupBy(array|callable|string $groupBy, bool $preserveKeys = false)
     * @method Collection pluck(array|string $value, null|string $key = null)
     * @method Collection pad(int $size, $value)
     * @method Collection split(int $numberOfGroups)
     * @method Collection combine($values)
     * @method Collection countBy(callable|null $callback = null)
     * @method Collection mapToDictionary(callable $callback)
     * @method Collection keys()
     * @method Collection transform(callable $callback)
     * @method Collection flatMap(callable $callback)
     * @method Collection collapse()
     */
    class _BaseCollection extends Collection
    {
    }
}

namespace LaravelIdea\Helper\App {

    use App\Contact;
    use App\Money;
    use App\Navel;
    use App\Pay;
    use App\Role;
    use App\Story;
    use App\Team;
    use App\TeamUser;
    use App\User;
    use Illuminate\Contracts\Support\Arrayable;
    use LaravelIdea\Helper\_BaseBuilder;
    use LaravelIdea\Helper\_BaseCollection;

    /**
     * @method Contact shift()
     * @method Contact pop()
     * @method Contact get($key, $default = null)
     * @method Contact pull($key, $default = null)
     * @method Contact first(callable $callback = null, $default = null)
     * @method Contact firstWhere(string $key, $operator = null, $value = null)
     * @method Contact[] all()
     * @method Contact last(callable $callback = null, $default = null)
     */
    class _ContactCollection extends _BaseCollection
    {
        /**
         * @param int $size
         * @return Contact[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }

    /**
     * @method _ContactQueryBuilder whereId($value)
     * @method _ContactQueryBuilder whereName($value)
     * @method _ContactQueryBuilder whereCreatedAt($value)
     * @method _ContactQueryBuilder whereUpdatedAt($value)
     * @method Contact create(array $attributes = [])
     * @method _ContactCollection|Contact[] cursor()
     * @method Contact|null find($id, array $columns = ['*'])
     * @method _ContactCollection|Contact[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Contact findOrFail($id, array $columns = ['*'])
     * @method Contact findOrNew($id, array $columns = ['*'])
     * @method Contact first(array $columns = ['*'])
     * @method Contact firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Contact firstOrCreate(array $attributes, array $values = [])
     * @method Contact firstOrFail(array $columns = ['*'])
     * @method Contact firstOrNew(array $attributes, array $values = [])
     * @method Contact forceCreate(array $attributes)
     * @method _ContactCollection|Contact[] fromQuery(string $query, array $bindings = [])
     * @method _ContactCollection|Contact[] get(array $columns = ['*'])
     * @method Contact getModel()
     * @method Contact[] getModels(array $columns = ['*'])
     * @method _ContactCollection|Contact[] hydrate(array $items)
     * @method Contact make(array $attributes = [])
     * @method Contact newModelInstance(array $attributes = [])
     * @method Contact updateOrCreate(array $attributes, array $values = [])
     */
    class _ContactQueryBuilder extends _BaseBuilder
    {
    }

    /**
     * @method Money shift()
     * @method Money pop()
     * @method Money get($key, $default = null)
     * @method Money pull($key, $default = null)
     * @method Money first(callable $callback = null, $default = null)
     * @method Money firstWhere(string $key, $operator = null, $value = null)
     * @method Money[] all()
     * @method Money last(callable $callback = null, $default = null)
     */
    class _MoneyCollection extends _BaseCollection
    {
        /**
         * @param int $size
         * @return Money[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }

    /**
     * @method _MoneyQueryBuilder whereId($value)
     * @method _MoneyQueryBuilder whereUserId($value)
     * @method _MoneyQueryBuilder whereMoney($value)
     * @method _MoneyQueryBuilder whereCase($value)
     * @method _MoneyQueryBuilder whereCreatedAt($value)
     * @method _MoneyQueryBuilder whereUpdatedAt($value)
     * @method Money create(array $attributes = [])
     * @method _MoneyCollection|Money[] cursor()
     * @method Money|null find($id, array $columns = ['*'])
     * @method _MoneyCollection|Money[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Money findOrFail($id, array $columns = ['*'])
     * @method Money findOrNew($id, array $columns = ['*'])
     * @method Money first(array $columns = ['*'])
     * @method Money firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Money firstOrCreate(array $attributes, array $values = [])
     * @method Money firstOrFail(array $columns = ['*'])
     * @method Money firstOrNew(array $attributes, array $values = [])
     * @method Money forceCreate(array $attributes)
     * @method _MoneyCollection|Money[] fromQuery(string $query, array $bindings = [])
     * @method _MoneyCollection|Money[] get(array $columns = ['*'])
     * @method Money getModel()
     * @method Money[] getModels(array $columns = ['*'])
     * @method _MoneyCollection|Money[] hydrate(array $items)
     * @method Money make(array $attributes = [])
     * @method Money newModelInstance(array $attributes = [])
     * @method Money updateOrCreate(array $attributes, array $values = [])
     * @method _MoneyQueryBuilder own($user_id = null)
     */
    class _MoneyQueryBuilder extends _BaseBuilder
    {
    }

    /**
     * @method Navel shift()
     * @method Navel pop()
     * @method Navel get($key, $default = null)
     * @method Navel pull($key, $default = null)
     * @method Navel first(callable $callback = null, $default = null)
     * @method Navel firstWhere(string $key, $operator = null, $value = null)
     * @method Navel[] all()
     * @method Navel last(callable $callback = null, $default = null)
     */
    class _NavelCollection extends _BaseCollection
    {
        /**
         * @param int $size
         * @return Navel[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }

    /**
     * @method _NavelQueryBuilder whereId($value)
     * @method _NavelQueryBuilder whereName($value)
     * @method _NavelQueryBuilder whereImage($value)
     * @method _NavelQueryBuilder whereTeamId($value)
     * @method _NavelQueryBuilder wherePrice($value)
     * @method _NavelQueryBuilder whereCreatedAt($value)
     * @method _NavelQueryBuilder whereUpdatedAt($value)
     * @method Navel create(array $attributes = [])
     * @method _NavelCollection|Navel[] cursor()
     * @method Navel|null find($id, array $columns = ['*'])
     * @method _NavelCollection|Navel[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Navel findOrFail($id, array $columns = ['*'])
     * @method Navel findOrNew($id, array $columns = ['*'])
     * @method Navel first(array $columns = ['*'])
     * @method Navel firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Navel firstOrCreate(array $attributes, array $values = [])
     * @method Navel firstOrFail(array $columns = ['*'])
     * @method Navel firstOrNew(array $attributes, array $values = [])
     * @method Navel forceCreate(array $attributes)
     * @method _NavelCollection|Navel[] fromQuery(string $query, array $bindings = [])
     * @method _NavelCollection|Navel[] get(array $columns = ['*'])
     * @method Navel getModel()
     * @method Navel[] getModels(array $columns = ['*'])
     * @method _NavelCollection|Navel[] hydrate(array $items)
     * @method Navel make(array $attributes = [])
     * @method Navel newModelInstance(array $attributes = [])
     * @method Navel updateOrCreate(array $attributes, array $values = [])
     */
    class _NavelQueryBuilder extends _BaseBuilder
    {
    }

    /**
     * @method Pay shift()
     * @method Pay pop()
     * @method Pay get($key, $default = null)
     * @method Pay pull($key, $default = null)
     * @method Pay first(callable $callback = null, $default = null)
     * @method Pay firstWhere(string $key, $operator = null, $value = null)
     * @method Pay[] all()
     * @method Pay last(callable $callback = null, $default = null)
     */
    class _PayCollection extends _BaseCollection
    {
        /**
         * @param int $size
         * @return Pay[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }

    /**
     * @method _PayQueryBuilder whereId($value)
     * @method _PayQueryBuilder whereName($value)
     * @method _PayQueryBuilder whereCreatedAt($value)
     * @method _PayQueryBuilder whereUpdatedAt($value)
     * @method Pay create(array $attributes = [])
     * @method _PayCollection|Pay[] cursor()
     * @method Pay|null find($id, array $columns = ['*'])
     * @method _PayCollection|Pay[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Pay findOrFail($id, array $columns = ['*'])
     * @method Pay findOrNew($id, array $columns = ['*'])
     * @method Pay first(array $columns = ['*'])
     * @method Pay firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Pay firstOrCreate(array $attributes, array $values = [])
     * @method Pay firstOrFail(array $columns = ['*'])
     * @method Pay firstOrNew(array $attributes, array $values = [])
     * @method Pay forceCreate(array $attributes)
     * @method _PayCollection|Pay[] fromQuery(string $query, array $bindings = [])
     * @method _PayCollection|Pay[] get(array $columns = ['*'])
     * @method Pay getModel()
     * @method Pay[] getModels(array $columns = ['*'])
     * @method _PayCollection|Pay[] hydrate(array $items)
     * @method Pay make(array $attributes = [])
     * @method Pay newModelInstance(array $attributes = [])
     * @method Pay updateOrCreate(array $attributes, array $values = [])
     */
    class _PayQueryBuilder extends _BaseBuilder
    {
    }

    /**
     * @method Role shift()
     * @method Role pop()
     * @method Role get($key, $default = null)
     * @method Role pull($key, $default = null)
     * @method Role first(callable $callback = null, $default = null)
     * @method Role firstWhere(string $key, $operator = null, $value = null)
     * @method Role[] all()
     * @method Role last(callable $callback = null, $default = null)
     */
    class _RoleCollection extends _BaseCollection
    {
        /**
         * @param int $size
         * @return Role[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }

    /**
     * @method _RoleQueryBuilder whereId($value)
     * @method _RoleQueryBuilder whereName($value)
     * @method _RoleQueryBuilder whereCreatedAt($value)
     * @method _RoleQueryBuilder whereUpdatedAt($value)
     * @method Role create(array $attributes = [])
     * @method _RoleCollection|Role[] cursor()
     * @method Role|null find($id, array $columns = ['*'])
     * @method _RoleCollection|Role[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Role findOrFail($id, array $columns = ['*'])
     * @method Role findOrNew($id, array $columns = ['*'])
     * @method Role first(array $columns = ['*'])
     * @method Role firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Role firstOrCreate(array $attributes, array $values = [])
     * @method Role firstOrFail(array $columns = ['*'])
     * @method Role firstOrNew(array $attributes, array $values = [])
     * @method Role forceCreate(array $attributes)
     * @method _RoleCollection|Role[] fromQuery(string $query, array $bindings = [])
     * @method _RoleCollection|Role[] get(array $columns = ['*'])
     * @method Role getModel()
     * @method Role[] getModels(array $columns = ['*'])
     * @method _RoleCollection|Role[] hydrate(array $items)
     * @method Role make(array $attributes = [])
     * @method Role newModelInstance(array $attributes = [])
     * @method Role updateOrCreate(array $attributes, array $values = [])
     */
    class _RoleQueryBuilder extends _BaseBuilder
    {
    }

    /**
     * @method Story shift()
     * @method Story pop()
     * @method Story get($key, $default = null)
     * @method Story pull($key, $default = null)
     * @method Story first(callable $callback = null, $default = null)
     * @method Story firstWhere(string $key, $operator = null, $value = null)
     * @method Story[] all()
     * @method Story last(callable $callback = null, $default = null)
     */
    class _StoryCollection extends _BaseCollection
    {
        /**
         * @param int $size
         * @return Story[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }

    /**
     * @method _StoryQueryBuilder whereId($value)
     * @method _StoryQueryBuilder whereUserId($value)
     * @method _StoryQueryBuilder whereStorynum($value)
     * @method _StoryQueryBuilder whereTitle($value)
     * @method _StoryQueryBuilder whereBody($value)
     * @method _StoryQueryBuilder whereNavelId($value)
     * @method _StoryQueryBuilder whereCase($value)
     * @method _StoryQueryBuilder whereDone($value)
     * @method _StoryQueryBuilder whereCreatedAt($value)
     * @method _StoryQueryBuilder whereUpdatedAt($value)
     * @method Story create(array $attributes = [])
     * @method _StoryCollection|Story[] cursor()
     * @method Story|null find($id, array $columns = ['*'])
     * @method _StoryCollection|Story[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Story findOrFail($id, array $columns = ['*'])
     * @method Story findOrNew($id, array $columns = ['*'])
     * @method Story first(array $columns = ['*'])
     * @method Story firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Story firstOrCreate(array $attributes, array $values = [])
     * @method Story firstOrFail(array $columns = ['*'])
     * @method Story firstOrNew(array $attributes, array $values = [])
     * @method Story forceCreate(array $attributes)
     * @method _StoryCollection|Story[] fromQuery(string $query, array $bindings = [])
     * @method _StoryCollection|Story[] get(array $columns = ['*'])
     * @method Story getModel()
     * @method Story[] getModels(array $columns = ['*'])
     * @method _StoryCollection|Story[] hydrate(array $items)
     * @method Story make(array $attributes = [])
     * @method Story newModelInstance(array $attributes = [])
     * @method Story updateOrCreate(array $attributes, array $values = [])
     */
    class _StoryQueryBuilder extends _BaseBuilder
    {
    }

    /**
     * @method Team shift()
     * @method Team pop()
     * @method Team get($key, $default = null)
     * @method Team pull($key, $default = null)
     * @method Team first(callable $callback = null, $default = null)
     * @method Team firstWhere(string $key, $operator = null, $value = null)
     * @method Team[] all()
     * @method Team last(callable $callback = null, $default = null)
     */
    class _TeamCollection extends _BaseCollection
    {
        /**
         * @param int $size
         * @return Team[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }

    /**
     * @method _TeamQueryBuilder whereId($value)
     * @method _TeamQueryBuilder whereName($value)
     * @method _TeamQueryBuilder whereChatcode($value)
     * @method _TeamQueryBuilder whereCreatedAt($value)
     * @method _TeamQueryBuilder whereUpdatedAt($value)
     * @method Team create(array $attributes = [])
     * @method _TeamCollection|Team[] cursor()
     * @method Team|null find($id, array $columns = ['*'])
     * @method _TeamCollection|Team[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Team findOrFail($id, array $columns = ['*'])
     * @method Team findOrNew($id, array $columns = ['*'])
     * @method Team first(array $columns = ['*'])
     * @method Team firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Team firstOrCreate(array $attributes, array $values = [])
     * @method Team firstOrFail(array $columns = ['*'])
     * @method Team firstOrNew(array $attributes, array $values = [])
     * @method Team forceCreate(array $attributes)
     * @method _TeamCollection|Team[] fromQuery(string $query, array $bindings = [])
     * @method _TeamCollection|Team[] get(array $columns = ['*'])
     * @method Team getModel()
     * @method Team[] getModels(array $columns = ['*'])
     * @method _TeamCollection|Team[] hydrate(array $items)
     * @method Team make(array $attributes = [])
     * @method Team newModelInstance(array $attributes = [])
     * @method Team updateOrCreate(array $attributes, array $values = [])
     */
    class _TeamQueryBuilder extends _BaseBuilder
    {
    }

    /**
     * @method TeamUser shift()
     * @method TeamUser pop()
     * @method TeamUser get($key, $default = null)
     * @method TeamUser pull($key, $default = null)
     * @method TeamUser first(callable $callback = null, $default = null)
     * @method TeamUser firstWhere(string $key, $operator = null, $value = null)
     * @method TeamUser[] all()
     * @method TeamUser last(callable $callback = null, $default = null)
     */
    class _TeamUserCollection extends _BaseCollection
    {
        /**
         * @param int $size
         * @return TeamUser[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }

    /**
     * @method _TeamUserQueryBuilder whereId($value)
     * @method _TeamUserQueryBuilder whereUserId($value)
     * @method _TeamUserQueryBuilder whereTeamId($value)
     * @method _TeamUserQueryBuilder whereRank($value)
     * @method _TeamUserQueryBuilder whereCreatedAt($value)
     * @method _TeamUserQueryBuilder whereUpdatedAt($value)
     * @method TeamUser create(array $attributes = [])
     * @method _TeamUserCollection|TeamUser[] cursor()
     * @method TeamUser|null find($id, array $columns = ['*'])
     * @method _TeamUserCollection|TeamUser[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method TeamUser findOrFail($id, array $columns = ['*'])
     * @method TeamUser findOrNew($id, array $columns = ['*'])
     * @method TeamUser first(array $columns = ['*'])
     * @method TeamUser firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method TeamUser firstOrCreate(array $attributes, array $values = [])
     * @method TeamUser firstOrFail(array $columns = ['*'])
     * @method TeamUser firstOrNew(array $attributes, array $values = [])
     * @method TeamUser forceCreate(array $attributes)
     * @method _TeamUserCollection|TeamUser[] fromQuery(string $query, array $bindings = [])
     * @method _TeamUserCollection|TeamUser[] get(array $columns = ['*'])
     * @method TeamUser getModel()
     * @method TeamUser[] getModels(array $columns = ['*'])
     * @method _TeamUserCollection|TeamUser[] hydrate(array $items)
     * @method TeamUser make(array $attributes = [])
     * @method TeamUser newModelInstance(array $attributes = [])
     * @method TeamUser updateOrCreate(array $attributes, array $values = [])
     */
    class _TeamUserQueryBuilder extends _BaseBuilder
    {
    }

    /**
     * @method User shift()
     * @method User pop()
     * @method User get($key, $default = null)
     * @method User pull($key, $default = null)
     * @method User first(callable $callback = null, $default = null)
     * @method User firstWhere(string $key, $operator = null, $value = null)
     * @method User[] all()
     * @method User last(callable $callback = null, $default = null)
     */
    class _UserCollection extends _BaseCollection
    {
        /**
         * @param int $size
         * @return User[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }

    /**
     * @method _UserQueryBuilder whereId($value)
     * @method _UserQueryBuilder whereName($value)
     * @method _UserQueryBuilder whereEmail($value)
     * @method _UserQueryBuilder wherePayId($value)
     * @method _UserQueryBuilder whereContact($value)
     * @method _UserQueryBuilder whereImage($value)
     * @method _UserQueryBuilder whereContactId($value)
     * @method _UserQueryBuilder wherePaymoney($value)
     * @method _UserQueryBuilder whereEmailVerifiedAt($value)
     * @method _UserQueryBuilder wherePassword($value)
     * @method _UserQueryBuilder whereRememberToken($value)
     * @method _UserQueryBuilder whereCreatedAt($value)
     * @method _UserQueryBuilder whereUpdatedAt($value)
     * @method User create(array $attributes = [])
     * @method _UserCollection|User[] cursor()
     * @method User|null find($id, array $columns = ['*'])
     * @method _UserCollection|User[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method User findOrFail($id, array $columns = ['*'])
     * @method User findOrNew($id, array $columns = ['*'])
     * @method User first(array $columns = ['*'])
     * @method User firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method User firstOrCreate(array $attributes, array $values = [])
     * @method User firstOrFail(array $columns = ['*'])
     * @method User firstOrNew(array $attributes, array $values = [])
     * @method User forceCreate(array $attributes)
     * @method _UserCollection|User[] fromQuery(string $query, array $bindings = [])
     * @method _UserCollection|User[] get(array $columns = ['*'])
     * @method User getModel()
     * @method User[] getModels(array $columns = ['*'])
     * @method _UserCollection|User[] hydrate(array $items)
     * @method User make(array $attributes = [])
     * @method User newModelInstance(array $attributes = [])
     * @method User updateOrCreate(array $attributes, array $values = [])
     */
    class _UserQueryBuilder extends _BaseBuilder
    {
    }
}

namespace LaravelIdea\Helper\Illuminate\Notifications {

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Notifications\DatabaseNotification;
    use LaravelIdea\Helper\_BaseBuilder;
    use LaravelIdea\Helper\_BaseCollection;

    /**
     * @method DatabaseNotification shift()
     * @method DatabaseNotification pop()
     * @method DatabaseNotification get($key, $default = null)
     * @method DatabaseNotification pull($key, $default = null)
     * @method DatabaseNotification first(callable $callback = null, $default = null)
     * @method DatabaseNotification firstWhere(string $key, $operator = null, $value = null)
     * @method DatabaseNotification[] all()
     * @method DatabaseNotification last(callable $callback = null, $default = null)
     */
    class _DatabaseNotificationCollection extends _BaseCollection
    {
        /**
         * @param int $size
         * @return DatabaseNotification[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }

    /**
     * @method DatabaseNotification create(array $attributes = [])
     * @method _DatabaseNotificationCollection|DatabaseNotification[] cursor()
     * @method DatabaseNotification|null find($id, array $columns = ['*'])
     * @method _DatabaseNotificationCollection|DatabaseNotification[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method DatabaseNotification findOrFail($id, array $columns = ['*'])
     * @method DatabaseNotification findOrNew($id, array $columns = ['*'])
     * @method DatabaseNotification first(array $columns = ['*'])
     * @method DatabaseNotification firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method DatabaseNotification firstOrCreate(array $attributes, array $values = [])
     * @method DatabaseNotification firstOrFail(array $columns = ['*'])
     * @method DatabaseNotification firstOrNew(array $attributes, array $values = [])
     * @method DatabaseNotification forceCreate(array $attributes)
     * @method _DatabaseNotificationCollection|DatabaseNotification[] fromQuery(string $query, array $bindings = [])
     * @method _DatabaseNotificationCollection|DatabaseNotification[] get(array $columns = ['*'])
     * @method DatabaseNotification getModel()
     * @method DatabaseNotification[] getModels(array $columns = ['*'])
     * @method _DatabaseNotificationCollection|DatabaseNotification[] hydrate(array $items)
     * @method DatabaseNotification make(array $attributes = [])
     * @method DatabaseNotification newModelInstance(array $attributes = [])
     * @method DatabaseNotification updateOrCreate(array $attributes, array $values = [])
     */
    class _DatabaseNotificationQueryBuilder extends _BaseBuilder
    {
    }
}
