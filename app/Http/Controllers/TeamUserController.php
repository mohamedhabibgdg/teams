<?php

namespace App\Http\Controllers;

use App\Team;
use App\TeamUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TeamUserController extends Controller
{
    public function deleteTeamUser($teamId,$userID){
        $user= User::find($userID);
        $user->teams()->detach($teamId);
        return back();
    }
    public function addTeamUser($teamId,$userID){
        $user= User::find($userID);
        $user->teams()->attach($teamId);
        return back();
    }
    public function deleteTeamUserLeader($teamId,$userID){
        $teamUser= TeamUser::where(['user_id'=>$userID,'team_id'=>$teamId])->first()->get();
            if (Auth::user()->hasRole('admins')){
                $teamUser->update(['rank'=>1]);
            }
        return back();
    }
    public function addTeamUserLeader($teamId,$userID){
        $teamUser= TeamUser::where(['user_id'=>$userID,'team_id'=>$teamId])->first()->get();
            if (Auth::user()->hasRole('admins')){
                $teamUser->update(['rank'=>3]);
            }
        return back();
    }

}
