<?php

return [

    'title' => '',

    'title_prefix' => ' ملوك الرويات ',

    'title_postfix' => '',

    'logo' => '<b>ملوك الرويات</b>',

    'logo_mini' => '<b>ملوك</b>',

    'skin' => 'blue-light',

    'layout' => null,

    'collapse_sidebar' => false,

    'dashboard_url' => 'user/novel',

    'logout_url' => 'logout',

    'logout_method' => null,

    'login_url' => 'login',

    'register_url' => 'register',

    'menu' => [

        [
            'text'        => 'المسخدمين',
            'url'         => 'admin/users',
            'icon'        => 'user',
            'label_color' => 'success',
            'route'       => 'users.index'
        ],
        [
            'text' => 'الفرق',
            'url'  => 'admin/teams',
            'icon' => 'users',
        ],
        [
            'text' => 'الرويات',
            'url'  => 'admin/novels',
            'icon' => 'book',
        ],
        [
            'text' => 'الفصول',
            'url'  => 'admin/stories',
            'icon' => 'newspaper-o',
        ],
        [
            'text' => 'الدفع',
            'url'  => 'admin/pays',
            'icon' => 'cc-paypal',
        ],
        [
            'text' => 'طرق التواصل',
            'url'  => 'admin/contacts',
            'icon' => 'bomb',
        ],

        [
            'text' => 'المستحقات',
            'url'  => 'admin/money',
            'icon' => 'usd',
        ],
    ],



    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\DataFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
    ],



    'plugins' => [
        'datatables' => false,
        'select2'    => false,
        'chartjs'    => false,
    ],
];
