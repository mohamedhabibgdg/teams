@extends('adminlte::page')

@section('content')
    <div class="col-md-3" style="margin-bottom: 10px;">
        <form action="?" name="selection" method="GET">

            <select class="form-control" name="selected" onchange="selection.submit()" id="">
                <option {{request('selected',null)=='0'?'selected':''}} value="0">مستحقات تحت الطلب</option>
                <option {{request('selected',null)=='1'?'selected':''}} value="1">مستحقات تم استلامها</option>
                <option {{request('selected',null)=='3'?'selected':''}} value="3">مستحقات متراكمة</option>
                <option {{request('selected',null)=='4'?'selected':''}} value="4">مستحقات الشهر</option>
            </select>
        </form>
    </div>
    @if ($selected==0)
        <div class="col-md-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">مستحقات تحت الطلب</h3>
                </div>
                <div class="box-body">
                    @if ($moneys->count()>0)
                        <table class="table-responsive table w-100">
                            <tr>
                                <td>#</td>
                                <td>اسم المستخدم</td>
                                <td>$</td>
                                <td>التاريخ</td>
                                <td>دفع نهائي</td>
                                <td>حذف</td>
                            </tr>
                            @foreach ($moneys as $key=> $money)
                                <tr>
                                    <td>{{($key+1)}}</td>
                                    <td><a href="{{route('users.show',$money->user)}}">{{$money->user->name}}</a></td>
                                    <td>{{$money->money}}$</td>
                                    <td>{{date_format($money->created_at,'Y-m-d')}}</td>
                                    <td>
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-money{{$money->id.($key+2)}}">عملية دفع</button>
                                        <div class="modal fade bs-money{{$money->id}}" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                                            <div class="modal-dialog modal-sm" role="document">
                                                <div class="modal-content" style="padding: 10px;">
                                                    <form action="{{url('admin/money/'.$money->id)}}" method="POST">
                                                        @csrf
                                                        @method('PUT')
                                                        <h3>
                                                            هل انت متاكد انك تريد دفع المستحقات بقيمة <br>
                                                            {{$money->money}}$
                                                            <br>
                                                            للمستخدم <a href="{{route('users.show',$money->user->id)}}">{{$money->user->name}}</a>
                                                        </h3>
                                                        <button type="submit" class="btn btn-success" style="float: left;">دفع</button>
                                                        <button type="button" class="btn btn-danger" style="float: right;" data-dismiss="modal">الغاء</button>
                                                        <div class="clearfix"></div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target=".bs-del-money{{$money->id.($key+5)}}"> حذف</button>
                                        <div class="modal fade bs-del-money{{$money->id}}" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                                            <div class="modal-dialog modal-sm" role="document">
                                                <div class="modal-content" style="padding: 10px;">
                                                    <form action="{{route('delMoney',$money->id)}}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger" style="float: left;">حذف</button>
                                                        <button type="button" class="btn btn-default" style="float: right;" data-dismiss="modal">الغاء</button>
                                                        <div class="clearfix"></div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        {{$moneys->links()}}
                    @else
                        <div class="alert alert-info text-center">
                            <h3>
                                لا يوجد مستحقات حاليا عد في وقت لاحق
                            </h3>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    @endif

    @if ($selected==1)
        <div class="col-md-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">مستحقات تم استلامها </h3>
                </div>
                <div class="box-body">
                    @if ($moneys->count()>0)
                        <table class="table-responsive table w-100">
                            <tr>
                                <td>#</td>
                                <td>اسم المستخدم</td>
                                <td>$</td>
                                <td>التاريخ</td>
                            </tr>
                            @foreach ($moneys as $key=> $money)
                                <tr>
                                    <td>{{($key+1)}}</td>
                                    <td><a href="{{route('users.show',$money->user)}}">{{$money->user->name}}</a></td>
                                    <td>{{$money->money}}$</td>
                                    <td>{{date_format($money->created_at,'Y-m-d')}}</td>

                                </tr>
                            @endforeach
                        </table>
                        {{$moneys->links()}}
                    @else
                        <div class="alert alert-info text-center">
                            <h3>
                                لا يوجد مستحقات حاليا عد في وقت لاحق
                            </h3>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    @endif

    @if ($selected==3)
        <div class="col-md-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">مستحقات متراكمة </h3>
                </div>
                <div class="box-body">
                    @if ($moneys->count()>0)
                        <table class="table-responsive table w-100">
                            <tr>
                                <td>#</td>
                                <td>اسم المستخدم</td>
                                <td>$</td>
                                <td>التاريخ</td>
                            </tr>
                            @foreach ($moneys as $key=> $money)
                                <tr>
                                    <td>{{($key+1)}}</td>
                                    <td><a href="{{route('users.show',$money->user)}}">{{$money->user->name}}</a></td>
                                    <td>{{$money->money}}$</td>
                                    <td>{{date_format($money->created_at,'Y-m-d')}}</td>

                                </tr>
                            @endforeach
                        </table>
                        {{$moneys->links()}}
                    @else
                        <div class="alert alert-info text-center">
                            <h3>
                                لا يوجد مستحقات حاليا عد في وقت لاحق
                            </h3>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    @endif

    @if ($selected==4)
        <div class="col-md-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">مستحقات الشهر </h3>
                </div>
                <div class="box-body">
                    @if ($moneys->count()>0)
                        <table class="table-responsive table w-100">
                            <tr>
                                <td>#</td>
                                <td>اسم المستخدم</td>
                                <td>$</td>
                                <td>التاريخ</td>
                            </tr>
                            @foreach ($moneys as $key=> $money)
                                <tr>
                                    <td>{{($key+1)}}</td>
                                    <td><a href="{{route('users.show',$money->user)}}">{{$money->user->name}}</a></td>
                                    <td>{{$money->money}}$</td>
                                    <td>{{date_format($money->created_at,'Y-m-d')}}</td>

                                </tr>
                            @endforeach
                        </table>
                        {{$moneys->links()}}
                    @else
                        <div class="alert alert-info text-center">
                            <h3>
                                لا يوجد مستحقات حاليا عد في وقت لاحق
                            </h3>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    @endif

@stop
