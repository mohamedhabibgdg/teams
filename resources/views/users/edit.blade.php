@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>تعديل المستخدم</h1>
@stop

@section('content')
    @include('errors')

    <form action="{{url('admin/users/'.$user->id)}}" method="POST" role="form" >
        @csrf
        @method('PUT')

        <div class="form-group">
            <div class="col-md-4">
                <label for="name">الاسم</label>
            </div>
            <div class="col-md-6">
                <input type="text" name="name" value="{{$user->name}}" id="name" class="form-control" placeholder="الاسم" aria-describedby="name">
                <br>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-4">
                <label for="email">البريد الالكتروني</label>
            </div>
            <div class="col-md-6">
                <input type="email" name="email" value="{{$user->email}}" id="email" class="form-control" placeholder="البريد الالكتروني" aria-describedby="email">
                <br>
            </div>
        </div>

        <div class="form-group row">
            <label for="name" class="col-md-4">كيف تريد طريقة التواصل ؟</label>

            <div class="col-md-6">
                <select class="form-control{{ $errors->has('contact_id') ? ' is-invalid' : '' }}" name="contact_id"  value="{{ old('contact_id') }}" id="contact_id">
                    @foreach (\App\Contact::all() as $contact)
                        <option value="{{$contact->id}}" {{($contact->id==$user->contact_id)?'selected':''}} >{{$contact->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="name" class="col-md-4">طريقة التواصل</label>
            <div class="col-md-6">
                <textarea name="contact" class="form-control w-100" required>{{ $user->contact }}</textarea>
            </div>
        </div>

        <div class="form-group row">
            <label for="name" class="col-md-4">كيف تريد الدفع ؟</label>
            <div class="col-md-6">
                <select class="form-control" name="pay_id"  id="pay_id">
                    @foreach (\App\Pay::all() as $pay)
                        <option value="{{$pay->id}}" {{($pay->id==$user->contact_id)?'selected':''}}>{{$pay->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="name" class="col-md-4 col-form-label text-md-right">طريقة الدفع</label>
            <div class="col-md-6">
                <textarea name="paymoney" class="form-control w-100" required>{{$user->paymoney}}</textarea>
            </div>
        </div>


        <button type="submit" class="btn btn-warning">تعديل المستخدم</button>
    </form>

@stop
