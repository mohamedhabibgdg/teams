<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Money;
use App\Navel;
use App\Pay;
use App\Story;
use App\Team;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\Rule;

class UserPagesController extends Controller
{
    public function welcome(){
        $navel=Navel::where('name','سياسات وقوانين الموقع')->first();
        $teams=$navel->stories ?? collect([]);
        return view('welcome')->with(['teams'=>$teams]);
    }

    public function teams(Story $story){
        if ($story->navel->name === 'سياسات وقوانين الموقع')return view('readTeams')->with(['story'=>$story]);
        return Redirect::route('welcome');
    }
    public function edit(Story $story)
    {
        return view('user.storyedit')->with(['story'=>$story]);
    }
    public function store(Request $request)
    {
        $validate=$request->validate([
            'title'=>['required'],
            'body'=>['required'],
            'navel_id'=>['required'],
            'storynum'=>['required',Rule::unique('stories')->where('navel_id',request('navel_id'))]
        ]);
        $validate['user_id']=auth()->id();
        Story::create($validate);
        session()->flash('success','تم انشاء الفصل بنجاح');
        return redirect(url('user/novel'));
    }
    public function show(Story $story)
    {

        $nextPage=Story::where('navel_id',$story->navel_id)->where('storynum','>',$story->storynum)->orderBy('storynum','ASC')->first()->id ?? -2;
        $BackPage=Story::where('navel_id',$story->navel_id)->where('storynum','<',$story->storynum)->orderBy('storynum','DESC')->first()->id ?? -2;

        return view('user.story')->with(['story'=>$story,'userid'=>Auth::user()->id,'nextPage'=>$nextPage,'backPage'=>$BackPage]);
    }

    public function profile(User $id)
    {
        $case=false;
        $newMoney=$id->storiesMoney();

        $myAllMoney=$id->myAllMoney();
        $oldMoney=$id->oldMoney();
        $moneyMonth=$id->moneyMonth();
        $newMoney+=$id->newMoney();
        $detalis=$id->mymoney;

        if (auth()->user()->id == $id->id || auth()->user()->hasRole('admins')){$case=true;}
        $data=['user'=>$id,'pay'=>$id->pay,'contact'=>$id->contact,'myprofile'=>$case,'userMoney'=>$myAllMoney,'lastMoney'=>$oldMoney,'nowMoney'=>$newMoney,'detalisMoney'=>$detalis,'moneyMonth'=>$moneyMonth];
        return view('user.profile')->with($data);
    }

    public function myteam(Team $id){

        $teamid=\App\TeamUser::where(['user_id'=>Auth::user()->id,'team_id'=>$id->id])->first();
        try{
            $admin=$teamid->rank;
        }catch (\Exception $e){
            $admin=false;
        }

        $arr=[];
        foreach ($id->users as $user)
        {
            $arr[]+=$user->id;
        }
        $users=User::whereNotIn('id',$arr)->get();
        return view('user.myteam')->with(['team'=>$id,'users'=>$users,'is_admin'=>$admin]);

    }

    public function updatePassword(Request $request, $id)
    {
        $request->validate(['newpassword'=>'required|min:6']);
        $request->newpassword=Hash::make($request->newpassword);
        User::find($id)->update(['password'=>$request->newpassword]);
        return redirect(url('user/novel'));
    }
    public function editpass($id)
    {
        return view('user.pass')->with(['user'=>User::find($id)]);
    }

    public function changedone(User $userid){

        foreach ($userid->stories as $story) {
            if ($story->case==0){
                Story::find($story->id)->update(['case'=>1]);
            }
        }


        return back();
    }
    public function create()
    {
        $teams=Auth::user()->teams;
        $arrTeams=[];
        foreach ($teams as $team) {
            foreach ($team->novels as $novel) {

                $arrTeams[]=$novel->id;
            }
        }
        $MyNavels=Navel::findMany($arrTeams);

        return view('user.addstory')->with(['novales'=>$MyNavels]);
    }
    public function update(Request $request,Story $story){
        $validate = $request->validate([
            'title'=>['required'],
            'body'=>['required'],
            'navel_id'=>['required'],
            'storynum'=>['required',Rule::unique('stories')->where('navel_id',request('navel_id')->ignore($story->id))]
        ]);
        $story->update($validate);
        return redirect(url('user/novel'));

    }
    public function profileupdate(Request $request,User $id){
        $request->validate([
            'name'=>['required'],
            'email'=>['required'],
            'pay_id'=>['required'],
            'paymoney'=>['required'],
            'contact_id'=>['required'],
            'contact'=>['required'],
        ]);
        $id->update($request->only(['name','email','pay_id','paymoney','contact_id','contact']));
        return back();
    }
    public function profileedit(User $id)
    {
        if (Auth::user()->id == $id->id || auth()->user()->hasRole('admins')){
            return view('user.useredit')->with(['user'=>$id]);
        }
        return redirect(url('user/profile/'.$id->id));
    }


    public function activeUser(User $user){
        $user->update(['email_verified_at'=>now()]);
        return Redirect::route('users.index');
    }


}
