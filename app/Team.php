<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Team extends Model {
    use LogsActivity;
    protected static $logOnlyDirty = true;

    public $guarded=[];

    public function users(){ return $this->belongsToMany(User::class)->withTimestamps(); }

    public function novels(){ return $this->hasMany(Navel::class); }

}
