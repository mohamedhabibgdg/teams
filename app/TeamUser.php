<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class TeamUser extends Model
{
    use LogsActivity;
    protected static $logOnlyDirty = true;


    protected $table = 'team_user';

}
