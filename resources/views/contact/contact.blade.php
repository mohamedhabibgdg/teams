@extends('adminlte::page')


@section('content_header')
    <h1>اسم طريقة التواصل :  {{$contact->name}}</h1>
@stop

@section('content')
    <h3>طريقة التواصل</h3>

  <div class="col-md-12">
      <div class="box">
          <div class="box-header with-border">
              <div class="box-tools pull-right">
                  <!-- Buttons, labels, and many other things can be placed here! -->
                  <!-- Here is a label for example -->
                  <a href="{{url('admin/contacts/'.$contact->id.'/edit')}}" class="btn btn-warning">تحديث</a>
                  <label for="#">
                      <form action="{{url('admin/contacts/'.$contact->id)}}" method="POST">
                          @csrf
                          @method('DELETE')
                          <button class="btn btn-danger">حذف</button>
                      </form>
                  </label>
              </div>
              <!-- /.box-tools -->
          </div>
          <div class="box-body">
              @if (count($contact->users)>0)
                    <ul class="scrolH">
                        @foreach ($contact->users as $user)
                            <li>
                                <a href="{{url('admin/users/'.$user->id)}}">{{$user->name}}</a>
                            </li>
                        @endforeach
                    </ul>
                 @else
                  {{"لا يوجد اعضاء"}}
              @endif
                  <hr>

          </div>
      </div>
  </div>
  <div class="clearfix"></div>
@stop
