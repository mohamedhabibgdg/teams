<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Navel;
use Faker\Generator as Faker;

$factory->define(Navel::class, function (Faker $faker) {
    return [
        "name"=>$faker->unique()->name,
        "price"=>rand(10,1000),
        "image"=>$faker->imageUrl(),
        "team_id"=>\App\Team::get()->random()->id
    ];
});
