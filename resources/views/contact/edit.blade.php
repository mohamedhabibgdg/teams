@extends('adminlte::page')

@section('content_header')
    <h1>تعديل طريقة التواصل</h1>
@stop

@section('content')
    <form action="{{url('admin/teams/'.$contact->id)}}" method="POST" role="form">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="name">اسم طريقة التواصل</label>
            <input type="text" name="name" id="name" class="form-control" value="{{$contact->name}}" placeholder="" aria-describedby="name">
        </div>
        <button type="submit" class="btn btn-warning">تعديل طريقة التواصل</button>
    </form>

@stop
