@extends('adminlte::page')
@section('content_header')
    <h1>تعديل الرواية</h1>
@stop

@section('content')
    <form action="{{url('admin/novels/'.$novel->id)}}" method="POST" role="form" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="name">اسم الرواية</label>
            <input type="text" value="{{$novel->name}}" class="form-control" name="name" id="name" aria-describedby="helpId" placeholder="Novel Name">
        </div>
        <div class="form-group">
            <label for="name">سعر الرواية</label>
            <input type="number" value="{{$novel->price}}" step="any" class="form-control" name="price" id="price" aria-describedby="helpId" placeholder="Price">
        </div>
        <div class="form-group">
            <label for="">صورة الرواية</label>
            <input type="file" class="form-control-file" name="image" id="image" placeholder="" aria-describedby="fileHelpId">
            <img src="{{asset('uploads/Novel\\'.$novel->image)}}" width="150" height="150" alt="">
        </div>
        <div class="form-group">
            <label for="">الفريق المسئول عن الرواية</label>
            <select class="form-control" name="team_id" id="team_id">
                @foreach ($teams as $team)
                    <option value="{{$team->id}}" {{($novel->team_id == $team->id)?"selected":''}} >{{$team->name}}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-warning">تعديل الرواية</button>
    </form>

@stop
