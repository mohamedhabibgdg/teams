@extends('adminlte::page')
@section('content_header')
    <h1></h1>
@stop
@section('content')
    <div class="box">
        <div class="box-header">
            <div class="box-tools">
                <a href="{{url('admin/stories/'.$story->id.'/edit')}}" class="label label-warning">تحديث</a>
                <label for="">
                    <form action="{{url('admin/stories/'.$story->id)}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn label label-danger">حذف</button>
                    </form>

                </label>
            </div>
            <h4><a href="{{route('users.show',$story->user->id)}}"><i class="fa fa-user"></i> {{$story->user->name}}</a></h4>
            <h2 class="box-title font-weight-bold">{{$story->storynum}} - {{$story->title}}</h2>
        </div>
    </div>
    <div style="text-align: center;direction: rtl">
        {!! $story->body !!}
    </div>
    <hr>
    @if ($nextPage!=-2)
        <div class="pull-left">
            <a href="{{url('admin/stories/'.$nextPage)}}" class="btn btn-primary">{{\App\Story::find($nextPage)->storynum}}</a>
        </div>
    @endif
    @if ($BackPage!=-2)
        <div class="pull-right">
            <a href="{{url('admin/stories/'.$BackPage)}}"  class="btn btn-primary">{{\App\Story::find($BackPage)->storynum}}</a>
        </div>
    @endif
    <div class="clearfix"></div>
    <hr>
<a href="{{url('admin/stories')}}" class="btn btn-success">عودة</a>

@stop

