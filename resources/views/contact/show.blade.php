@extends('adminlte::page')
@section('content')

        <div class="box">
            <div class="box-header with-border">
                <div class="box-tools pull-right">
                    <!-- Buttons, labels, and many other things can be placed here! -->
                    <!-- Here is a label for example -->
                    <a href="{{url('admin/contacts/create')}}" class="label label-primary">اضافة طريقة التواصل</a>
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                @if ($contacts->count() >0 )
                <table class="table table-responsive">
                    <tr style="border: none">
                        <th>#</th>
                        <th>اسم طريقة التواصل</th>
                        <th>عدد الاعضاء</th>
                    </tr>
                        @foreach ($contacts as $contact)
                            <tr>
                                <td>{{$contact->id}}</td>
                                <td><a href="{{url('admin/contacts/'.$contact->id)}}">{{$contact->name}}</a></td>
                                <td>{{count($contact->users)}}</td>
                            </tr>
                        @endforeach
                </table>
                @endif
            </div>
            {{$contacts->links()}}
        </div>

@stop
