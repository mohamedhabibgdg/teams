<?php

namespace App\Http\Controllers;

use App\Team;
use App\TeamUser;
use App\User;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('team.show')->with('teams',Team::latest()->paginate(50));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('team.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>['required','string']
        ]);
        $team=new Team();
        $team->name=$request->name;
        if($request->chatCode !='') $team->chatCode=$request->chatCode;
        $team->save();
        return redirect(route('teams.show',$team->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function show(Team $team)
    {
        $arr=[];
        foreach ($team->users as $user)
        {
            $arr[]+=$user->id;
        }
        $users=User::whereNotIn('id',$arr)->get();
        return view('team.team')->with(['team'=>$team,'users'=>$users]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function edit(Team $team)
    {
        return view('team.edit')->with(['team'=>$team]);
    }
    public function editdata(Team $id)
    {
        return view('user.teamedit')->with(['team'=>$id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Team $team)
    {
        $request->validate([
            'name'=>['required','string']
        ]);
        $team->update($request->only(['name']));
        $team->update($request->only(['chatCode']));
        return redirect(route('teams.show',$team->id));
    }
    public function updatedata(Request $request, Team $id)
    {
        $id->update($request->only(['name']));
        $id->update($request->only(['chatCode']));
        return back();
    }


    public function updaterank(Request $request,Team $teamId,User $userID)
    {
        $request['team_id']=$teamId->id;
        $request['user_id']=$userID->id;
        $request['rank']=3;

        TeamUser::where($request->only(['user_id','team_id']))->update($request->only(['user_id','team_id','rank']));
        return back();
    }
    public function updateunrank(Request $request,Team $teamId,User $userID)
    {
        $request['team_id']=$teamId->id;
        $request['user_id']=$userID->id;
        $request['rank']=1;
        TeamUser::where($request->only(['user_id','team_id']))->update($request->only(['user_id','team_id','rank']));
        return back();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function destroy(Team $team)
    {
        $team->delete();
        return redirect(route('teams.index'));
    }
}
