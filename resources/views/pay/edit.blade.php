@extends('adminlte::page')
@section('content_header')
    <h1>تعديل طريقة الدفع</h1>
@stop

@section('content')
    <form action="{{url('admin/pays/'.$pay->id)}}" method="POST" role="form">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="name">اسم طريقة الدفع</label>
            <input type="text" name="name" id="name" class="form-control" value="{{$pay->name}}" placeholder="" aria-describedby="name">
        </div>
        <button type="submit" class="btn btn-warning">تعديل طريقة الدفع</button>
    </form>

@stop
