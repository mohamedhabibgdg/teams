<?php

namespace App\Http\Controllers;

use App\Navel;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class NovelPagesController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $NUrl = Navel::where('name', 'سياسات وقوانين الموقع')->first();
        $novels = Navel::has('stories')->where('name', '!=', 'سياسات وقوانين الموقع')->latest()->paginate(50);
        return view('user.novels')->with(['novels' => $novels, 'urlNoval' => $NUrl]);

    }

    /**
     * @param Navel $id
     * @return Application|Factory|View
     */
    public function show(Navel $id)
    {
        $novel = $id;
        $stories = $novel->stories()->orderBy('storynum', 'desc')->get();
        return view('user.novel', compact('novel', 'stories'));

    }

}
