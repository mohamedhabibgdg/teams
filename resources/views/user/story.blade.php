@extends('ms.master')
@section('content')

    {{--<style>--}}
        {{--.myChapter a{--}}
            {{--color: #3c8dbc !important;--}}
        {{--}--}}
        {{--.myChapter *{--}}
            {{--text-align: center;--}}
        {{--}--}}
    {{--</style>--}}
    <h1 class="text-center">{{$story->storynum}} - {{$story->title}}</h1>
    <h4 class="pull-right"><a href="{{url('user/profile/'.$story->user->id)}}"><i class="fa fa-user"></i> {{$story->user->name}}</a></h4>
    @if ($userid==$story->user_id || \Illuminate\Support\Facades\Auth::user()->hasRole('admins'))
        <a class="btn btn-warning  text-white pull-left" href="{{url('user/stories/'.$story->id.'/edit')}}">تحديث</a>
    @endif
    <div class="clearfix"></div>
    <hr>

    <div class="myChapter" style="direction:rtl;">
        {!! $story->body !!}
    </div>

    <div class="clearfix"></div>
    <hr>
    @if ($nextPage!=-2)
        <div class="pull-left">
            <a href="{{url('user/stories/'.$nextPage)}}" class="btn btn-dark text-white">{{\App\Story::find($nextPage)->storynum}}</a>
        </div>
    @endif
    @if ($backPage!=-2)
        <div class=" pull-right">
            <a href="{{url('user/stories/'.$backPage)}}"  class="btn btn-dark text-white">{{\App\Story::find($backPage)->storynum}}</a>
        </div>
    @endif
    <div class="clearfix"></div>
    <hr>
    <a href="{{url('user/novel/'.$story->navel->id)}}" class="btn btn-secondary">العودة</a>


    <div class="clearfix"></div>
@stop
