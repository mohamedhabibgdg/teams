@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1 class="text-center font-weight-bold text-muted">{{optional($user)->name}}</h1>
@stop
@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">{{optional($user)->name}}</h3>
            <div class="box-tools pull-right">

                <a href="{{url('admin/changepass/'.optional($user)->id.'/edit')}}" class="btn btn-primary">تغير كلمه السر</a>
                <a href="{{url('admin/users/'.optional($user)->id.'/edit')}}" class="btn btn-warning">تحديث</a>

                <label for="#">
                    <form action="{{url('admin/users/'.optional($user)->id)}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger">حذف</button>
                    </form>
                </label>

            </div>
            <!-- /.box-tools -->
        </div>
        <div class="box-body">
            الاسم :  {{optional($user)->name}}<br>
            البريد الالكتروني : {{optional($user)->email}}<br>
            @if ($pay)
                الدفع بواسطة :{{optional($pay)->name}}({{optional($user)->paymoney}})<br>
            @endif
            @if ($contact)
                التواصل عن طريق : {{optional($contact)->name}} ({{optional($user)->contact}}) <br>
            @endif
        </div>

    </div>



    <div class="col-md-4">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">الفصول</h3>
                <div class="box-tools pull-right"></div>
            </div>
                <div class="box-body">

                    @if (count(optional($user)->stories)>0)
                        <ul class="list-unstyled"  style="max-height:200px;overflow: hidden;overflow-y: auto;">
                            @foreach (optional($user)->stories()->orderBy('storynum','DESC')->get() as $stroy)
                                <li><a href="{{url('admin/stories/'.$stroy->id)}}">{{$stroy->storynum}} -  {{$stroy->title}}</a></li>
                            @endforeach
                        </ul>
                    @else
                        {{"لا ينتمي الي اي فريق"}}
                    @endif
                </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title">الفرق</h3>
                <div class="box-tools pull-right"></div>
            </div>
                <div class="box-body">
                    @if (count(optional($user)->teams)>0)
                        <ul class="list-unstyled scrolH">
                            @foreach (optional($user)->teams as $team)
                                <li><a href="{{url('admin/teams/'.optional($team)->id)}}">{{optional($team)->name}}</a></li>
                            @endforeach
                        </ul>
                        @else
                        {{"لا يوجد فصول"}}
                    @endif
                </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-usd" aria-hidden="true"></i> حوافز</h3>
                <div class="box-tools pull-right"></div>
            </div>
            <div class="box-body">
                <form action="{{route('addUserMoney',optional($user)->id)}}" method="POST">
                    @csrf
                    <input class="form-control" type="number" step="any" placeholder="المبلغ" name="moneyUser">
                    <hr>
                    <button class="btn btn-success" type="submit">اضافة حافز</button>
                </form>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="col-md-3 col-sm-6 col-xs-12">

        <div class="info-box bg-green text-white col-md-3">
            <span class="info-box-icon"><i class="fa fa-usd"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">مستحقات تم استلامها</span>
                <span class="info-box-number">{{$lastMoney}}$</span>

            </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
    </div>



    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-red col-md-3">
            <span class="info-box-icon"><i class="fa fa-usd"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">مستحقات تحت الطلب</span>
                <span class="info-box-number">{{$userMoney}}$</span>

            </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
    </div>



    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="info-box bg-light-blue-active col-md-3 ">
            <span class="info-box-icon"><i class="fa fa-usd"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">مستحقات متراكمة</span>
                <span class="info-box-number">{{$monthMoney}}$</span>
            </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->

    </div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-aqua col-md-3 ">
            <span class="info-box-icon"><i class="fa fa-usd"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">مستحقات الشهر</span>
                <span class="info-box-number">{{$nowMoney}}$</span>
            </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->

    </div>
    <div class="clearfix"></div>
    <div class="col-md-12">
        <div class="box box-success">

            <div class="box-header with-border">
                <h3 class="box-title"> تحويل الي مستحقات متراكمة </h3>
            </div>

            <div class="box-body">
                {!! Form::open(['route' => ['users.user.Month','user'=>optional($user)->id], 'method' => 'post']) !!}
                    <button class="btn btn-soundcloud"> تحويل مستحقات الشهر </button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="col-md-12">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">بيانات عملية الدفع</h3>
                <div class="box-tools pull-right"></div>
            </div>
            <div class="box-tools pull-left">
                <!-- Small modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-payUserMoney">دفع كل العمليات</button>

                <!-- Small modal -->
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target=".bs-delUserMoney">حذف كل العمليات</button>

                <div class="modal fade bs-delUserMoney" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                            {!! Form::open(['route' => ['delUserMoney','user'=>optional($user)->id], 'method' => 'post']) !!}
                                <button class="btn btn-danger  pull-left">حذف كل العمليات</button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

                <div class="modal fade bs-payUserMoney" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">

                            {!! Form::open(['route' => ['payUserMoney','user'=>optional($user)->id], 'method' => 'post']) !!}
                                <button class="btn btn-primary">دفع كل العمليات</button>
                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>


                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
            <br>
            <div class="box-body scrolH">
                <table class="table-responsive table table-hover">
                    <tr>
                        <td>#</td>
                        <td>المبلغ</td>
                        <td>تاريخ طلب المبلغ</td>
                        <td>الحاله</td>
                        <td>دفع</td>
                        <td>حذف</td>
                    </tr>
                    @foreach ($detalisMoney as $key=> $des)
                    <tr>
                        <td>{{($key+1)}}</td>
                        <td>{{optional($des)->money}}</td>
                        <td>{{date_format(optional($des)->updated_at,'Y-m-d')}}</td>
                        <td>
                            @if (optional($des)->case==1)
                                <div class="text-success">
                                   <h4> تم الدفع</h4>
                                </div>
                            @else
                                <div class="text-danger">
                                    <h4>لم يتم الدفع</h4>
                                </div>
                            @endif
                        </td>
                        <td>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-money{{$key+2}}" @if (optional($des)->case==1) disabled @endif > دفع</button>
                            <div class="modal fade bs-money{{$key+2}}" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                                <div class="modal-dialog modal-sm" role="document">
                                    <div class="modal-content" style="padding: 10px;">
                                        <form action="{{url('admin/money/'.optional($des)->id)}}" method="POST">
                                            @csrf
                                            @method('PUT')
                                            <h3>
                                                هل انت متاكد انك تريد دفع المستحقات بقيمة <br>
                                                {{optional($des)->money}}$
                                            </h3>
                                            <button type="submit" class="btn btn-success" @if (optional($des)->case==1) disabled @endif style="float: left;">دفع</button>
                                            <button type="button" class="btn btn-danger" style="float: right;" data-dismiss="modal">الغاء</button>
                                            <div class="clearfix"></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target=".bs-del-money{{$key+5}}"> حذف</button>
                            <div class="modal fade bs-del-money{{$key+5}}" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                                <div class="modal-dialog modal-sm" role="document">
                                    <div class="modal-content" style="padding: 10px;">
                                        <form action="{{route('delMoney',optional($des)->id)}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger" style="float: left;">حذف</button>
                                            <button type="button" class="btn btn-default" style="float: right;" data-dismiss="modal">الغاء</button>
                                            <div class="clearfix"></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    {{--<div class="col-md-12">--}}
        {{--<div class="box box-danger">--}}
            {{--<div class="box-header with-border">--}}
                {{--<h3 class="box-title">Title</h3>--}}
                {{--<div class="box-tools pull-right">Habib</div>--}}
            {{--</div>--}}
            {{--<div class="box-body">--}}
                {{--Body--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}


@stop
