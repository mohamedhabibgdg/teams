@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">السلام عليكم ورحمه الله وبركاته</div>
                    <h3 class="p-4 text-muted">الموقع دا خاص بالمترجمين اذا كنت شايف انك تقدر تترجم قصص وروايات هنكون سعداء ان حضرتك تنضم لينا.</h3>
                    @if ($teams->count()>0)
                        <h2 align="center">سياسات وقوانين الموقع</h2>
                        <ol class="scrolH row list-unstyled">
                            @foreach ($teams as $team)
                                <li class="col-md-4">
                                    <a href="{{url('/teams/'.$team->id)}}">{{$team->title}}</a>
                                </li>
                            @endforeach

                        </ol>

                    @endif

                    <b class="text-left pl-4 pb-2">الادارة</b>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
