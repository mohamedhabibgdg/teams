<?php

namespace App\Http\Controllers;

use App\Money;
use App\Story;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MoneyController extends Controller
{
    public function index()
    {
        $selected=\request('selected',0);
        $selected=$selected=="2"?1:$selected;
//        $moneys=Money::where('case',0)->orWhere('case',3)->paginate(50);
        $moneys=Money::where('case',$selected)->paginate(50);
        return view('admin.requestMoney')->with(['moneys'=>$moneys,'selected'=>$selected]);
    }

    public function store(User $user=null){

        $user= $user->id ? $user :\auth()->user();
        foreach ($user->mymoney()->where('case',3)->get() as $moony){$moony->update(['case'=>0]);}
        foreach ($user->mymoney()->where('case',4)->get() as $moony){$moony->update(['case'=>0]);}

        return redirect(url('user/profile/'.$user->id));
    }

    public function update(Money $money)
    {
        foreach ($money->user->stories()->where('case',1)->get() as $story){Story::find($story->id)->update(['case'=>2]);}
        $money->update(['case'=>1]);
        return back();
    }

    public function delete(Money $money)
    {
        $money->delete();
        return back();
    }

}
