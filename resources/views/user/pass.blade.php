@extends('ms.master')

@section('content')
    <h1>تحديث كلمه السر</h1>
    <form action="{{url('user/changepass/'.$user->id)}}" method="POST" role="form">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="newpassword">ادخل كلمه السر الجديدة</label>
            <input required type="password" name="newpassword" id="newpassword" class="form-control" placeholder="newpassword" aria-describedby="newpassword" autocomplete="off">
        </div>
        <button type="submit" class="btn btn-warning">تحديث كلمه السر</button>
    </form>

@stop
