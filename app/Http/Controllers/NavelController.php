<?php

namespace App\Http\Controllers;

use App\Navel;
use App\Team;
use Illuminate\Http\Request;

class NavelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('novel.show')->with('novels',Navel::latest()->paginate(50));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('novel.create')->with(['teams'=>Team::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        if ($request->has('image')){
            $imageExt=$request->file('image')->getClientOriginalExtension();
            $imageName=time().'.Novel.'.$imageExt;
            $path="/uploads/Novel";
            $request->file('image')->move(public_path().$path,$imageName);
            $request['image']=$imageName;
            Navel::create([
                'name'=>$request->name,
                'team_id'=>$request->team_id,
                'price'=>$request->price,
                'image'=>$imageName
            ]);



        }else{
            Navel::create($request->only(['name','team_id','price']));
        }
        return redirect(route('novels.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Navel  $navel
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Navel $novel)
    {

        return view('novel.novel')->with(['novel'=>$novel]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Navel  $navel
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit($navel)
    {
        $navel=Navel::find($navel);
        return view('novel.edit')->with(['novel'=>$navel,'teams'=>Team::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Navel  $navel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request,Navel $novel)
    {
        if ($request->has('image')){
            $imageExt=$request->file('image')->getClientOriginalExtension();
            $imageName=time().'.Novel.'.$imageExt;
            $path="/uploads/Novel";
            $request->file('image')->move(public_path().$path,$imageName);
            $novel->update([
                'name'=>$request->name,
                'team_id'=>$request->team_id,
                'price'=>$request->price,
                'image'=>$imageName
            ]);
        }else{
            $novel->update($request->only(['name','team_id','price']));
        }

        return redirect(route('novels.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Navel  $navel
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Navel $novel)
    {
        try {
            $novel->delete();
        } catch (\Exception $e) {
        }
        return redirect(route('novels.index'));
    }
}
