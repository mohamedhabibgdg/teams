@extends('adminlte::page')
@section('content_header')
    <h1>انشاء فصل جديد</h1>
@stop
@section('css')
    <script src="https://cdn.ckeditor.com/4.11.3/standard/ckeditor.js"></script>
@stop
@section('content')
    <form action="{{url('admin/stories')}}" method="POST" role="form">
        @csrf
        <div class="form-group">
            <label for="navel_id">اسم الرواية</label>
            <select class="form-control" name="navel_id" id="navel_id">
                @foreach (\App\Team::latest()->get() as $team)
                    @foreach ($team->novels as $novel)
                        <option value="{{$novel->id}}">{{$novel->name}}</option>
                    @endforeach
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="storynum">رقم الفصل</label>
            <input required autocomplete="off" type="number" name="storynum" id="storynum" class="form-control" placeholder="" aria-describedby="storynum">
        </div>
        <div class="form-group">
            <label for="title">عنوان الفصل</label>
            <input type="text" name="title" id="title" class="form-control" placeholder="" aria-describedby="title">
        </div>

        <div class="form-group">
            <label for="body">محتوي الفصل</label>
            <textarea class="form-control" name="body" id="editor1" rows="3"></textarea>
        </div>

        <button type="submit" class="btn btn-primary">اضافة الفصل</button>
    </form>


@stop

@section('js')
    <script>
        CKEDITOR.replace( 'editor1' );
    </script>
@stop
