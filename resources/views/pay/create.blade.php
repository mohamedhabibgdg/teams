@extends('adminlte::page')
@section('content_header')
    <h1>انشاء طريقة الدفع</h1>
@stop

@section('content')
    <form action="{{url('admin/pays')}}" method="POST" role="form">
        @csrf
        <div class="form-group">
            <label for="name">اسم طريقة الدفع</label>
            <input type="text" name="name" id="name" class="form-control" placeholder="" aria-describedby="name">
        </div>
        <button type="submit" class="btn btn-primary">اضافة طريقة الدفع</button>
    </form>

@stop
