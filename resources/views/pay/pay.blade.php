@extends('adminlte::page')
@section('content_header')
    <h1>اسم طريقة الدفع :  {{$pay->name}}</h1>
@stop

@section('content')

  <div class="col-md-12">
      <div class="box">
          <div class="box-header with-border">
              <h3 class="box-title">المستخدمون - طريقة الدفع</h3>
              <div class="box-tools pull-right">
                  <!-- Buttons, labels, and many other things can be placed here! -->
                  <!-- Here is a label for example -->
                  <a href="{{url('admin/pays/'.$pay->id.'/edit')}}" class="btn btn-warning">تحديث</a>
                  <label for="#">
                      <form action="{{url('admin/pays/'.$pay->id)}}" method="POST">
                          @csrf
                          @method('DELETE')
                          <button class="btn btn-danger">حذف</button>
                      </form>
                  </label>
              </div>
              <!-- /.box-tools -->
          </div>
          <div class="box-body">
              @if (count($pay->users)>0)
                <ul class="scrolH">
                    @foreach ($pay->users as $user)
                        <li>
                            <a href="{{url('admin/users/'.$user->id)}}">{{$user->name}}</a>
                        </li>
                    @endforeach
                </ul>
              @else
                  <div class="alert alert-info text-center"> لا يوجد اشخاص يستخدون هذة الطريقة </div>
              @endif
          </div>
      </div>
  </div>


  <div class="clearfix"></div>
@stop
