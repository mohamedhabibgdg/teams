@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>اضافة فريق</h1>
@stop

@section('content')
    <form action="{{url('admin/teams')}}" method="POST" role="form">
        @csrf
        <div class="form-group">
            <label for="name">اسم الفريق</label>
            <input type="text" name="name" id="name" class="form-control" placeholder="اسم الفريق" aria-describedby="name">
        </div>
        <div class="form-group">
            <label for="chatCode">كود المحادثات</label>
            <textarea name="chatCode"  cols="30" rows="10" placeholder="كود المحادثات" class="form-control"></textarea>
        </div>
        <button type="submit" class="btn btn-primary">اضافة فريق</button>
    </form>

@stop
