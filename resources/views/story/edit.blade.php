@extends('adminlte::page')
@section('content_header')
    <h1>تعديل الفصل</h1>
@stop
@section('css')
    <script src="https://cdn.ckeditor.com/4.11.3/standard/ckeditor.js"></script>
@stop
@section('content')
    <form action="{{url('admin/stories/'.$story->id)}}" method="POST" role="form">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="novel_id">اسم الرواية</label>
            <select class="form-control" name="navel_id" id="navel_id">
                @foreach (\App\Team::latest()->get() as $team)
                    @foreach ($team->novels as $novel)
                        <option value="{{$novel->id}}" {{($story->navel_id==$novel->id)?'selected':''}}>{{$novel->name}}</option>
                    @endforeach
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="storynum">رقم الفصل</label>
            <input required autocomplete="off" value="{{$story->storynum}}" type="number" name="storynum" id="storynum" class="form-control" placeholder="" aria-describedby="storynum">
        </div>
        <div class="form-group">
            <label for="title">عنوان الفصل</label>
            <input type="text" value="{{$story->title}}" name="title" id="title" class="form-control" placeholder="" aria-describedby="title">
        </div>
        <div class="form-group">
            <label for="body">محتوي الفصل</label>
            <textarea class="form-control" name="body" id="editor1" rows="3">{{$story->body}}</textarea>
        </div>

        <button type="submit" class="btn btn-warning">تعديل الفصل</button>
    </form>


@stop

@section('js')
    <script>
        CKEDITOR.replace( 'editor1' );
    </script>
@stop
