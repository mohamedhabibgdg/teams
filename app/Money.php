<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Money extends Model
{
    use LogsActivity;
    protected static $logOnlyDirty = true;

    public const MONEY_REQUEST=0;

    protected $guarded=[];

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function scopeOwn(Builder $builder,$user_id=null){
        return $builder->where('user_id',$user_id ?? auth()->id());
    }
}
