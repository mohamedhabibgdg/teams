@extends('ms.master')
@section('content')
    <script src="https://cdn.ckeditor.com/4.11.3/standard/ckeditor.js"></script>

    <form action="{{ route('user.stories.store') }}" method="POST" role="form">
        @csrf

        <div class="form-group">
            <label for="navel_id">الرواية </label>
            <select required class="form-control" name="navel_id" id="navel_id">

            @if (auth()->user()->hasRole('admins'))
                @foreach (\App\Navel::all() as $novel)
                    <option value="{{$novel->id}}">{{$novel->name}}</option>
                @endforeach
            @else
                @foreach ($novales as $novel)
                    <option value="{{$novel->id}}">{{$novel->name}}</option>
                @endforeach
            @endif

            </select>
            @error('navel_id')
            <div class="text-danger"> {{ $message }} </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="storynum">رقم الفصل</label>
            <input required autocomplete="off" type="number" name="storynum" value="{{ old('storynum') }}" id="storynum" class="form-control" placeholder="" aria-describedby="storynum"/>
            @error('storynum')
                <div class="text-danger"> {{ $message }} </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="title">العنوان</label>
            <input required autocomplete="off" type="text" name="title" id="title" value="{{ old('title') }}" class="form-control" placeholder="" aria-describedby="title"/>
            @error('title')
            <div class="text-danger"> {{ $message }} </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="body">محتوي الفصل </label>
            <textarea required class="form-control" name="body" id="editor2" rows="3">{{ old('body') }}</textarea>
        </div>

        <button type="submit" class="btn btn-primary">اضافه الفصل</button>
    </form>
    <script>
        CKEDITOR.replace( 'editor2' );
    </script>
@stop


