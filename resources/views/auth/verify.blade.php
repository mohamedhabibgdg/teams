@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    ملوك الروايات
                </div>

                <div class="card-body">
                    <div class="alert alert-info text-center" style="font-size: 16px;" role="alert">
                        سيتم مراجعة طلبك قريبا و للموافقة علي طلبك مباشرة الرجاء التواصل مع ادارة الموقع.
                    </div>
                    للتواصل مع ادرة الموقع <a href="{{url('https://www.facebook.com/kolnovel')}}" class="badge badge-primary">
                        <span style="font-weight:500;font-size: 15px;" class="p-0">اضغط هنا</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
