<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Contact extends Model
{
    use LogsActivity;
    protected static $logOnlyDirty = true;

    public $guarded=[];

    public function users(){
        return $this->hasMany('App\User');
    }
}
