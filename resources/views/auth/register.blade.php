@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('تسجيل البيانات') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" onsubmit="myData()">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('الاسم') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus placeholder="Ibrahim Shazly">

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('الايمال') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" placeholder="IbrahimShazly@gmail.com" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('طريقة التواصل') }}</label>
                            <div class="col-md-6">
                                <label for=""></label>
                                <select class="form-control{{ $errors->has('contact_id') ? ' is-invalid' : '' }}" name="contact_id"  id="contact_id">
                                    @foreach (\App\Contact::all() as $contact)
                                        <option value="{{$contact->id}}">{{$contact->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('حساب التواصل') }}</label>
                            <div class="col-md-6">
                                <input name="contact" id="contact" class="form-control w-100" required placeholder="fb.com/IbrahimShazly">
                                <small id="helpId" class="form-text text-success">يرجي حذف //:http او //:https</small>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('طريقة الدفع') }}</label>
                            <div class="col-md-6">
                                <label for=""></label>
                                <select class="form-control{{ $errors->has('pay_id') ? ' is-invalid' : '' }}" name="pay_id" id="pay_id">
                                    @foreach (\App\Pay::all() as $pay)
                                        <option value="{{$pay->id}}">{{$pay->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('حساب الدفع') }}</label>
                            <div class="col-md-6">
                                <input name="paymoney" id="paymoney" class="form-control w-100" required>
                                <small id="helpId" class="form-text text-success">يرجي حذف //:http او //:https</small>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('كلمه السر') }}</label>
                            <div class="col-md-6">
                                <input id="password" type="password" placeholder="كلمه السر" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('تاكيد كلمه السر ') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" placeholder="تاكيد كلمه السر " type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8">
                                <button type="submit" class="btn btn-primary w-25 float-left">
                                    {{ __('التسجيل') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
