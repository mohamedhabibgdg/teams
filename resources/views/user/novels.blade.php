@extends('ms.master')
@section('content')
    @isset($urlNoval)
        <h3 class="text-right">
            <a href="{{route('novel.show',$urlNoval->id)}}" class="text-primary">{{$urlNoval->name}}</a>
        </h3>
    @endif
    <h3 class="text-center">الروايات</h3>
    <div class="position-relative text-center">
        @foreach ($novels as $novel)
                <div class=" p-2 card col-md-3 mb-3 mr-3 d-inline-block" style="height:400px;">
                    <div class="row no-gutters position-relative">
                        <div class="col-md-12 overlay-novel">
                            <div class="card-body text-center">
                                <img src="{{asset("uploads/Novel/".$novel->image)}}" class="card-img" height="136" width="100" alt="">
                                <div class="clearfix"></div>
                                <hr>
                                <a class="btn btn-default font-weight-bold text-dark  centeritem w-100" href="{{url('user/novel/'.$novel->id)}}">{{$novel->name}}</a>
                                @if (count($novel->stories()->orderBy('storynum','DESC')->get())>0)
                                    <hr>
                                    @foreach ($novel->stories()->orderBy('storynum','DESC')->get() as $key=> $str)

                                    @if ($key<=4 &&(date_format($str->created_at,'Y-m-d')=== date_format(now(),'Y-m-d')) )
                                        <a href="{{url('user/stories/'.$str->id)}}" @if (date_format($str->created_at,'Y-m-d') === date_format(now(),'Y-m-d')) class="text-success" @endif>{{$str->storynum}} - {{$str->title}}</a>
                                        @if (date_format($str->created_at,'Y-m-d') === date_format(now(),'Y-m-d')) <b class="label rounded label-danger" style="padding:1px"> اليوم</b> @endif
                                        <br>
                                    @endif

                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
        @endforeach
    </div>
    <hr>
    {{$novels->links()}}
@stop
