@extends('ms.master')
@section('content')
    <form action="{{url('user/profile/'.$user->id)}}" method="POST" role="form">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="name">الاسم</label>
            <input type="text" name="name" value="{{$user->name}}" id="name" class="form-control" placeholder="Username" aria-describedby="name">
        </div>
        <div class="form-group">
            <label for="email">الايمال</label>
            <input type="email" name="email" value="{{$user->email}}" id="email" class="form-control" placeholder="Email" aria-describedby="email">
        </div>
        <div class="form-group row">
            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('طريقة التواصل') }}</label>

            <div class="col-md-12">
                <select class="form-control{{ $errors->has('contact_id') ? ' is-invalid' : '' }}" name="contact_id"  value="{{ old('contact_id') }}" id="contact_id">
                    @foreach (\App\Contact::all() as $contact)
                        <option value="{{$contact->id}}" {{($contact->id==$user->contact_id)?'selected':''}} >{{$contact->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('التواصل') }}</label>
            <div class="col-md-12">
                <textarea name="contact" class="form-control w-100" required>{{ $user->contact }}</textarea>
            </div>
        </div>

        <div class="form-group row">
            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('طريقة الدفع') }}</label>
            <div class="col-md-12">
                <select class="form-control" name="pay_id"  id="pay_id">
                    @foreach (\App\Pay::all() as $pay)
                        <option value="{{$pay->id}}" {{($pay->id==$user->pay_id)?'selected':''}}>{{$pay->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('الدفع') }}</label>
            <div class="col-md-12">
                <textarea name="paymoney" class="form-control w-100" required>{{$user->paymoney}}</textarea>
            </div>
        </div>

        <button type="submit" class="btn btn-warning text-white">تحديث بيانات المستخدم</button>
    </form>

@stop
