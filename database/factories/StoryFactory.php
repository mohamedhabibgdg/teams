<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Story;
use Faker\Generator as Faker;

$factory->define(Story::class, function (Faker $faker) {
    return [
        "title"=>$faker->words(rand(3,10),true),
        "body"=>$faker->sentences(rand(10,30),true),
        "user_id"=>\App\User::get()->random()->id,
        "storynum"=>$faker->randomNumber()
    ];
});
