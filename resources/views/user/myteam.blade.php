@extends('ms.master')
@section('content')

    <div class="col-md-8  float-right">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">معلومات الفر يق</h3>
                <div class="box-tools pull-right">

                    @if ($is_admin==3)
                    <a href="{{url('user/team/'.$team->id.'/edit')}}" class="btn btn-warning text-white">تحديث معلومات الفريق</a>

                    <label for="#">
                        <form action="{{url('user/team/'.$team->id)}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger">حذف الفريق</button>
                        </form>
                    </label>
                    @endif
                </div>
                <!-- /.box-tools -->
            </div>
            <div class="box-body">
                @if (count($team->users)>0)
                    <table class="table">
                        <tr>
                            <td>الاسم</td>
                            <td>المبالغ المطلوب</td>
                            @if ($is_admin==3)
                                <td>الحذف من الفريق</td>
                            @endif
                        </tr>
                        @foreach ($team->users as $user)
                            <tr>
                                <td>
                                    <a class="col-md-8" href="{{url('user/profile/'.$user->id)}}">{{$user->name}}</a>
                                </td>
                                <td>
                                    <?php $money=0;?>
                                        @foreach ($team->novels as $novel)
                                            @foreach ($user->stories as $story)
                                                @if ($story->case==0 && $novel->id==$story->navel_id)
                                                    <?php $money+=$novel->price; ?>
                                                @endif
                                            @endforeach
                                        @endforeach
                                    {{$money}}
                                    $
                                </td>
                                @if ($is_admin==3)
                                <td>
                                    <form class="" action="{{url('user/teamsusers/'.$team->id.'/'.$user->id)}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger" type="submit">حذف من الفريق</button>
                                    </form>
                                </td>

                                @endif
                            </tr>
                        @endforeach
                    </table>
                    <?php $totle=0; ?>
                    @foreach ($team->novels as $novel)
                        @foreach ($team->users as $user)
                            @foreach ($user->stories as $story)
                                @if ($story->case==0 && $novel->id==$story->navel_id)
                                    <?php $totle+=$novel->price; ?>
                                @endif
                            @endforeach
                        @endforeach
                    @endforeach
                    <div class="text-right">الاجمالي<br>{{$totle}}$</div>
                @else
                    {{"لا يوجد اعضاء في الفريق"}}
                @endif
                <hr>
                <h3 class="box-title">اسم الرواية والفصول </h3>
                @if (count($team->novels)>0)
                    <table class="table">
                        @foreach ($team->novels as $novel)
                            <tr>
                                <td>
                                    <a href="{{url('user/novel/'.$novel->storynum)}}">{{$novel->name}}</a>
                                    <ul class="scrolH">
                                        @foreach ($novel->stories()->orderBy('storynum','desc')->get() as $story)
                                            <li class="col-md-12"><a href="{{url('user/stories/'.$story->id)}}">{{$story->storynum.' - '.$story->title}}</a></li>
                                        @endforeach
                                    </ul>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                @endif
            </div>
        </div>
    </div>
    @if($is_admin==3)
    <div class="col-md-4 float-right">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">اضافة اعضاء للفريق</h3>
            </div>
            <div class="box-body scrolH">
                <table class="table">
                    <tr>
                        <th>الاسم</th>
                        <th>اضافة</th>
                    </tr>
                    @foreach ($users as $user)
                          <tr>
                              <td><a class="font-weight-bold" href="{{url('user/profile/'.$user->id) }}">{{$user->name}}</a></td>
                              <td>
                                  <form  action="{{url('user/teamsusers/'.$team->id.'/'.$user->id)}}" method="POST">
                                      @csrf
                                      @method('PUT')
                                      <button class="btn btn-primary"  type="submit">اضافه</button>
                                  </form>
                              </td>
                          </tr>
                    @endforeach
                </table>

            </div>
        </div>
    </div>
    @endif
    <div class="clearfix"></div>
    @if ($team->chatCode !== null)
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-header">
                    {{$team->name}} - صندوق المحادثة
                </div>
                <div class="box-body">
                    {!! $team->chatCode !!}
                </div>
            </div>
        </div>
    @endif

@stop
