@extends('adminlte::page')

@section('content_header')
    <h1>عرض جميع المستخدمين</h1>
    {!! Form::open(['route' => 'users.Month', 'method' => 'post']) !!}
        <button type="submit" class="btn btn-soundcloud pull-right">المستحقات الشهرية</button>
    {!! Form::close() !!}
    <div class="clearfix"></div>

@stop

@section('content')

    <div class="box">
        <div class="box-header with-border">
            <div class="box-tools">
                <a href="{{url('admin/users/create')}}" class="label label-primary">اضافة مستخدم</a>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-responsive">
                <tr style="border: none">
                    <th>#</th>
                    <th>اسم المستخدم</th>
                    <th>البريد الالكتروني</th>
                    <th>اسم الفريق</th>
                    <th>تفعيل المستخدم</th>
                </tr>
                @if ($users->count() >0 )
                    @foreach ($users as $user)
                        <tr>
                            <td>{{$user->id}}</td>
                            <td><a href="{{url('admin/users/'.$user->id)}}">{{$user->name}}</a></td>
                            <td>{{$user->email}}</td>
                            <td>
                                @if (count($user->teams)>0)
                                    @foreach ($user->teams as $team)
                                        {!! "<a href='".url('admin/teams/'.$team->id)."' class='text-primary'>" !!}
                                        {{($team->name ?? 'لا يوجد فريق').'/'}}
                                        {!! '</span>' !!}
                                    @endforeach
                                @else
                                    {!!"<span class='text-danger'>لا يوجد فريق</span>"!!}
                                @endif
                            </td>
                            <td>
                                @if ($user->email_verified_at==null)
                                    {!! Form::open(['route' => ['activeUser','user'=>$user->id], 'method' => 'PUT']) !!}
                                        <button type="submit" class="btn btn-danger">تفعيل</button>
                                    {!! Form::close() !!}
                                @else
                                    <button class="btn btn-success" disabled>مفعل</button>
                                @endif

                            </td>
                        </tr>
                    @endforeach
                @endif
            </table>

        </div>
        {{$users->links()}}
    </div>
@stop
