<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Story extends Model
{
    use LogsActivity;
    protected static $logOnlyDirty = true;
    public $guarded=[];

    public function navel(){ return $this->belongsTo(Navel::class); }

    public function user(){ return $this->belongsTo(User::class); }

}
