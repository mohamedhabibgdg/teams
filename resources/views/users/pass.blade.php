@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Edit User Password</h1>
@stop

@section('content')
    <form action="{{url('admin/changepass/'.$user->id)}}" method="POST" role="form">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="password">Enter new pass</label>
            <input required type="password" name="password" id="name" class="form-control" placeholder="password" aria-describedby="password">
        </div>
        <button type="submit" class="btn btn-warning">Edit Password</button>
    </form>

@stop
