<?php

namespace App\Http\Controllers;
use App\Contact;
use App\Money;
use App\Pay;
use App\Role;
use App\Team;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use App\User;
use Illuminate\Http\Request;



class AdminUsersController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.users')->with('users',User::latest()->paginate(50));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function teamdata()
    {
        return view('team.teamreport')->with(['teams'=>Team::latest()->paginate(50)]);
    }

    public function create()
    {
        return view('users.create');
    }

    public function delMoney(User $user)
    {
        $user->mymoney()->where('case',0)->delete();
        return back();
    }
    public function payMoney(User $user)
    {
        $user->mymoney()->where('case',0)->update(['case'=>1]);
        return back();
    }
    public function usersMonth(){
        foreach (User::whereHas('stories',function (Builder $builder){
            return $builder->where('case',0);
        },'>',0)->get() as $user) {
            $user->userMonth();
        }
        return redirect()->route('users.index');
    }
    public function userMonth(User $user){

        $user->userMonth();

        return \redirect()->route('users.show',$user);
    }


    public function addMoney(Request $request,User $user){

        $request->validate([ 'moneyUser'=>['required','numeric'] ]);
        if ($request->moneyUser!=0){
            Money::create([
                'user_id'=>$user->id,
                'money'=>$request->input('moneyUser'),
                'case'=>4
            ]);
        }


        return redirect()->route('users.show',$user);
    }
    public function store(Request $request){

        $data=$request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'pay_id'=>['required'],
            'paymoney'=>['required', 'string'],
            'contact_id'=>['required'],
            'contact'=>['required', 'string'],
        ]);
        $data['password']=Hash::make($data['password']);
        $user= User::create($data);
        $user->roles()->attach(Role::where('name','users')->first());

        return redirect()->route('users.index');
    }

    public function show(User $user)
    {

        $case=false;
        $newMoney=$user->storiesMoney();

        $myAllMoney=$user->myAllMoney();
        $oldMoney=$user->oldMoney();
        $moneyMonth=$user->moneyMonth();
        $newMoney+=$user->newMoney();
        $detalis=$user->mymoney;

        return view('users.user')->with([
            'user'=>$user,
            'pay'=>$user->pay,
            'contact'=>$user->contact,
            'userMoney'=>$myAllMoney,
            'lastMoney'=>$oldMoney,
            'nowMoney'=>$newMoney,
            'detalisMoney'=>$detalis,
            'monthMoney'=>$moneyMonth
        ]);
    }

    public function edit(User $user)
    {
        return view('users.edit',compact('user'));
    }
    public function editpass(User $id)
    {
        return view('users.pass')->with(['user'=>$id]);
    }

    public function update(Request $request, User $id)
    {
        $validate=$request->validate([
            'name'=>['required'],
            'email'=>['required'],
            'pay_id'=>['required'],
            'paymoney'=>['required'],
            'contact_id'=>['required'],
            'contact'=>['required'],
        ]);

        $id->update($validate);
        return redirect(route('users.index').'/'.$id->id);
    }
    public function updatePassword(Request $request,User $id)
    {
        $data=$request->validate(['password'=>'required|min:6']);
        $data['password']=Hash::make($data['password']);
        $id->update($data);
        return redirect(route('users.index'));
    }

    public function destroy(User $id)
    {
        DB::transaction(function () use ($id) {
            $id->stories()->delete();
            $id->mymoney()->delete();
            $id->delete();
        });

        return redirect()->route('users.index');
    }
}
