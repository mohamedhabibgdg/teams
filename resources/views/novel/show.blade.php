@extends('adminlte::page')
@section('content_header')
    <h1>عرض الروايات</h1>
@stop
@section('content')
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <div class="box-tools pull-right">

                    <a href="{{url('admin/novels/create')}}" class="label label-primary">اضافة رواية</a>
                </div>
                </div>
                <div class="box-body">
                    @if ($novels->count()>0)
                        <table class="table table-responsive">
                            <tr style="border: none">
                                <th>#</th>
                                <th>اسم الرواية</th>
                                <th>صورة الرواية</th>
                                <th>عدد فصول الرواية</th>
                            </tr>
                            @foreach ($novels as $novel)
                                <tr>
                                    <td>{{$novel->id}}</td>
                                    <td><a href="{{route('novels.show',$novel->id)}}">{{$novel->name}}</a></td>
                                    <td><img src="{{asset('uploads/Novel/'.$novel->image)}}" width="50px" height="50px" alt=""></td>
                                    <td>{{count($novel->stories)}}</td>
                                </tr>
                            @endforeach
                        </table>
                        {{$novels->links()}}
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop
