@extends('adminlte::page')
@section('content_header')
    <h1>عرض جميع الفصول</h1>
@stop

@section('content')

    <div class="box">
        <div class="box-header with-border">
            <div class="box-tools pull-right">
                <!-- Buttons, labels, and many other things can be placed here! -->
                <!-- Here is a label for example -->
                <a href="{{route('stories.create')}}" class="label label-primary">اضافة فصل</a>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @if ($stories->count()>0)
                <table class="table table-responsive">
                    <tr style="border: none">
                        <th>رقم الفصل</th>
                        <th>عنوان الفصل</th>
                        <th>اسم الرواية</th>
                        <th>اسم الكاتب</th>
                        <th>تاريخ الاضافة</th>
                    </tr>
                    @foreach ($stories as $story)
                        <tr>
                            <td>{{$story->storynum}}</td>
                            <td><a href="{{url('admin/stories/'.$story->id)}}">{{$story->title}}</a></td>
                            @if(isset($story->navel))
                                <td><a href="{{url('admin/novels/'.$story->navel->id)}}">{{$story->navel->name}}</a></td>
                            @else
                                <td class="text-danger">{{"No Navel Exits"}}</td>
                            @endif
                            <td><a href="{{url('admin/users/'.$story->user->id)}}">{{$story->user->name}}</a></td>
                            <td>{{date_format($story->created_at,'Y-m-d')}}</td>
                        </tr>
                    @endforeach
                </table>
                {{$stories->links()}}
            @endif
        </div>
    </div>
@stop
