<?php

use App\Contact;
use App\Navel;
use App\Pay;
use App\Role;
use App\Story;
use App\Team;
use App\TeamUser;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $admins= Role::create(['id'=>1, 'name'=>'admins']);
        $users= Role::create(['id'=>2, 'name'=>'users']);
        factory(Pay::class,30)->create();
        factory(Contact::class,30)->create();
        $user=factory(User::class)->create([
            "name" => "habib",
           "email" => "admin@app.com",
           "email_verified_at" => now(),
           "password" => bcrypt('123456789'),
            "image" => "uploads/Novel/1552502130.Novel.jpg"
        ]);
        $user->roles()->attach($admins);
        factory(Team::class,30)->create()->each(function (Team $team){
            $allUsers=factory(User::class,5)->create();
            $team->users()->attach($allUsers->pluck('id')->toArray());
            TeamUser::whereTeamId($team->id)->first()->update(['rank' => 2]);
            factory(Navel::class,10)->create(['team_id' => $team->id])->each(function (Navel $navel) use ($team) {
                $team->users->map(function (User $user) use ($navel) {
                    factory(Story::class,rand(5,20))->create(['navel_id' => $navel->id,'user_id' => $user->id]);
                });

            });

        });

    }
}
