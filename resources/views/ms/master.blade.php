<!doctype html>
<html lang="en">
<head>
    <title>Kol Novel Team</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('vendor/adminlte/dist/css/AdminLTE.css')}}">
    <style>
            body {
                direction : rtl;
                text-align: right;
            }
        </style>
</head>
<body class="bg-transparent">
<nav class="navbar navbar-expand-lg navbar-light bg-light position-sticky w-100" style="top:0;z-index: 121313213;">
    <div class="container">
        <a class="navbar-brand" href="{{url('user/novel')}}">الروايات</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <ul class="navbar-nav mr-auto">
                <li class="nav-item dropdown"><a href="{{url('user/stories/create')}}" class="nav-link ">اضافة فصل </a></li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{\Illuminate\Support\Facades\Auth::user()->name}}
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        @if (count(\Illuminate\Support\Facades\Auth::user()->teams)>0)
                        @foreach (\Illuminate\Support\Facades\Auth::user()->teams as $team)
                                <a class="dropdown-item" href="{{url('user/team/'.$team->id)}}">{{$team->name}}</a>
                            @endforeach
                                <div class="dropdown-divider"></div>
                        @endif
                            @if (Auth::user()->hasRole('admins'))
                                <a class="dropdown-item" href="{{url('admin/users')}}"><b>الادارة</b></a>
                            @endif
                            <a class="dropdown-item" href="{{url('user/profile/'.\Auth::user()->id)}}">الصفحه الشخصية</a>
                        <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">تسجيل الخروج

                                <i class="fa fa-fw fa-power-off"></i>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>

                    </div>
                </li>


            </ul>

        </div>
    </div>
</nav>


<div class="container" style="min-height:516px;">
    @if (session('success'))
        <div class="alert alert-success w-100 text-center" role="alert">
            <strong>{{session('success')}}</strong>
        </div>
        @include('errors')
    @endif
    <br>
    @yield('content')
    <br>
</div>
<footer class=" text-center bg-dark text-white">
    <div class="container  p-3">
        <ul class="list-unstyled list-inline float-right">
            <li class="list-inline-item"><a class="text-white" href="#"><i class="fa fa-facebook-square"></i></a></li>
            <li class="list-inline-item"><a class="text-white" href="#"><i class="fa fa-twitter"></i></a></li>
            <li class="list-inline-item"><a class="text-white" href="#"><i class="fa fa-instagram"></i></a></li>
            <li class="list-inline-item"><a class="text-white" href="#"><i class="fa fa-youtube-play"></i></a></li>
        </ul>
        <div class="float-left font-weight-bold">
            <a href="mailto:mohamedmosaadhabib@gmail.com" class="text-white">          جميع الحقوق محفوظة &copy; {{Date('Y')}}</a>
        </div>
        <div class="clearfix"></div>
    </div>
</footer>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="{{asset('js/jquery-3.3.1.slim.min.js')}}"></script>
<script src="{{asset('js/popper.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
</body>
</html>
