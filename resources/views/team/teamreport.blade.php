@extends('adminlte::page')

@section('title', 'Dashboard')
@section('content_header')
    <h1>Report Panel</h1>
@stop

@section('content')

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Teams</h3>
            <div class="box-tools pull-right">
                <!-- Buttons, labels, and many other things can be placed here! -->
                <!-- Here is a label for example -->
                <a href="{{url('admin/teams/create')}}" class="label label-primary">Add Team</a>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @if (count($teams) >0 )
                <table class="table table-responsive">
                    <tr style="border: none">
                        <th>#</th>
                        <th>Name</th>
                        <th>Count</th>
                    </tr>
                    @foreach ($teams as $team)
                        <tr>
                            <td>{{$team->id}}</td>
                            <td><a href="{{url('admin/teams/'.$team->id)}}">{{$team->name}}</a></td>
                            <td>
                                <?php $totle=0; ?>
                                @foreach ($team->users as $user)
                                    @foreach ($user->stories as $story)
                                        @if ($story->case==0)
                                            <?php $totle+=$story->navel->price;?>
                                        @endif
                                    @endforeach
                                @endforeach
                                {{$totle}}
                            </td>
                        </tr>
                    @endforeach
                </table>
                {{$teams->links()}}
            @endif
        </div>
    </div>

@stop
