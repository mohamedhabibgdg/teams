@extends('adminlte::page')

@section('content_header')
    <h1>عرض جميع الفرق</h1>
@stop

@section('content')

        <div class="box">
            <div class="box-header with-border">
                <div class="box-tools">
                    <a href="{{route('teams.create')}}" class="label label-primary">اضافة فريق</a>
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                @if ($teams->count() >0 )
                <table class="table table-responsive">
                    <tr style="border: none">
                        <th>#</th>
                        <th>اسم الفريق</th>
                        <th>اجمالي ارباح الفريق</th>
                        <th>عدد اعضاء الفريق</th>
                    </tr>
                        @foreach ($teams as $team)
                            <tr>
                                <td>{{$team->id}}</td>
                                <td><a href="{{route('teams.show',$team->id)}}">{{$team->name}}</a></td>
                                <td>
                                    <?php $totle=0; ?>
                                        @foreach ($team->novels as $novel)
                                            @foreach ($team->users as $user)
                                                @foreach ($user->stories as $story)
                                                    @if ($story->case==0 && $novel->id==$story->navel_id)
                                                        <?php $totle+=$novel->price; ?>
                                                    @endif
                                                @endforeach
                                            @endforeach
                                        @endforeach
                                    {{number_format($totle)}}
                                </td>
                                <td><div class="dropdown">
                                        <button class="btn btn-secondary dropdown-toggle" type="button" id="triggerId{{$team->id}}"
                                                data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false">
                                            {{count($team->users)}}
                                        </button>
                                        @if (count($team->users)>0)
                                            <div class="dropdown-menu"  aria-labelledby="triggerId{{$team->id}}">
                                                <ol class="">
                                                    @foreach ($team->users as $key=> $user)
                                                        @if ($key>=9)
                                                            @break
                                                        @endif
                                                        <li><a href="{{route('users.show',$user)}}">{{$user->name}}</a></li>
                                                    @endforeach
                                                </ol>
                                            </div>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                </table>

                @endif
            </div>
            {{$teams->links()}}
        </div>

@stop
