<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Pay extends Model
{
    public $guarded=[];
    use LogsActivity;
    protected static $logOnlyDirty = true;

    public function users(){
        return $this->hasMany('App\User');
    }
}
