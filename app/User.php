<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Traits\LogsActivity;

class User extends Authenticatable implements MustVerifyEmail{
    use Notifiable;
    use LogsActivity;
    protected static $logOnlyDirty = true;

    protected $fillable = [
        'name', 'email', 'password','paymoney','contact_id','contact','pay_id','email_verified_at'
    ];


    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function pay(){ return $this->belongsTo(Pay::class)->withDefault([
        "name"=>"no Pay way"
    ]); }

    public function cont(){ return $this->belongsTo(Contact::class); }
    public function contact(){ return $this->belongsTo(Contact::class)->withDefault([
        "name"=>"no Contact way"
    ]); }

    public function mymoney(){ return $this->hasMany(Money::class); }

    public function teams() { return $this->belongsToMany(Team::class)->withTimestamps(); }

    public function stories() { return $this->hasMany(Story::class); }


    public function roles(){ return $this->belongsToMany(Role::class); }

    public function hasAnyRole($roles){
        if (is_array($roles)){
            foreach ($roles as $role){
                if ($this->hasRole($role)){ return true; }
            }
        }else{
            if ($this->hasRole($roles)){
                return true;
            }
        }
        return false;
    }

    public function hasRole($role){
        if ($this->roles()->where('name',$role)->first()){
            return true;
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function storiesMoney(){

        return $this->stories()->where('case',0)->with('navel')->get()->sum('navel.price');
    }

    public function myAllMoney(){ return $this->mymoney()->where('case',0)->sum('money'); }
    public function oldMoney(){ return $this->mymoney()->where('case',1)->sum('money'); }
    public function moneyMonth(){ return $this->mymoney()->where('case',3)->sum('money'); }
    public function newMoney(){ return $this->mymoney()->where('case',4)->sum('money'); }

    public function GivenMoney(){ return $this->mymoney()->sum('money'); }


    public function userMonth()
    {
        DB::transaction(function (){
            $counter=$this->storiesMoney();
            if ($counter!==0){
                Money::create([
                    'user_id'=>$this->id,
                    'money'=>$counter,
                    'case'=>3
                ]);
            }
            $this->stories()->update(['case' => 1]);
            $this->mymoney()->where('case',4)->update(['case'=>3]);
        });
    }
}
