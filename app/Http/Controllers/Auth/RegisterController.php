<?php

namespace App\Http\Controllers\Auth;

use App\Role;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{

    use RegistersUsers;

    protected $redirectTo = '/user/novel';

    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'pay_id'=>['required'],
            'paymoney'=>['required', 'string'],
            'contact_id'=>['required'],
            'contact'=>['required', 'string'],
        ]);
    }

    protected function create(array $data)
    {
        $data['paymoney']=str_replace('http://','',$data['paymoney']);
        $data['paymoney']=str_replace('https://','',$data['paymoney']);

        $data['contact']=str_replace('http://','',$data['contact']);
        $data['contact']=str_replace('https://','',$data['contact']);
        $data['password']=Hash::make($data['password']);
        $user= User::create($data);

        $user->roles()->attach(Role::where('name','users')->first());
        return $user;
    }
}
