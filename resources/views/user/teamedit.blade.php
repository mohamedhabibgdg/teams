@extends('ms.master')
@section('content')
    <form action="{{url('user/team/'.$team->id)}}" method="POST" role="form">
        @csrf
        @method('PUT')
        <h3>كود الفريق <b class="text-aqua">{{$team->id}}</b> </h3>
        <div class="form-group">
            <label for="name">اسم الفريق</label>
            <input type="text" name="name" id="name" class="form-control" value="{{$team->name}}" placeholder="اسم الفريق" aria-describedby="name">
            <small id="helpId" class="text-muted">اسم الفريق لا يمكن ان يتكرر</small>
        </div>
        <div class="form-group">
            <label for="chatCode">كود المحادثات</label>
            <textarea name="chatCode"  cols="30" rows="10" class="form-control">{{$team->chatCode}}</textarea>
        </div>
        <button type="submit" class="btn btn-warning float-left text-white">تعديل  الفريق</button>
        <a class="btn-secondary btn pull-right" href="{{url('user/team/'.$team->id)}}">العودة</a>
        <div class="clearfix"></div>
    </form>

@stop
