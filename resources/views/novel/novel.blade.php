@extends('adminlte::page')
@section('content_header')

@stop
@section('content')

    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">اسم الرواية : {{$novel->name}}</h3>
                <div class="box-tools pull-right">
                    <a href="{{url('admin/novels/'.$novel->id.'/edit')}}" class="btn btn-warning">تعديل</a>

                    <label for="#">
                        <form action="{{url('admin/novels/'.$novel->id)}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger">حذف</button>
                        </form>
                    </label>
                </div>
            </div>
            <div class="box-body">
                الفريق المسؤل عن الرواية <br>
                <a href="{{url('admin/teams/'.$novel->team->id)}}">{{$novel->team->name}}</a>
                <br>
               سعر الفصل الواحد  <br> {{$novel->price}}$
            </div>
        </div>
    </div>
    
    <div class="col-md-6">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">الفصول</h3>
                <div class="box-tools pull-right"></div>
            </div>
            <div class="box-body">
                @if (count($novel->stories)>0)
                    <ul class="scrolH">
                        @foreach ($novel->stories()->orderBy('storynum','DESC')->paginate(15) as $story)
                            <li style="border-bottom: 1px solid #3f3f3f">
                                <a href="{{url('admin/stories/'.$story->id)}}" class="pull-right">{{$story->storynum}} - {{$story->title}}</a>
                                <a href="{{route('users.show',$story->user->id)}}" class="pull-left" >{{$story->user->name}}</a>
                            </li>
                        @endforeach
                    </ul>
                    @else
                    {{'لا يوجد فصول حتي الان'}}
                @endif
            </div>
        </div>
    </div>

@stop
