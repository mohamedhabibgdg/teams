@extends('layouts.app')

@section('content')
    <div class="container">
        <h2 align="center">{{$story->title}}</h2>
        <p>
            {!! $story->body !!}
        </p>
        <div class="clearfix"></div>
        <hr>
        <a href="{{route('welcome')}}" class="btn btn-secondary"> عودة للصفحة الرئسية</a>
    </div>

@endsection
