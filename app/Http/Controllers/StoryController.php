<?php

namespace App\Http\Controllers;

use App\Story;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class StoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
//        Story::orderBy('storynum','DESC')->paginate(50)
        return view('story.show')->with('stories',Story::orderByDesc('created_at')->paginate(50));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('story.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validated=$request->validate([
            'title'=>['required'],
            'body'=>['required'],
            'navel_id'=>['required'],
            'storynum'=>['required',Rule::unique('stories')->where('navel_id',request('navel_id'))]
        ]);
        $validated['user_id'] = auth()->id();
        Story::create($validated);
        return redirect()->route('stories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\story  $story
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Story $story)
    {
        $nextPage=Story::where('navel_id',$story->navel_id)->where('storynum','>',$story->storynum)->orderBy('storynum','ASC')->first()->id ?? -2;
        $BackPage=Story::where('navel_id',$story->navel_id)->where('storynum','<',$story->storynum)->orderBy('storynum','DESC')->first()->id ?? -2;

        return view('story.story')->with(['story'=>$story,'nextPage'=>$nextPage,'BackPage'=>$BackPage]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Story  $story
     * @return \Illuminate\Http\Response
     */
    public function edit(Story $story)
    {
        return view('story.edit')->with('story',$story);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Story  $story
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Story $story){

        $validate  = $request->validate([
            'title'=>['required'],
            'body'=>['required'],
            'navel_id'=>['required'],
            'storynum'=>['required',Rule::unique('stories')->where('navel_id',request('navel_id')->ignore($story->id))]
        ]);

        $story->update($validate);
        return redirect()->route('stories.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\story  $story
     * @return \Illuminate\Http\Response
     */
    public function destroy(Story $story)
    {
        $story->delete();
        return redirect()->route('stories.index');
    }
}
