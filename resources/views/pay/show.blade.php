@extends('adminlte::page')
@section('content_header')
    <h1>طرق الدفع</h1>
@stop

@section('content')

        <div class="box">
            <div class="box-header with-border">
                <div class="box-tools pull-right">
                    <!-- Buttons, labels, and many other things can be placed here! -->
                    <!-- Here is a label for example -->
                    <a href="{{url('admin/pays/create')}}" class="label label-primary">اضافة طريقة الدفع</a>
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                @if ($pays->count() >0 )
                <table class="table table-responsive">
                    <tr style="border: none">
                        <th>#</th>
                        <th>اسم طريقة الدفع</th>
                        <th>عدد المستخدون</th>
                    </tr>
                        @foreach ($pays as $pay)
                            <tr>
                                <td>{{$pay->id}}</td>
                                <td><a href="{{url('admin/pays/'.$pay->id)}}">{{$pay->name}}</a></td>
                                <td>{{count($pay->users)}}</td>
                            </tr>
                        @endforeach
                </table>
                @endif
            </div>
            {{$pays->links()}}
        </div>

@stop
