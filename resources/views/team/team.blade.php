@extends('adminlte::page')

@section('content_header')
    <h1>اسم الفريق :  {{$team->name}}</h1>
@stop

@section('content')

  <div class="col-md-8">
      <div class="box">
          <div class="box-header with-border">
              <div class="box-tools pull-right">
                  <!-- Buttons, labels, and many other things can be placed here! -->
                  <!-- Here is a label for example -->
                  <a href="{{url('admin/teams/'.$team->id.'/edit')}}" class="btn btn-warning">تحديث</a>
                  <label for="#">
                      <form action="{{url('admin/teams/'.$team->id)}}" method="POST">
                          @csrf
                          @method('DELETE')
                          <button class="btn btn-danger">حذف</button>
                      </form>
                  </label>
              </div>
              <!-- /.box-tools -->
          </div>
          <div class="box-body">
              @if (count($team->users)>0)
                  <table class="table table-responsive">
                      <tr>
                          <td>اسم العضو</td>
                          <td><i class="fa fa-usd"></i></td>
                          <td>المدير</td>
                          {{--<td>Take All money</td>--}}
                          <td>حذف من الفريق</td>
                      </tr>
                      @foreach ($team->users as $user)
                          <tr>
                              <td>
                                  <a class="col-md-8" href="{{url('admin/users/'.$user->id)}}">{{$user->name}}</a>
                              </td>
                              <td>
                              <?php $money=0;?>
                              @foreach ($team->novels as $novel)
                                  @foreach ($user->stories as $story)
                                      @if ($story->case==0 && $novel->id==$story->navel_id)
                                          <?php $money+=$novel->price; ?>
                                      @endif
                                   @endforeach
                              @endforeach
                                  {{$money}}
                              $
                              </td>
                              <td>
                                  @if (\App\TeamUser::where(['user_id'=>$user->id,'team_id'=>$team->id])->first()->rank!=3)
                                      <form action="{{url('admin/rank/'.$team->id.'/'.$user->id)}}" method="POST">
                                          @csrf
                                          @method('PUT')
                                          <button class="btn btn-success" type="submit">مدير</button>
                                      </form>
                                  @else
                                      <form action="{{url('admin/unrank/'.$team->id.'/'.$user->id)}}" method="POST">
                                          @csrf
                                          @method('PUT')
                                          <button class="btn btn-secondary" type="submit">ازاله مدير</button>
                                      </form>
                                  @endif
                              </td>
                              <td>

                                  <form class="" action="{{url('admin/teamsusers/'.$team->id.'/'.$user->id)}}" method="POST">
                                      @csrf
                                      @method('DELETE')
                                      <button class="btn btn-danger" type="submit">حذف</button>
                                  </form>
                              </td>

                          </tr>
                      @endforeach
                  </table>
              <?php $totle=0; ?>
                  @foreach ($team->novels as $novel)
                      @foreach ($team->users as $user)
                          @foreach ($user->stories as $story)
                              @if ($story->case==0 && $novel->id==$story->navel_id)
                                  <?php $totle+=$novel->price; ?>
                              @endif
                          @endforeach
                      @endforeach
                  @endforeach
                  <small>الاجمالي : {{$totle}}$</small>
                  @else
                  {{"لا يوجد اعضاء"}}
              @endif
                  <hr>
            <h3 class="box-title">روايات</h3>
                  @if (count($team->novels)>0)
                      @foreach ($team->novels as $novel)
                          <a href="{{url('admin/novels/'.$novel->id)}}">{{$novel->name}}</a>
                          <ul class="scrolH">
                              @foreach ($novel->stories as $story)
                                  <li><a href="{{url('admin/stories/'.$story->id)}}">{{$story->storynum}} -{{$story->title}}</a></li>
                              @endforeach
                          </ul>
                      <hr>
                      @endforeach
                  @endif
          </div>
      </div>
  </div>
  <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">اضافة اعضاء</h3>
                </div>
                <div class="box-body scrolH">

                    @foreach ($users as $user)
                        <br>
                            <b class="col-md-9">{{$user->name}}</b>
                            <form action="{{url('admin/teamsusers/'.$team->id.'/'.$user->id)}}" method="POST" class="col-md-3">
                                @csrf
                                @method('PUT')
                                <button class="btn btn-primary" type="submit">اضافة</button>
                            </form>
                        <br>
                    @endforeach
                </div>
            </div>
        </div>

  <div class="clearfix"></div>
@stop
