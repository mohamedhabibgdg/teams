<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', 'UserPagesController@welcome')->name('welcome');
Route::get('/teams/{story}', 'UserPagesController@teams');

Auth::routes(['verify'=>true]);

Route::group(['prefix' => 'user','middleware'=>['roles','verified'],'roles'=>['admins','users']], function () {


    Route::get('/profile/{id}','UserPagesController@profile');
    Route::get('/profile/{id}/edit','UserPagesController@profileedit');
    Route::put('/profile/{id}','UserPagesController@profileupdate');
    Route::get('/team/{id}','UserPagesController@myteam');
    Route::get('/changepass/{id}/edit','UserPagesController@editpass');
    Route::put('/changepass/{id}','UserPagesController@updatePassword');
    Route::delete('/teamsusers/{teamId}/{userID}','TeamUserController@deleteTeamUser');
    Route::put('/teamsusers/{teamId}/{userID}','TeamUserController@addTeamUser');
    Route::get('/team/{id}/edit','TeamController@editdata');
    Route::put('/team/{id}','TeamController@updatedata');
    Route::delete('/team/{team}','TeamController@delete');
    Route::put('/money/{user?}','MoneyController@store');
    Route::name('user.')->group( function () {

        Route::resource('stories','UserPagesController')->only(['show','store','edit','update','create']);

    });
    Route::resource('novel','NovelPagesController')->only(['show','index']);

});

Route::group(['prefix' => 'admin','middleware'=>'roles','roles'=>'admins'], function () {

    Route::resource('novels','NavelController');
    Route::get('/changepass/{id}/edit','AdminUsersController@editpass');
    Route::put('/changepass/{id}','AdminUsersController@updatePassword');
    Route::delete('/teamsusers/{teamId}/{userID}','TeamUserController@deleteTeamUser');
    Route::put('/teamsusers/{teamId}/{userID}','TeamUserController@addTeamUser');
    Route::put('/rank/{teamId}/{userID}','TeamController@updaterank');
    Route::put('/unrank/{teamId}/{userID}','TeamController@updateunrank');
    Route::get('/reports','AdminUsersController@teamdata');
    Route::put('/done/{userid}','UserPagesController@changedone');
    Route::put('users/Active/{user}','UserPagesController@activeUser')->name('activeUser');

    Route::post('users/{user}/addMoney', 'AdminUsersController@addMoney')->name('addUserMoney');
    Route::post('users/{user}/delMoney', 'AdminUsersController@delMoney')->name('delUserMoney');
    Route::post('users/{user}/payMoney', 'AdminUsersController@payMoney')->name('payUserMoney');
    Route::get('/money','MoneyController@index')->name('money');
    Route::put('/money/{money}','MoneyController@update');
    Route::delete('/money/{money}','MoneyController@delete')->name('delMoney');
    Route::post('users/Month', 'AdminUsersController@usersMonth')->name('users.Month');
    Route::post('users/{user}/Month', 'AdminUsersController@userMonth')->name('users.user.Month');

    Route::resource('users','AdminUsersController');
    Route::resource('novels','NavelController');
    Route::resource('pays','PayController');
    Route::resource('contacts','ContactController');
    Route::resource('teams','TeamController');
    Route::resource('stories','StoryController');
});
/*
 * index  : Show All Data (GET)
 * create : Show create View (GET)
 * store  : Save data in database (POST)
 * show   : show one data (GET)
 * edit   : show one data for edit  (GET)
 * update : update data and redirect (PUT || PATCH)
 * destroy: Delete Data From database (DELETE)
 *
 * */
